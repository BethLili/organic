<?php

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class RolesPermissionsSeeder extends Seeder
{
    protected $modelsDirectory = 'app/App/Models/*';

    /**
     * Run the database seeds.
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function run()
    {
        app()->make(PermissionRegistrar::class)->forgetCachedPermissions();

        $this->seedRoles();
        $this->seedGeneratedPermissions();
        $this->seedAdditionalPermissions();
//        $this->assignAdminPermissions();
//        $this->assignFinancePermissions();
//        $this->assignSalesPermissions();
//        $this->assignPurchasingPermissions();
    }

    private function seedRoles()
    {
        $backendRoles = [
            'Super Admin',
            'admin',
            'finance',
            'sales',
            'purchasing'
        ];

        foreach ($backendRoles as $backendRole) {
            $role = Role::findOrCreate($backendRole, 'backend');
            $this->command->info('Added backend role: ' . $role->name);
        }
    }

    /**
     * Get all the models in the app/Models folder, and generate permissions for those.
     */
    private function seedGeneratedPermissions()
    {
        $dir = base_path($this->modelsDirectory);
        $slugModels = [];
        foreach (glob($dir) as $file) {
            if (! is_dir($file)) {
                $slugModels[] = $this->studlyToSlug(basename($file));
            }
        }

        // Additional models
        $slugModels[] = 'role';
        $slugModels[] = 'permission';

        $resourceMethods = [
            'view-any',
            'view',
            'create',
            'delete',
            'update',
            'restore',
            'force-delete',
        ];

        foreach ($slugModels as $slugModel) {
            foreach ($resourceMethods as $resourceMethod) {
                Permission::findOrCreate($resourceMethod . '-' . $slugModel, 'backend');
            }
        }
    }

    private function assignAdminPermissions()
    {
        $adminRole = Role::findByName('admin', 'backend');

        $permissions = [
            'view-any-user',
            'view-user',
            'create-user',
            'delete-user',
            'update-user',
            'restore-user',
            'force-delete-user',
            'view-any-backend-user',
            'view-backend-user',
            'create-backend-user',
            'delete-backend-user',
            'update-backend-user',
            'restore-backend-user',
            'force-delete-backend-user',
            'view-any-category',
            'view-category',
            'create-category',
            'delete-category',
            'update-category',
            'restore-category',
            'force-delete-category',
            'view-any-vendor',
            'view-vendor',
            'create-vendor',
            'delete-vendor',
            'update-vendor',
            'restore-vendor',
            'force-delete-vendor',
            'view-any-product',
            'view-product',
            'create-product',
            'delete-product',
            'update-product',
            'restore-product',
            'force-delete-product',
        ];

        foreach ($permissions as $permission) {
            $adminRole->givePermissionTo($permission);
        }
    }

    private function assignFinancePermissions()
    {
        $adminRole = Role::findByName('finance', 'backend');

        $permissions = [
            'view-any-cash',
            'view-cash',
            'create-cash',
            'delete-cash',
            'update-cash',
            'restore-cash',
            'force-delete-cash',
            'view-any-cash-deposit',
            'view-cash-deposit',
            'create-cash-deposit',
            'delete-cash-deposit',
            'update-cash-deposit',
            'restore-cash-deposit',
            'force-delete-cash-deposit',
            'view-any-payment',
            'view-payment',
            'create-payment',
            'delete-payment',
            'update-payment',
            'restore-payment',
            'force-delete-payment',
            'view-any-cash-payment',
            'view-cash-payment',
            'create-cash-payment',
            'delete-cash-payment',
            'update-cash-payment',
            'restore-cash-payment',
            'force-delete-cash-payment',
            'view-any-costing',
            'view-costing',
            'create-costing',
            'delete-costing',
            'update-costing',
            'restore-costing',
            'force-delete-costing',
            'view-any-cash-ledger',
            'view-cash-ledger',
            'create-cash-ledger',
            'delete-cash-ledger',
            'update-cash-ledger',
            'restore-cash-ledger',
            'force-delete-cash-ledger',
            'view-any-equity',
            'view-equity',
            'create-equity',
            'delete-equity',
            'update-equity',
            'restore-equity',
            'force-delete-equity',
            'view-any-equity-ledger',
            'view-equity-ledger',
            'create-equity-ledger',
            'delete-equity-ledger',
            'update-equity-ledger',
            'restore-equity-ledger',
            'force-delete-equity-ledger',
            'view-any-ledger',
            'view-ledger',
            'create-ledger',
            'delete-ledger',
            'update-ledger',
            'restore-ledger',
            'force-delete-ledger',
            'view-any-payable-ledger',
            'view-payable-ledger',
            'create-payable-ledger',
            'delete-payable-ledger',
            'update-payable-ledger',
            'restore-payable-ledger',
            'force-delete-payable-ledger',
            'view-any-report',
            'view-report',
            'create-report',
            'delete-report',
            'update-report',
            'restore-report',
            'force-delete-report',
        ];

        foreach ($permissions as $permission) {
            $adminRole->givePermissionTo($permission);
        }
    }

    private function assignSalesPermissions()
    {
        $adminRole = Role::findByName('sales', 'backend');

        $permissions = [
            'view-any-product',
            'view-product',
            'create-product',
            'delete-product',
            'update-product',
            'restore-product',
            'force-delete-product',
            'view-any-sales-order',
            'view-sales-order',
            'create-sales-order',
            'delete-sales-order',
            'update-sales-order',
            'restore-sales-order',
            'force-delete-sales-order',
        ];

        foreach ($permissions as $permission) {
            $adminRole->givePermissionTo($permission);
        }
    }

    private function assignPurchasingPermissions()
    {
        $adminRole = Role::findByName('purchasing', 'backend');

        $permissions = [
            'view-any-product',
            'view-product',
            'create-product',
            'delete-product',
            'update-product',
            'restore-product',
            'force-delete-product',
            'view-any-product-ledger',
            'view-product-ledger',
            'create-product-ledger',
            'delete-product-ledger',
            'update-product-ledger',
            'restore-product-ledger',
            'force-delete-product-ledger',
            'view-any-purchase-order',
            'view-purchase-order',
            'create-purchase-order',
            'delete-purchase-order',
            'update-purchase-order',
            'restore-purchase-order',
            'force-delete-purchase-order',
        ];

        foreach ($permissions as $permission) {
            $adminRole->givePermissionTo($permission);
        }
    }

    private function studlyToSlug($input)
    {
        $input = str_replace('.php', '', $input);

        $arr = preg_split(
            '/(^[^A-Z]+|[A-Z][^A-Z]+)/',
            $input,
            -1, /* no limit for replacement count */
            PREG_SPLIT_NO_EMPTY /*don't return empty elements*/
            | PREG_SPLIT_DELIM_CAPTURE /*don't strip anything from output array*/
        );

        return Str::slug(implode(' ', $arr));
    }

    private function seedAdditionalPermissions()
    {
        $permissions = [
            'view-any-application-log',
        ];

        foreach ($permissions as $permission) {
            Permission::findOrCreate($permission, 'backend');
        }
    }
}
