<?php

use App\Models\TransactionType;
use Illuminate\Database\Seeder;

class TransactionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedTransactionType();
    }
    private function seedTransactionType()
    {
        $types = [
            ['name' => 'PO',], //sent to vendor
            ['name' => 'SO',],
            ['name' => 'CM',],
            ['name' => 'DM',],

        ];

        foreach ($types as $key => $type) {
            TransactionType::query()->create([
                'name' => $type['name'],
            ]);
        }
    }
}
