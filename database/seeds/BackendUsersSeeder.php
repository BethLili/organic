<?php

use App\Models\BackendUser;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class BackendUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedSuperAdminUsers();
//        $this->seedAdminUsers();
//        $this->seedFinanceUsers();
//        $this->seedSalesUsers();
//        $this->seedPurchasingUsers();
    }

    private function seedSuperAdminUsers()
    {
        $superAdminUsers = [
            [
                'name' => 'Super Admin',
                'email' => 'superadmin@organic.com',
                'password' => Hash::make('organic'),
            ],
        ];

        foreach ($superAdminUsers as $superAdminUser) {
            $user = BackendUser::query()->firstOrCreate(
                ['email' => $superAdminUser['email']],
                $superAdminUser);

            if (! $user->hasRole(Role::SUPER_ADMIN)) {
                $user->assignRole(Role::SUPER_ADMIN);

                $this->command->info('Added super admin user: ' . $user->email);
            }
        }
    }

    private function seedAdminUsers()
    {
        $adminUsers = [
            [
                'name' => 'admin',
                'email' => 'admin@organic.com',
                'password' => Hash::make('password'),
            ],
        ];

        foreach ($adminUsers as $adminUser) {
            $user = BackendUser::query()->firstOrCreate(
                ['email' => $adminUser['email']],
                $adminUser);

            if (! $user->hasRole(Role::ADMIN)) {
                $user->assignRole(Role::ADMIN);

                $this->command->info('Added admin user: ' . $user->email);
            }
        }
    }

    private function seedFinanceUsers()
    {
        $financeUsers = [
            [
                'name' => 'finance',
                'email' => 'finance@organic.com',
                'password' => Hash::make('password'),
            ],
        ];

        foreach ($financeUsers as $financeUser) {
            $user = BackendUser::query()->firstOrCreate(
                ['email' => $financeUser['email']],
                $financeUser);

            if (! $user->hasRole(Role::FINANCE)) {
                $user->assignRole(Role::FINANCE);

                $this->command->info('Added finance user: ' . $user->email);
            }
        }
    }

    private function seedSalesUsers()
    {
        $salesUsers = [
            [
                'name' => 'sales',
                'email' => 'sales@organic.com',
                'password' => Hash::make('password'),
            ],
        ];

        foreach ($salesUsers as $salesUser) {
            $user = BackendUser::query()->firstOrCreate(
                ['email' => $salesUser['email']],
                $salesUser);

            if (! $user->hasRole(Role::SALES)) {
                $user->assignRole(Role::SALES);

                $this->command->info('Added sales user: ' . $user->email);
            }
        }
    }

    private function seedPurchasingUsers()
    {
        $purchasingUsers = [
            [
                'name' => 'Purchasing',
                'email' => 'purchasing@organic.com',
                'password' => Hash::make('password'),
            ],
        ];

        foreach ($purchasingUsers as $purchasingUser) {
            $user = BackendUser::query()->firstOrCreate(
                ['email' => $purchasingUser['email']],
                $purchasingUser);

            if (! $user->hasRole(Role::PURCHASING)) {
                $user->assignRole(Role::PURCHASING);

                $this->command->info('Added purchasing user: ' . $user->email);
            }
        }
    }
}
