<?php

use App\Models\Equity;
use Illuminate\Database\Seeder;

class EquitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedCashAccounts();
    }

    private function seedCashAccounts()
    {
        Equity::query()->create([
            'name' => 'Lilibeth Fabregas',
            'balance' => 0
        ]);
    }
}
