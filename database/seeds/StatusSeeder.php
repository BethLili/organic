<?php

use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedStatus();
    }

    private function seedStatus()
    {
        $statuses = [
            ['name' => 'sent'],
            ['name' => 'received'],
            ['name' => 'cancelled'],
            ['name' => 'partially paid'],
            ['name' => 'paid'],

        ];

        foreach ($statuses as $key => $status) {
            Status::query()->create([
                'name' => $status['name'],
                'slug' => strtolower(str_replace(' ', '-', $status['name'])),
            ]);
        }
    }
}
