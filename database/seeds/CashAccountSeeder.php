<?php

use App\Models\Cash;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CashAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedCashAccounts();
    }

    private function seedCashAccounts()
    {
        Cash::query()->create([
            'bank' => 'Cash on hand',
            'name' => 'Cash on hand',
            'account' => 99999999,
            'balance' => 0
        ]);
    }
}
