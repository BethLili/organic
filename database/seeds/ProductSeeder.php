<?php

use App\Models\Product;
use Backend\Events\POReceived;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedProduct();
    }

    private function seedProduct()
    {
        $products = [
            [
                'name' => 'Brocoli',
                'description' => 'Healthy food for your vegetable dish, salads, meals, soup',
                'category_id' => 1,
                'image_path' => url('images/brocoli.jpg'),
            ],
            [
                'name' => 'Cauliflower',
                'description' => 'Make everything with cauliflower.',
                'category_id' => 1,
                'image_path' =>url('images/cauli.jpg'),
            ],
            [
                'name' => 'Green peas',
                'description' => 'Add flavor to your dishes with these green peas.',
                'category_id' => 1,
                'image_path' =>url('images/green-peas.jpg'),
            ],
            [
                'name' => 'Strawberry',
                'description' => 'Juicy, sweet with a slight sour taste.',
                'category_id' => 2,
                'image_path' =>url('images/strawberry.jpg'),
            ],
            [
                'name' => 'Cherries',
                'description' => 'Juicy, sweet and just right for your taste.',
                'category_id' => 2,
                'image_path' =>url('images/cherries.jpg'),
            ],
            [
                'name' => 'Yellow Watermelon',
                'description' => 'Juicy, sweet and just right for your taste.',
                'category_id' => 2,
                'image_path' =>url('images/watermelon-yellow.png'),
            ],
            [
                'name' => 'Apple',
                'description' => 'Perfect for every meal',
                'category_id' => 2,
                'image_path' =>url('images/apple-fuji-size-113.jpg'),
            ],
            [
                'name' => 'Blueberries',
                'description' => 'Perfect for every meal',
                'category_id' => 2,
                'image_path' =>url('images/blueberry-imported-170gms.jpg'),
            ],
            [
                'name' => 'Carrots',
                'description' => 'Perfect for every meal',
                'category_id' => 1,
                'image_path' =>url('images/carrots.jpg'),
            ],
            [
                'name' => 'Corn',
                'description' => 'Perfect for every meal',
                'category_id' => 3,
                'image_path' =>url('images/corn.jpg'),
            ],
            [
                'name' => 'Potato',
                'description' => 'Perfect for every meal',
                'category_id' => 4,
                'image_path' =>url('images/potato.jpg'),
            ],
            [
                'name' => 'Mongo',
                'description' => 'Perfect for every meal',
                'category_id' => 3,
                'image_path' =>url('images/mongo.jpg'),
            ],
        ];

        foreach ($products as $key => $product) {
            $cat = Product::query()->create([
                'name' => $product['name'],
                'slug' => strtolower(str_replace(' ', '-', $product['name'])),
                'description' => $product['description'],
                'image' => $product['image_path'],
                'category_id' => $product['category_id'],
            ]);

//            if (! empty($product['image_path'])) {
//                $cat->addMedia($product['image_path'])
//                    ->preservingOriginal()
//                    ->toMediaCollection('product_image');
//            }
        }
    }
}
