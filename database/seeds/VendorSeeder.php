<?php

use App\Models\Vendor;
use Illuminate\Database\Seeder;

class VendorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedVendor();
    }

    private function seedVendor()
    {
        $vendors = [
            [
                'name' => 'The Veggie DropOff',
                'description' => 'The Veggie Drop-Off, a weekend market that aims to sell fresh produce that directly helps our farmers, is going to hold their third weekend-market at Angono Lakeside Eco-Park, Angono, Rizal and The Sunday Market at The Pop Up Katipunan, Quezon City from March 14 to 15, from 7 a.m. to 3 p.m.',
                'email' => 'info@veggiedropoff.com',
                'contact' => '09172222222',
                'address' => 'Baguio City',
                'terms' => 30,
            ],
            [
                'name' => 'The Produce',
                'description' => 'The Veggie Drop-Off, a weekend market that aims to sell fresh produce that directly helps our farmers, is going to hold their third weekend-market at Angono Lakeside Eco-Park, Angono, Rizal and The Sunday Market at The Pop Up Katipunan, Quezon City from March 14 to 15, from 7 a.m. to 3 p.m.',
                'email' => 'info@theproduce.com',
                'contact' => '09173333333',
                'address' => 'Baguio City',
                'terms' => 15,
            ],
            [
                'name' => 'Healthy Options',
                'description' => 'The Veggie Drop-Off, a weekend market that aims to sell fresh produce that directly helps our farmers, is going to hold their third weekend-market at Angono Lakeside Eco-Park, Angono, Rizal and The Sunday Market at The Pop Up Katipunan, Quezon City from March 14 to 15, from 7 a.m. to 3 p.m.',
                'email' => 'info@tho.com',
                'contact' => '09174444444',
                'address' => 'Baguio City',
                'terms' => 20,
            ],
        ];

        foreach ($vendors as $key => $vendor) {
            Vendor::query()->create([
                'name' => $vendor['name'],
                'slug' => strtolower(str_replace(' ', '-', $vendor['name'])),
                'description' => $vendor['description'],
                'email' => $vendor['email'],
                'contact' => $vendor['contact'],
                'terms' => $vendor['terms'],
                'address' => $vendor['address'],
            ]);

        }
    }
}
