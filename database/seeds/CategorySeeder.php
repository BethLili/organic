<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedCategories();
    }

    private function seedCategories()
    {
        $categories = [
            ['name' => 'veggies'],
            ['name' => 'fruits'],
            ['name' => 'grains'],
            ['name' => 'crops'],
        ];

        foreach ($categories as $key => $category) {
            $cat = Category::query()->create([
                'name' => $category['name'],
                'slug' => $category['name'],
            ]);

            if (! empty($category['image_path'])) {
                $cat->addMedia($category['image_path'])
                    ->preservingOriginal()
                    ->toMediaCollection('thumbnail');
            }
        }
    }
}
