<?php

/** @var Factory $factory */

use App\Models\PurchaseOrder;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(PurchaseOrder::class, function (Faker $faker) {

    //this code below is not working because of the orders, the code below returns a string, array is needed
    //it goes as far as saving the PO in purchase_orders table, but cannot go through incrementsProductLedger
    $item = \App\Models\Product::find($faker->numberBetween(1,11));
    $price = $faker->numberBetween(20,40);
    $quantity = $faker->numberBetween(20,40);

    return [
        "vendor_id" => rand(1,3),
        "orders" => json_encode(['item' => $item->id, 'price' => $price, 'quantity' => $quantity]),
        "total" => $price * $quantity,
        "status_id" => 1,
    ];
});
