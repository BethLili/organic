<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Cash;
use Faker\Generator as Faker;

$factory->define(Cash::class, function (Faker $faker) {
    return [
        'bank' => $faker->company,
        'name' => $faker->name,
        'account' => $faker->bankAccountNumber,
        'balance' => 0,
    ];
});
