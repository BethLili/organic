<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BackendUser;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

$factory->define(BackendUser::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => Hash::make('password'), // password
        'remember_token' => Str::random(10),
    ];
});
