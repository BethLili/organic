<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    const SENT = 1;
    const RECEIVED = 2;
    const CANCELLED = 3;
    const PARTIALLY_PAID = 4;
    const PAID = 5;

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
