<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrder extends Model
{
    protected $fillable = ['vendor_id', 'orders', 'status', 'total', 'received_at', 'cancelled_at', 'due_at', 'fully_paid_at'];

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function path()
    {
        return '/purchase-orders/' . $this->id;
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

//    public function completed()
//    {
//        if (!$this->status === 1) {
//            return;
//        }
//
//        return $this->hasOne(PayableLedger::class, 'po_id');
//    }

    public function due()
    {
        if ($this->status->id !== Status::RECEIVED) {
            return false;
        }

        if (Carbon::createFromDate($this->due_at) > now()->startOfDay()) {
            return false;
        }

        return true;
    }

    public function amountDue()
    {
        if (! $this->due()) {
            return;
        }

        return $this->total;
    }

    public function payable()
    {
        return $this->hasMany(PayableLedger::class, 'po_id');
    }

    public function recordPayable($total)
    {
        return $this->payable()->create(['po_id' => $this->id, 'total' => $total]);
    }

    public function paymentTransactions()
    {
        return $this->hasMany(CashPayment::class, 'po_id'); //could be multiple payments per PO
    }

    public function payments()
    {
        if (!$this->paymentTransactions()) {
            return;
        }

        return $this->paymentTransactions()->sum('amount') * -1;
    }

    public function balance()
    {
        return $this->total - $this->payments();
    }

    public function ledgers()
    {
        return $this->morphMany(ProductLedger::class, 'ledgerable');
    }
}
