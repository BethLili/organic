<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductLedger extends Model
{
    protected $fillable = [
        'product_id',
        'transaction_type',
        'quantity',
        'balance',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function ledgerable()
    {
        return $this->morphTo();
    }
}
