<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Equity extends Model
{
//    use SoftDeletes;

    protected $fillable = ['name', 'balance'];

    public function path()
    {
        return '/equity/' . $this->id;
    }

    public function ledgers()
    {
        return $this->hasMany(EquityLedger::class);
    }
}
