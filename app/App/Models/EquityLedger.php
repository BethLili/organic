<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EquityLedger extends Model
{
    protected $fillable = [
        'equity_id',
        'amount',
    ];
}
