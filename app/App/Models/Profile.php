<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['user_id', 'first_name', 'last_name', 'birthday', 'mobile', 'personal_info', 'province', 'city'];

    public function state()
    {
        return $this->belongsTo(State::class, 'province');
    }

    public function place()
    {
        return $this->belongsTo(City::class, 'city');
    }
}
