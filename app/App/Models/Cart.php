<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = ['product_id', 'quantity'];

    public function path()
    {
        return '/carts/' . $this->id;
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function costing()
    {
        return $this->belongsTo(Costing::class, 'product_id');
    }
}
