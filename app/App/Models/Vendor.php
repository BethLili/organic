<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Vendor extends Model
{
    use SoftDeletes, Notifiable;

    protected $fillable = ['name', 'slug', 'email', 'description', 'address', 'contact', 'terms'];

    protected static function boot()
    {
        parent::boot();

        static::created(function ($vendor) {
            $vendor->createSlug();
        });

//        static::updated(function ($vendor) {
//            $vendor->createSlug();
//        });
    }

    public function createSlug()
    {
        $this->slug = strtolower(str_replace(' ', '-', $this->name));
        $this->save();
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function path()
    {
        return "/vendors/" . $this->slug;
    }

    public function purchaseOrders()
    {
        return $this->hasMany(PurchaseOrder::class);
    }

    public function cashPayments()
    {
        return $this->hasMany(CashPayment::class);
    }
}
