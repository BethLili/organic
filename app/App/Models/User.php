<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Contracts\Auth\Access\Authorizable;

class User extends Authenticatable implements HasMedia
{
    use HasMediaTrait, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function path() {
        return '/users/' . $this->id;
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('profile_photo')
            ->singleFile()
            ->useFallbackUrl('https://microhealth.com/assets/images/illustrations/personal-user-illustration-@2x.png');
    }

    public function getImage()
    {
        return $this->getFirstMediaUrl('profile_photo');
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function orders() {
        return $this->hasMany(SalesOrder::class, 'user_id');
    }

    public function rating()
    {
        return $this->hasMany(Rating::class);
    }

    public function cart()
    {
        return $this->hasMany(Cart::class);
    }

    public function itemOnCart($product_id)
    {
        return $this->cart()->where('product_id', $product_id)->exists();
    }

    public function wishlist()
    {
        return $this->hasMany(Wishlist::class);
    }

    public function itemOnWishlist($product_id)
    {
        return $this->wishlist()->where('product_id', $product_id)->exists();
    }
}
