<?php

namespace App\Models;

use App\Notifications\ProductOnWishlistReplenished;
use App\Notifications\PurchaseOrderReceived;
use Illuminate\Database\Eloquent\Model;

class Costing extends Model
{
    protected $fillable = ['po_id', 'product_id', 'quantity', 'cost', 'price'];

    protected static function boot()
    {
        parent::boot();

        static::updated(function ($costing) {
            $costing->checkQuantity();
        });
    }

    public function checkQuantity()
    {
//        if ($this->quantity > 0) {
//            foreach($this->product->wishlist as $item) {
//               if ($item->user) {
//                   $item->user->notify(new ProductOnWishlistReplenished($item->product));
//               }
//            }
//        }
        if ($this->quantity <= 0) {
            return;
        }

        foreach($this->product->wishlist as $item) {
            if (!$item->user) {
                return;
            }

            $item->user->notify(new ProductOnWishlistReplenished($item->product));
        }
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function cart()
    {
        return $this->hasMany(Cart::class, 'product_id');
    }
}
