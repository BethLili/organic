<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CashPayment extends Model
{
    protected $fillable = ['cash_id', 'po_id', 'amount', 'description'];

    protected static function boot()
    {
        parent::boot();

        static::created(function ($payment) {
            $payment->updateStatus();
        });

        static::deleted(function ($payment) {
            $payment->updateStatus();
        });
    }

//    public function balance($id = 1)
//    {
//        return $this->where('po_id', $id)->sum('total');
//    }

    public function updateStatus()
    {
        if ($this->purchaseOrder->balance() > 0) {
            $this->purchaseOrder->status_id = 4;
            $this->purchaseOrder->save();
            return $this;
        } else {
            $this->purchaseOrder->status_id = 5;
            $this->purchaseOrder->fully_paid_at = Carbon::now();
            $this->purchaseOrder->save();
            return $this;
        }
    }
    public function purchaseOrder()
    {
        return $this->belongsTo(PurchaseOrder::class, 'po_id');
    }

    public function cash()
    {
        return $this->belongsTo(Cash::class);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function path()
    {
        return '/cash-payments/' . $this->id;
    }
}
