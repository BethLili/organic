<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CashDeposit extends Model
{
    use SoftDeletes;

    protected $fillable = ['amount', 'description', 'cash_id', 'equity_id'];

    public function cash()
    {
        return $this->belongsTo(Cash::class);
    }

   public function equity()
    {
        return $this->belongsTo(Equity::class);
    }

}
