<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CashLedger extends Model
{
    protected $fillable = [
        'cash_id',
        'cashable_type',
        'cashable_id',
        'amount',
    ];

    public function cash()
    {
        return $this->belongsTo(Cash::class);
    }

    public function cashable()
    {
        return $this->morphTo();
    }


}
