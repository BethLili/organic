<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PayableLedger extends Model
{
    protected $fillable = ['po_id', 'total'];

    protected $casts = ['created_at' => 'datetime:Y-m-d',];

    public function purchaseOrder()
    {
        return $this->belongsTo(PurchaseOrder::class, 'po_id');
    }

    public function balance($id = 1)
    {
        return $this->where('po_id', $id)->sum('total');
    }
}
