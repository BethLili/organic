<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesOrder extends Model
{
    protected $fillable = ['user_id', 'orders', 'status', 'total', 'cancelled_at'];

    public function path()
    {
        return '/sales-orders/' . $this->id;
    }

    public function ledgers()
    {
        return $this->morphMany(ProductLedger::class, 'ledgerable');
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function customer()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
