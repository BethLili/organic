<?php

namespace App\Providers;

use App\Models\BackendUser;
use App\Models\Cash;
use App\Models\CashDeposit;
use App\Models\CashPayment;
use App\Models\Equity;
use App\Models\Ledger;
use App\Models\Product;
use App\Models\PurchaseOrder;
use App\Models\Report;
use App\Models\Role;
use App\Models\SalesOrder;
use App\Models\User;
use App\Policies\BackendUserPolicy;
use App\Policies\CashDepositPolicy;
use App\Policies\CashPaymentPolicy;
use App\Policies\CashPolicy;
use App\Policies\EquityPolicy;
use App\Policies\LedgerPolicy;
use App\Policies\ProductPolicy;
use App\Policies\PurchaseOrderPolicy;
use App\Policies\ReportPolicy;
use App\Policies\SalesOrderPolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
        BackendUser::class => BackendUserPolicy::class,
        Cash::class => CashPolicy::class,
        CashDeposit::class => CashDepositPolicy::class,
        CashPayment::class => CashPaymentPolicy::class,
        Equity::class => EquityPolicy::class,
        Product::class => ProductPolicy::class,
        PurchaseOrder::class => PurchaseOrderPolicy::class,
        SalesOrder::class => SalesOrderPolicy::class,
        Report::class => ReportPolicy::class,
        Ledger::class => LedgerPolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function ($user, $ability) {
            return $user->hasRole(Role::SUPER_ADMIN) ? true : null;
        });
    }
}
