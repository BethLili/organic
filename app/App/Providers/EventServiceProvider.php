<?php

namespace App\Providers;

use Backend\Events\CashDepositReceived;
use Backend\Events\CashPaymentMade;
use Backend\Events\PurchaseOrderPaid;
use Backend\Events\POReceived;
use Backend\Listeners\DecreaseCashBalance;
use Backend\Listeners\DecreaseCashLedgerBalance;
use Backend\Listeners\DecreasePayableBalance;
use Backend\Listeners\IncreaseEquityBalance;
use Backend\Listeners\IncreaseEquityLedgerBalance;
use Backend\Listeners\IncrementProductsLedger;
use Backend\Listeners\IncreaseCashBalance;
use Backend\Listeners\IncreaseCashLedgerBalance;
use Backend\Listeners\MakeCosting;
use Backend\Listeners\RecordCashPayment;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Web\Events\SalesOrderPlaced;
use Web\Listeners\DecrementProductsLedger;
use Web\Listeners\IncreaseCashLedgerBalance as WebCashLedger;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        POReceived::class => [
            IncrementProductsLedger::class,
            MakeCosting::class,
        ],
        CashDepositReceived::class => [
            IncreaseCashLedgerBalance::class,
            IncreaseEquityLedgerBalance::class,
            IncreaseCashBalance::class,
            IncreaseEquityBalance::class,
        ]
        ,
        PurchaseOrderPaid::class => [
            RecordCashPayment::class,
            DecreaseCashBalance::class,
            DecreasePayableBalance::class,
        ],
        CashPaymentMade::class => [
            DecreaseCashLedgerBalance::class,
        ],
        SalesOrderPlaced::class => [
            DecrementProductsLedger::class,
            IncreaseCashBalance::class,
            WebCashLedger::class,
        ]

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
