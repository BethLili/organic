<?php

namespace App\Rules;

use App\Models\Costing;
use Illuminate\Contracts\Validation\Rule;

class OrderedItemMustHaveEnoughQuantity implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        foreach($value as $e) {
            $a = Costing::where('product_id', '=', $e['item'])->first();
            return $e['quantity'] > 0;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'We don\'t have enough stocks for your order.';
    }
}
