<?php

namespace App\Rules;

use App\Models\Cash;
use Illuminate\Contracts\Validation\Rule;

class MustHaveEnoughBalance implements Rule
{
    private $request;

    /**
     * Create a new rule instance.
     *
     * @param $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */


    public function passes($attribute, $value)
    {
        $cash = Cash::find($this->request->get('cash_id'));
        return $value > 0 && $value <= $cash->balance;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You don\'t have enough balance.';
    }
}
