<?php

namespace App\Rules;

use App\Models\Costing;
use Illuminate\Contracts\Validation\Rule;

class MustHaveEnoughQuantity implements Rule
{
    private $request;

    /**
     * Create a new rule instance.
     *
     * @param $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $a = Costing::where('product_id', '=', $this->request->get('item'))->first();
        return $value <= $a->quantity;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'We don\'t have enough stocks for your order.';
    }
}
