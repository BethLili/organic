<?php

namespace App\Rules;

use App\Models\Cash;
use App\Models\PurchaseOrder;
use Illuminate\Contracts\Validation\Rule;

class MustNotExceedPayable implements Rule
{
    private $request;

    /**
     * Create a new rule instance.
     *
     * @param $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */


    public function passes($attribute, $value)
    {
        $purchaseOrder = PurchaseOrder::find($this->request);
        return $value <= $purchaseOrder->balance();
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Your payment exceeds your balance payable.';
    }
}
