<?php

namespace App\Notifications;

use App\Models\Product;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UpdatedCosting extends Notification
{
    private $costing;

    /**
     * Create a new notification instance.
     *
     * @param $costing
     */
    public function __construct($costing)
    {
        $this->costing = $costing;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'message' => $this->costing->product->name . '\'s costing has been updated.' ,
            'url' => $this->costing,
            'icon' => 'fas fa-carrot'
        ];
    }
}
