<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PurchaseOrderSent extends Notification
{
    private $purchaseOrder;

    /**
     * Create a new notification instance.
     *
     * @param $purchaseOrder
     */
    public function __construct($purchaseOrder)
    {
        $this->purchaseOrder = $purchaseOrder;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = new MailMessage();
        $message->subject('PO# ' . $this->purchaseOrder->id . ' Received From Organic.ph')
            ->from('info@organic.com')
            ->greeting('Dear ' . $notifiable->name . ',')
            ->line('A new purchase order has been made to you with PO number ' .
                $this->purchaseOrder->id .
                ' with a total of ₱' .
                $this->purchaseOrder->total . '.')
            ->line('Please process accordingly.')
            ->line('Orders are:');

        $orders = json_decode($this->purchaseOrder->orders);
        foreach ($orders as $order) {
            $message->line($order->item . ': ' . $order->quantity . ' kgs at ₱' . $order->price . ' per kilo');
        }

        $message->action('View orders here', 'http://organic-web.local/vendors/purchase-orders/' . $this->purchaseOrder->id);

        return $message;
    }
}
