<?php

namespace App\Notifications;

use App\Models\Product;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ProductOnWishlistReplenished extends Notification
{
    private $product;

    /**
     * Create a new notification instance.
     *
     * @param $product
     */
    public function __construct($product)
    {
        $this->product = $product;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'message' => $this->product->name . ' is now on hand. You will keep receiving notifications as long as this item is in your wishlist.' ,
            'url' => $this->product->path(),
            'icon' => 'fas fa-carrot'
        ];
    }
}
