<?php

namespace App\Notifications;

use App\Models\Product;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ProductAddedInWishlist extends Notification
{
    private $product;

    /**
     * Create a new notification instance.
     *
     * @param $product
     */
    public function __construct($product)
    {
        $this->product = $product;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'message' => 'You added ' . $this->product->name . ' in your wishlist. You will be notified once stocks are replenished' ,
            'url' => $this->product->path(),
            'icon' => 'fas fa-carrot'
        ];
    }
}
