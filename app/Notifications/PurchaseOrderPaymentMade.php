<?php

namespace App\Notifications;

use App\Models\Product;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PurchaseOrderPaymentMade extends Notification
{
    private $purchaseOrder;

    /**
     * Create a new notification instance.
     *
     * @param $purchaseOrder
     */
    public function __construct($purchaseOrder)
    {
        $this->purchaseOrder = $purchaseOrder;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'message' => 'Paid PO#' . $this->purchaseOrder->id ,
            'url' => $this->purchaseOrder->path(),
            'icon' => 'fas fa-money-check-alt'
        ];
    }
}
