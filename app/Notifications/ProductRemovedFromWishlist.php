<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;

class ProductRemovedFromWishlist extends Notification
{
    private $product;

    /**
     * Create a new notification instance.
     *
     * @param $product
     */
    public function __construct($product)
    {
        $this->product = $product;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'message' => 'You removed ' . $this->product->name . ' from your wishlist.' ,
            'url' => $this->product->path(),
            'icon' => 'fas fa-carrot'
        ];
    }
}
