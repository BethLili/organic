<?php

namespace Backend\Http\Laratables;

use Backend\Http\Controllers\PurchaseOrderController;

class PayableLedgerLaratables
{
    public static function laratablesCustomActions($payable)
    {
        return '<a class="btn btn-sm btn-info" href="'
            . action([PurchaseOrderController::class, 'show'], $payable)
            . '">View</a>';
    }

    public static function laratablesTotal($cashLedger)
    {
        return number_format($cashLedger->total, 2);
    }

    public static function laratablesCreatedAt($ledger)
    {
        return $ledger->created_at->format('d M Y');
    }
}
