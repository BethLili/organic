<?php

namespace Backend\Http\Laratables;

use Backend\Http\Controllers\SalesOrderController;

class SalesOrderLaratables
{
    public static function laratablesCustomActions($salesOrder)
    {
        return '<a class="btn btn-sm btn-info" href="'
            . action([SalesOrderController::class, 'show'], $salesOrder)
            . '">View</a>';
    }

    public static function laratablesTotal($cashLedger)
    {
        return number_format($cashLedger->total, 2);
    }

    public static function laratablesCreatedAt($ledger)
    {
        return $ledger->created_at->format('d M Y');
    }
}
