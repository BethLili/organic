<?php

namespace Backend\Http\Laratables;

use Backend\Http\Controllers\CashLedgerController;

class CashLedgerLaratables
{
    public static function laratablesCustomActions($ledger)
    {
        return '<a class="btn btn-sm btn-info" href="'
            . action([CashLedgerController::class, 'show'], $ledger)
            . '">View</a>';
    }

    public static function laratablesAmount($cashLedger)
    {
        return number_format($cashLedger->amount, 2);
    }

    public static function laratablesCashableType($ledger)
    {
        if ($ledger->cashable_type === 'App\\Models\\CashDeposit') {
            return "Cash Deposit";
        } else if ($ledger->cashable_type === 'App\\Models\\CashPayment') {
            return "Cash Payment";
        } else {
            return "Sales";
        }
    }

    public static function laratablesCreatedAt($ledger)
    {
        return $ledger->created_at->format('d M Y');
    }
}
