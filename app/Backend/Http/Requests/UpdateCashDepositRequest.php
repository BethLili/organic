<?php

namespace Backend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCashDepositRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cash_id' => 'required',
            'equity_id' => 'required',
            'amount' => 'required|numeric|between:0,999999999.99',
            'description' => 'required|max:25',
        ];
    }
}
