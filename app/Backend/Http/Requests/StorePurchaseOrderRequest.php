<?php

namespace Backend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePurchaseOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vendor_id' => 'exists:vendors,id',
            'total' => 'required|numeric|between:0,9999999.99',
            'orders' => 'required|array',
            'orders.*.item' => 'required|exists:products,id',
            'orders.*.quantity' => 'required|numeric|between:0,9999.99',
            'orders.*.price' => 'required|numeric|between:0,9999.99'
        ];
    }
}
