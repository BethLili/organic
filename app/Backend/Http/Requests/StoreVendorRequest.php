<?php

namespace Backend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreVendorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:30',
            'terms' => 'required|numeric',
            'email' => 'required|unique:vendors',
            'description' => 'required|min:20|max:100',
            'address' => 'required|max:100',
            'contact' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'This email is already registered with another vendor.'
        ];
    }
}
