<?php

namespace Backend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCashDepositRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cash_id' => 'required|exists:cashes,id',
            'equity_id' => 'required|exists:equities,id',
            'amount' => 'required|numeric|between:0,999999999.99',
            'description' => 'required|max:20',
        ];
    }
}
