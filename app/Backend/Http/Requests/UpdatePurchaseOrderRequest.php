<?php

namespace Backend\Http\Requests;

use App\Rules\MustHaveEnoughBalance;
use App\Rules\MustNotExceedPayable;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePurchaseOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => ['nullable',
                new MustHaveEnoughBalance($this->request),
                new MustNotExceedPayable($this->route()->purchase_order->id)],
        ];
    }
}
