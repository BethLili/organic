<?php

namespace Backend\Http\Transformers;

use App\Models\CashPayment;
use League\Fractal\TransformerAbstract;

class CashPaymentTransformer extends TransformerAbstract
{
    public function transform(CashPayment $cashPayment)
    {
        return [
            'id' => $cashPayment->id,
            'created_at' => $cashPayment->created_at,
            'reference' => $cashPayment->reference,
            'amount' => $cashPayment->amount,
            'cash' => $cashPayment->cash,
        ];
    }
}
