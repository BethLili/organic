<?php

namespace Backend\Http\Transformers;

use App\Models\PurchaseOrder;
use App\Models\Costing;
use League\Fractal\TransformerAbstract;

class CostingTransformer extends TransformerAbstract
{
    public function transform(Costing $shop)
    {
        return [
            'id' => $shop->id,
            'po_id' => $shop->po_id,
            'quantity' => $shop->quantity,
            'cost' => $shop->cost,
            'price' => $shop->price,
        ];
    }
}
