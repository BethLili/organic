<?php

namespace Backend\Http\Transformers;

use App\Models\PurchaseOrder;
use App\Models\Status;
use League\Fractal\TransformerAbstract;

class PurchaseOrderTransformer extends TransformerAbstract
{
    public function transform(PurchaseOrder $purchaseOrder)
    {
        return [
            'po' => $purchaseOrder,
            'vendor' => $purchaseOrder->vendor,
            'orders' => json_decode($purchaseOrder->orders),
           'dueDate' => $purchaseOrder->status === Status::RECEIVED ?? $purchaseOrder->due()->diffForHumans(), //this is not used, but returns an error on PurchaseOrders.vue if deleted. weird
            'path' => $purchaseOrder->path(),
        ];
    }
}
