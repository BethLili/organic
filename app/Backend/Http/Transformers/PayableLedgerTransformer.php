<?php

namespace Backend\Http\Transformers;

use App\Models\PayableLedger;
use League\Fractal\TransformerAbstract;

class PayableLedgerTransformer extends TransformerAbstract
{
    public function transform(PayableLedger $payable)
    {
        return [
            'id' => $payable->id,
            'total' => $payable->total,
            'po' => $payable->purchaseOrder,
            'po_path' => $payable->purchaseOrder->path(),
            'vendor' => $payable->purchaseOrder->vendor,
            'vendor_path' => $payable->purchaseOrder->vendor->path(),
            'created_at' => $payable->created_at,
        ];
    }
}
