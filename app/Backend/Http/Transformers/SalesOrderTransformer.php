<?php

namespace Backend\Http\Transformers;

use App\Models\SalesOrder;
use League\Fractal\TransformerAbstract;

class SalesOrderTransformer extends TransformerAbstract
{
    public function transform(SalesOrder $salesOrder)
    {
        return [
            'id' => $salesOrder->id,
            'total' => $salesOrder->total,
            'sales_path' => $salesOrder->path(),
            'customer' => $salesOrder->customer,
            'customer_path' => $salesOrder->customer->path(),
            'created_at' => $salesOrder->created_at,
            'status' => $salesOrder->status,
        ];
    }
}
