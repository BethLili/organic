<?php

namespace Backend\Http\Transformers;

use App\Models\Vendor;
use League\Fractal\TransformerAbstract;

class VendorLedgerTransformer extends TransformerAbstract
{
    public function transform(Vendor $vendor)
    {
        return [
            'ledger' => $vendor,
            'cash' => $vendor->cash,
        ];
    }
}
