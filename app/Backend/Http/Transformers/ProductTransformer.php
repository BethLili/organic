<?php

namespace Backend\Http\Transformers;

use App\Models\Product;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
    public function transform(Product $product)
    {
        return [
            'id' => $product->id,
            'name' => $product->name,
            'description' => $product->description,
            'category' => $product->category->name,
            'category_id' => $product->category->id,
            'category_path' => $product->category->path(),
            'path' => $product->path(),
            'image_url' => $product->image,
            'balance' => $product->balance(),
        ];
    }
}
