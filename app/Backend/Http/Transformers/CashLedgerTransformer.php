<?php

namespace Backend\Http\Transformers;

use App\Models\CashLedger;
use League\Fractal\TransformerAbstract;

class CashLedgerTransformer extends TransformerAbstract
{
    public function transform(CashLedger $cashLedger)
    {
        return [
            'ledger' => $cashLedger,
            'cash' => $cashLedger->cash,
        ];
    }
}
