<?php

namespace Backend\Http\Transformers;

use App\Models\ProductLedger;
use League\Fractal\TransformerAbstract;

class ProductLedgerTransformer extends TransformerAbstract
{
    public function transform(ProductLedger $productLedger)
    {
        return [
            'ledger' => $productLedger,
            'product' => $productLedger->product,
            'vendor' => $productLedger->ledgerable->vendor,
            'customer' => $productLedger->ledgerable->customer,
            'cost' => $productLedger->product->costing,
        ];
    }
}
