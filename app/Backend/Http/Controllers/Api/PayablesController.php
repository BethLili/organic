<?php

namespace Backend\Http\Controllers\Api;

use App\Models\PayableLedger;
use App\Models\Vendor;
use Backend\Http\Transformers\PayableLedgerTransformer;

class PayablesController extends Controller
{
    public function index(PayableLedger $payable)
    {
        $payables = PayableLedger::query()->get();

        return $this->response->withCollection($payables, new PayableLedgerTransformer);
    }

    public function show(PayableLedger $payable, Vendor $vendor)
    {
        $payables = PayableLedger::query()->whereKey($vendor->id)->get();

        return $this->response->withCollection($payables, new PayableLedgerTransformer);
    }
}
