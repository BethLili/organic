<?php

namespace Backend\Http\Controllers\Api;

use App\Models\SalesOrder;
use App\Models\User;
use Backend\Http\Transformers\SalesOrderTransformer;

class SalesOrderController extends Controller
{
    public function index(SalesOrder $salesOrder)
    {
        $salesOrders = SalesOrder::query()->get();

        return $this->response->withCollection($salesOrders, new SalesOrderTransformer());
    }

    public function show(SalesOrder $salesOrder, User $user)
    {
        $salesOrders = SalesOrder::query()->where('user_id', '=', $user->id)->get();

        return $this->response->withCollection($salesOrders, new SalesOrderTransformer());
    }
}
