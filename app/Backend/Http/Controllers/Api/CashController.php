<?php

namespace Backend\Http\Controllers\Api;

use App\Models\Cash;
use Backend\Events\PurchaseOrderPaid;
use Illuminate\Http\Request;

class CashController extends Controller
{
    public function index()
    {
        return Cash::all();
    }

    public function update(Request $request, Cash $cash)
    {
        $cash->update(['balance' => $cash->balance - $request->balance]);

        event(new PurchaseOrderPaid(auth('backend')->user(), $cash));
    }
}
