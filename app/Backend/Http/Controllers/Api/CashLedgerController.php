<?php

namespace Backend\Http\Controllers\Api;

use App\Models\Cash;
use App\Models\CashDeposit;
use App\Models\CashLedger;
use App\Models\CashPayment;
use Backend\Http\Transformers\CashLedgerTransformer;

class CashLedgerController extends Controller
{
    public function index()
    {
        $cashLedger = CashLedger::query()->get();

        return $this->response->withCollection($cashLedger, new CashLedgerTransformer);
    }

    public function show(Cash $cash)
    {
        $cashLedger = CashLedger::query()->where('cash_id', $cash->id)->get();

        return $this->response->withCollection($cashLedger, new CashLedgerTransformer);
    }

    public function showDeposits(Cash $cash)
    {
        $cashLedger = CashLedger::query()->where('cash_id', $cash->id)->where('cashable_type', '=',CashDeposit::class)->get();

        return $this->response->withCollection($cashLedger, new CashLedgerTransformer);
    }

    public function showPayments(Cash $cash)
    {
        $cashLedger = CashLedger::query()->where('cash_id', $cash->id)->where('cashable_type', '=',CashPayment::class)->get();

        return $this->response->withCollection($cashLedger, new CashLedgerTransformer);
    }
}
