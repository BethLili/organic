<?php

namespace Backend\Http\Controllers\Api;

use App\Models\Cash;
use App\Models\CashDeposit;
use App\Models\CashLedger;
use App\Models\CashPayment;
use App\Models\EquityLedger;
use Backend\Http\Transformers\CashLedgerTransformer;
use Backend\Http\Transformers\EquityLedgerTransformer;

class EquityLedgerController extends Controller
{
    public function show($equityId)
    {
        return EquityLedger::query()->where('equity_id', $equityId)->get();
    }
}
