<?php

namespace Backend\Http\Controllers\Api;

use App\Models\ProductLedger;
use App\Models\Product;
use App\Models\PurchaseOrder;
use App\Models\SalesOrder;
use Backend\Http\Transformers\ProductLedgerTransformer;

class ProductLedgerController extends Controller
{
    public function index(ProductLedger $productLedger)
    {
        $productsLedger = ProductLedger::query()->get();

        return $this->response->withCollection($productsLedger, new ProductLedgerTransformer);
    }

    public function show($productId)
    {
        $productLedger = ProductLedger::query()->where('product_id', $productId)->get();

        return $this->response->withCollection($productLedger, new ProductLedgerTransformer);
    }

    public function showPurchases($productId)
    {
        $productLedger = ProductLedger::query()
            ->where('product_id', $productId)
            ->where('ledgerable_type', '=',PurchaseOrder::class)
            ->get();

        return $this->response->withCollection($productLedger, new ProductLedgerTransformer);
    }

    public function showSales($productId)
    {
        $productLedger = ProductLedger::query()
            ->where('product_id', $productId)
            ->where('ledgerable_type', '=',SalesOrder::class)
            ->get();

        return $this->response->withCollection($productLedger, new ProductLedgerTransformer);
    }
}
