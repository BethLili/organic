<?php

namespace Backend\Http\Controllers\Api;

use App\Models\Product;
use App\Models\PurchaseOrder;
use App\Models\Vendor;
use Backend\Http\Transformers\PurchaseOrderTransformer;

class PurchaseOrderController extends Controller
{
    public function index(PurchaseOrder $purchaseOrder)
    {
        $purchaseOrders = PurchaseOrder::query()->get();

        return $this->response->withCollection($purchaseOrders, new PurchaseOrderTransformer);
    }

    public function show($vendorId)
    {
        $purchaseOrder = PurchaseOrder::query()->where('vendor_id', $vendorId)->get();

        return $this->response->withCollection($purchaseOrder, new PurchaseOrderTransformer);
    }
}
