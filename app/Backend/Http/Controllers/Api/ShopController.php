<?php

namespace Backend\Http\Controllers\Api;

use App\Models\Product;
use App\Models\Costing;
use Backend\Http\Transformers\CostingTransformer;

class ShopController extends Controller
{
    public function index(Product $product)
    {
        $shops = Costing::query()->where('product_id', $product->id)->get();

        return $this->response->withCollection($shops, new CostingTransformer());
    }
}
