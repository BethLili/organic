<?php

namespace Backend\Http\Controllers\Api;

use App\Models\Vendor;

class VendorController extends Controller
{
    public function index()
    {
        return Vendor::all();
    }

    public function show($vendorId)
    {
        $vendor = Vendor::find($vendorId);

        return $this->response->withCollection($vendor, new Ve);
    }
}
