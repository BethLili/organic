<?php

namespace Backend\Http\Controllers\Api;

use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
        return Category::all();
    }
}
