<?php

namespace Backend\Http\Controllers\Api;

use App\Models\CashPayment;
use App\Models\Vendor;
use Backend\Http\Transformers\CashPaymentTransformer;

class CashPaymentController extends Controller
{
    public function show($vendorId)
    {
        $payments = CashPayment::query()
            ->where('vendor_id', $vendorId)
            ->get();

        return $this->response->withCollection($payments, new CashPaymentTransformer());
    }
}
