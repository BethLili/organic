<?php

namespace Backend\Http\Controllers\Api;

use App\Models\Status;

class StatusController extends Controller
{
    public function indexPO()
    {
        return Status::where('model', '=', 'po')->get();
    }

    public function indexSO()
    {
        return Status::where('model', '=', 'so')->get();
    }
}
