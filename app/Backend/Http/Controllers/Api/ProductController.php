<?php

namespace Backend\Http\Controllers\Api;

use App\Models\Product;
use Backend\Http\Transformers\ProductTransformer;

class ProductController extends Controller
{
    public function index(Product $product)
    {
        $products = Product::query()->get();

        return $this->response->withCollection($products, new ProductTransformer);
    }

    public function show($id)
    {
        $product = Product::find($id);

        return $product->slug;
    }
}
