<?php

namespace Backend\Http\Controllers;

use App\Models\Cash;
use Backend\Http\Requests\StoreCashRequest;
use Backend\Http\Requests\UpdateCashRequest;
use Illuminate\Http\Request;

class CashController extends Controller
{
    public function index()
    {
        $cashList = Cash::all();
        return view('backend.cash.index', compact('cashList'));
    }

    public function store(StoreCashRequest $request)
    {
        $cash = Cash::query()->create($request->all());

        return redirect()->action([self::class, 'index'], $cash)
            ->with('flash', 'Cash has been saved.');
    }

    public function show(Cash $cash)
    {
        return view('backend.cash.show', compact('cash'));
    }

    public function edit(Cash $cash)
    {
        $cashList = Cash::all();
        return view('backend.cash.edit', compact('cash', 'cashList'));
    }

    public function update(UpdateCashRequest $request, Cash $cash)
    {
        $cash->update($request->all());

        return redirect()->action([self::class, 'index'], $cash)
            ->with('flash', 'Cash has been updated.');
    }

    public function destroy(Cash $cash)
    {
        $cash->delete();
    }
}
