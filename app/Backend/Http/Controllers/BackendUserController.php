<?php

namespace Backend\Http\Controllers;

use App\Models\BackendUser;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BackendUserController extends Controller
{

    public function index()
    {
        return view('backend.backend-users.index');
    }
}
