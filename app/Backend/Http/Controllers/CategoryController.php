<?php

namespace Backend\Http\Controllers;

use App\Models\BackendUser;
use App\Models\Category;
use App\Notifications\CategoryDeleted;
use App\Notifications\NewCategoryAdded;
use App\Notifications\PurchaseOrderPaymentMade;
use Backend\Http\Requests\StoreCategoryRequest;
use Backend\Http\Requests\UpdateCategoryRequest;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();

        return view('backend.categories.index', compact('categories'));
    }

    public function show(Category $category)
    {
        return view('backend.categories.show', compact('category'));
    }

    public function store(StoreCategoryRequest $request)
    {
        Category::query()->create([
            'name' => $request->name,
            'slug' => strtolower(str_replace(' ', '-', $request->name)),
            ]);

        $backendUsers = BackendUser::all();
        foreach ($backendUsers as $backendUser) {
            $backendUser->notify(new NewCategoryAdded($request));
        }

        return redirect()->action([self::class, 'index'])
            ->with('flash', 'Category has been saved.');
    }

    public function edit(Category $category)
    {
        $categories = Category::all();
        return view('backend.categories.edit', compact('category', 'categories'));
    }

    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $category->update([
            'name' => $request->name,
            'slug' => strtolower(str_replace(' ', '-', $request->name)),
        ]);

        return redirect()->action([self::class, 'index'])
            ->with('flash', 'Category has been updated.');
    }

    public function destroy(Category $category)
    {
        $category->delete();

        $backendUsers = BackendUser::all();
        foreach ($backendUsers as $backendUser) {
            $backendUser->notify(new CategoryDeleted($category));
        }
    }
}
