<?php

namespace Backend\Http\Controllers;

use App\Models\Equity;
use Backend\Http\Requests\StoreEquityRequest;
use Backend\Http\Requests\UpdateEquityRequest;

class EquityController extends Controller
{
    public function index()
    {
        $equities = Equity::all();
        return view('backend.equity.index', compact('equities'));
    }

    public function store(StoreEquityRequest $request)
    {
        $equity = Equity::query()->create($request->all());

        return redirect()->action([self::class, 'index'], $equity)
            ->with('flash', 'Equity has been saved.');
    }

    public function show(Equity $equity)
    {
        $equityTransactions = $equity->ledgers;
        return view('backend.equity.show', compact('equity', 'equityTransactions'));
    }

    public function edit(Equity $equity)
    {
        $equities = Equity::all();
        return view('backend.equity.edit', compact('equity', 'equities'));
    }

    public function update(UpdateEquityRequest $request, Equity $equity)
    {
        $equity->update($request->all());

        return redirect()->action([self::class, 'index'], $equity)
            ->with('flash', 'Equity has been updated.');
    }

    public function destroy(Equity $equity)
    {
        $equity->delete();
    }
}
