<?php

namespace Backend\Http\Controllers;

use App\Models\Cash;
use App\Models\CashPayment;
use App\Models\PurchaseOrder;
use Backend\Events\PurchaseOrderPaid;
use Backend\Http\Requests\StoreCashPaymentRequest;
use Backend\Http\Requests\UpdateCashPaymentRequest;
use Illuminate\Http\Request;

class CashPaymentController extends Controller
{
    public function index()
    {
        $payments = CashPayment::all();
        return view('backend.cash-payments.index', compact('payments'));
    }

    public function show(CashPayment $cashPayment)
    {
        return view('backend.cash-payments.show', compact('cashPayment'));
    }

    public function store(StoreCashPaymentRequest $request)
    {
        $cashPayment = CashPayment::query()->create($request->all());

        event(new PurchaseOrderPaid(auth('backend')->user(), $cashPayment));

        return redirect()->action([self::class, 'index'], $cashPayment)
            ->with('flash', 'Cash payment transaction has been saved.');
    }

    public function edit()
    {

    }

    public function update(UpdateCashPaymentRequest $request, CashPayment $cashPayment)
    {
        $cashPayment->update($request->all());

        return redirect()->action([self::class, 'index'], $cashPayment)
            ->with('flash', 'Cash payment transaction has been updated.');
    }

    public function destroy(CashPayment $cashPayment)
    {
        $cashPayment->delete();

        return redirect()->action([self::class, 'index'], $cashPayment)
            ->with('flash', 'Cash payment transaction has been deleted.');
    }
}
