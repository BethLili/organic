<?php

namespace Backend\Http\Controllers;

use App\Models\CashLedger;
use App\Models\PayableLedger;
use App\Models\PurchaseOrder;
use App\Models\SalesOrder;
use Backend\Http\Laratables\CashLedgerLaratables;
use Backend\Http\Laratables\PayableLedgerLaratables;
use Backend\Http\Laratables\PurchaseOrderLaratables;
use Backend\Http\Laratables\SalesOrderLaratables;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Http\Request;

class LedgerController extends Controller
{
    public function index()
    {
        return view('backend.ledgers.index');
    }

    public function sales()
    {
        return view('backend.ledgers.sales-ledger');
    }

    public function salesDatatables()
    {
        return Laratables::recordsOf(SalesOrder::class, SalesOrderLaratables::class);
    }

    public function purchases()
    {
        return view('backend.ledgers.purchases-ledger');
    }

    public function purchasesDatatables()
    {
        return Laratables::recordsOf(PurchaseOrder::class, PurchaseOrderLaratables::class);
    }

    public function payables()
    {
        $purchaseOrders = PurchaseOrder::all();

        $due = $purchaseOrders->filter(function($value) {
            return $value->due();
        });

        $totalDue = collect($due)
            ->reduce(function($carry, $item){
                return $carry + $item->total;
            }, 0);

        $payables = $purchaseOrders->filter(function($value) {
            return $value->status_id === 2;
        });

        $totalPayables = collect($payables)
            ->reduce(function($carry, $item){
                return $carry + $item->total;
            }, 0);

        $salesOrders = SalesOrder::query()->where('status_id', 2)->count();

        return view('backend.ledgers.payables-ledger', compact('purchaseOrders', 'totalDue', 'totalPayables', 'salesOrders'));
    }

    public function payablesDatatables()
    {
        return Laratables::recordsOf(PayableLedger::class, PayableLedgerLaratables::class);
    }

    public function cash()
    {
        return view('backend.ledgers.cash-ledger');
    }

    public function bankDatatables()
    {
        return Laratables::recordsOf(CashLedger::class, CashLedgerLaratables::class);
    }
}
