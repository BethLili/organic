<?php

namespace Backend\Http\Controllers;

use App\Models\CashLedger;
use App\Models\EquityLedger;
use App\Models\PayableLedger;
use App\Models\ProductLedger;
use App\Models\SalesOrder;
use Backend\Http\Requests\DatesOfGeneratedReportsRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    //not used currently
    public function index()
    {
        return view('backend.reports.index');
    }

    public function profitAndLoss()
    {
        $from = Carbon::now()->startOfMonth();
        $to = Carbon::now()->endOfMonth();

        $sales = $this->getSales($from, $to);

        $totalBeginning = $this->getBeginningInventory($from);

        $totalPurchases = $this->getPurchases($from, $to);

        $totalEnding = $this->getEndingInventory($to);

        //get all expenses - unavailable yet

        return view('backend.reports.profit-and-loss', compact('sales', 'totalBeginning', 'totalPurchases', 'totalEnding'));
    }

    public function generateProfitAndLoss(DatesOfGeneratedReportsRequest $request)
    {
        $from = Carbon::createFromDate($request->from)->startOfDay();
        $to = Carbon::createFromDate($request->to)->endOfDay();

        $sales = $this->getSales($from, $to);

        //get all cost of goods sold
        $totalBeginning = $this->getBeginningInventory($from);

        $totalPurchases = $this->getPurchases($from, $to);

        $totalEnding = $this->getEndingInventory($to);

        //get all expenses - unavailable yet

        return view('backend.reports.generate-profit-and-loss',
            compact('from', 'to', 'sales', 'totalBeginning', 'totalPurchases', 'totalEnding'));
    }

    public function balanceSheet()
    {
        $from = Carbon::now()->startOfMonth();
        $to = Carbon::now()->endOfMonth();

        list($cash, $inventory, $netIncome, $payable, $equity) = $this->getBalanceSheetItems($from, $to);

        return view('backend.reports.balance-sheet', compact('cash', 'inventory', 'payable', 'equity', 'netIncome'));
    }

    public function generateBalanceSheet(DatesOfGeneratedReportsRequest $request)
    {
        $from = Carbon::createFromDate($request->from)->startOfDay();
        $to = Carbon::createFromDate($request->to)->endOfDay();

        list($cash, $inventory, $netIncome, $payable, $equity) = $this->getBalanceSheetItems($from, $to);

        return view('backend.reports.generate-balance-sheet', compact('cash', 'inventory', 'payable', 'equity', 'netIncome', 'from', 'to'));
    }

    /**
     * @param Carbon $from
     * @param Carbon $to
     * @return array
     */
    public function getBalanceSheetItems(Carbon $from, Carbon $to): array
    {
        $cash = CashLedger::query()->whereBetween('created_at', [$from, $to])->sum('amount');

        $inventory = $this->getEndingInventory($to);

        $sales = $this->getSales($from, $to);

        $totalBeginning = $this->getBeginningInventory($from);

        $totalPurchases = $this->getPurchases($from, $to);

        $netIncome = $sales - ($totalBeginning + $totalPurchases - $inventory);

        $payable = PayableLedger::query()->whereBetween('created_at', [$from, $to])->sum('total');
        $equity = EquityLedger::query()->whereBetween('created_at', [$from, $to])->sum('amount');
        return array($cash, $inventory, $netIncome, $payable, $equity);
    }

    /**
     * @param Carbon $from
     * @return int
     */
    protected function getBeginningInventory(Carbon $from): int
    {
        $beginningItems = ProductLedger::whereDate('created_at', '<=', $from)
            ->groupBy('product_id')
            ->select('product_id', DB::raw('sum(quantity) as total'))
            ->get();

        $totalBeginningPerItem = [];

        foreach ($beginningItems as $item) {

            $totalBeginningPerItem[] = $item->total * $item->product->costing->cost;
        }

        return collect($totalBeginningPerItem)
            ->reduce(function ($carry, $item) {
                return $carry + $item;
            }, 0);
    }

    /**
     * @param Carbon $from
     * @param Carbon $to
     * @return int
     */
    protected function getPurchases(Carbon $from, Carbon $to): int
    {
        $purchases = ProductLedger::where('ledgerable_type', '=', 'App\Models\PurchaseOrder')
            ->whereBetween('created_at', [$from, $to])
            ->groupBy('product_id')
            ->select('product_id', DB::raw('sum(quantity) as total'))
            ->get();

        $totalPurchasesPerItem = [];

        foreach ($purchases as $item) {
            if (!$item->product) {
                return false;
            }
            $totalPurchasesPerItem[] = $item->total * $item->product->costing->cost;
        }

        return collect($totalPurchasesPerItem)
            ->reduce(function ($carry, $item) {
                return $carry + $item;
            }, 0);
    }

    /**
     * @param Carbon $to
     * @return int
     */
    protected function getEndingInventory(Carbon $to): int
    {
        $endingItems = ProductLedger::whereDate('created_at', '<=', $to)
            ->groupBy('product_id')
            ->select('product_id', DB::raw('sum(quantity) as total'))
            ->get();

        $totalEndingPerItem = [];

        foreach ($endingItems as $item) {
            if (!$item->product) {
                return false;
            }
            $totalEndingPerItem[] = $item->total * $item->product->costing->cost;
        }

        return collect($totalEndingPerItem)
            ->reduce(function ($carry, $item) {
                return $carry + $item;
            }, 0);
    }

    /**
     * @param Carbon $from
     * @param Carbon $to
     * @return int|mixed
     */
    protected function getSales(Carbon $from, Carbon $to)
    {
        return SalesOrder::query()->whereBetween('created_at', [$from, $to])->sum('total');
    }
}
