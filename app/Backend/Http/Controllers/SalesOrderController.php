<?php

namespace Backend\Http\Controllers;

use App\Models\Cash;
use App\Models\Product;
use App\Models\SalesOrder;
use Backend\Http\Requests\UpdatePurchaseOrderRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SalesOrderController extends Controller
{

    public function index()
    {
        return view('backend.sales.index');
    }

    public function create()
    {

        return view('backend.sales.create');
    }

    public function store(Request $request, SalesOrder $salesOrder)
    {

    }

    public function show(SalesOrder $salesOrder)
    {
        $orders = json_decode($salesOrder->orders);
        $products = Product::all();
        return view('backend.sales.show', compact('salesOrder', 'orders', 'products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\SalesOrder $salesOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(SalesOrder $salesOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\SalesOrder $salesOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalesOrder $salesOrder)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\SalesOrder $salesOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalesOrder $salesOrder)
    {
        $salesOrder->status_id = 6;
        $salesOrder->cancelled_at = Carbon::now();
        $salesOrder->save();
    }
}
