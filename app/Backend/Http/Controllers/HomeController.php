<?php

namespace Backend\Http\Controllers;

use App\Models\PurchaseOrder;
use App\Models\SalesOrder;
use App\Models\Vendor;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $purchaseOrders = PurchaseOrder::all();

        $due = $purchaseOrders->filter(function($value) {
            return $value->due();
        });

        $totalDue = collect($due)
            ->reduce(function($carry, $item){
                return $carry + $item->total;
            }, 0);

        $payables = $purchaseOrders->filter(function($value) {
            return $value->status_id === 2;
        });

        $totalPayables = collect($payables)
            ->reduce(function($carry, $item){
                return $carry + $item->total;
            }, 0);

        $salesOrders = SalesOrder::query()->where('status_id', 2)->count();

        return view('backend.home', compact('purchaseOrders', 'totalDue', 'totalPayables', 'salesOrders'));
    }


    public function utilities()
    {
        $vendors = Vendor::all();

        return view('backend.utilities', compact('vendors'));
    }

    public function finance()
    {
        return view('backend.finance');
    }
}
