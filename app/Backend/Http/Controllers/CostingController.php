<?php

namespace Backend\Http\Controllers;

use App\Models\BackendUser;
use App\Models\Product;
use App\Models\Costing;
use App\Notifications\NewProductAdded;
use App\Notifications\UpdatedCosting;
use Backend\Http\Requests\StoreCostingRequest;
use Illuminate\Http\Request;

class CostingController extends Controller
{
    public function show(Product $product)
    {
        $item = Costing::where('product_id', '=', $product->id)->first();
        return view('backend.shops.show', compact('product', 'item'));
    }

    public function update(Costing $shop, StoreCostingRequest $request)
    {
        $shop->price = $request->price;
        $shop->save();

        $backendUsers = BackendUser::all();
        foreach ($backendUsers as $backendUser) {
            $backendUser->notify(new UpdatedCosting($shop));
        }

    }
}
