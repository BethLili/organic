<?php

namespace Backend\Http\Controllers;

use App\Models\BackendUser;
use App\Models\Category;
use App\Models\Product;
use App\Notifications\NewProductAdded;
use App\Notifications\ProductDeleted;
use Backend\Http\Requests\StoreProductRequest;
use Backend\Http\Requests\UpdateProductRequest;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**

     */
    public function index()
    {
        $products = Product::all();

        $categories = Category::all();

        return view('backend.products.index', compact('products', 'categories'));
    }

    public function store(StoreProductRequest $request)
    {
        $image = $request->file('image');
        $image_name = time(). "." . $image->getClientOriginalExtension();
        $destination = "images/";

        $image->move($destination, $image_name);

        $product = Product::query()->create([
            'name' => $request->name,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'image' => $destination.$image_name,
        ]);

        $backendUsers = BackendUser::all();
        foreach ($backendUsers as $backendUser) {
            $backendUser->notify(new NewProductAdded($product));
        }

        return redirect()->action([self::class, 'index'])
            ->with('flash', $product->name. ' has been added.');
    }

    public function show(Product $product)
    {
        $categories = Category::all();
        return view('backend.products.show', compact('product', 'categories'));
    }

    public function edit(Product $product)
    {
        return view('backend.products.edit', compact('product'));
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        $image = $request->file('image');
        $image_name = time(). "." . $image->getClientOriginalExtension();

        $destination = "images/";

        $image->move($destination, $image_name);

        $product->update([
            'name' => $request->name,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'image' => $destination.$image_name,
        ]);

        return redirect()->action([self::class, 'show'], $product)
            ->with('flash', $product->name. ' has been edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        $backendUsers = BackendUser::all();
        foreach ($backendUsers as $backendUser) {
            $backendUser->notify(new ProductDeleted($product));
        }
    }
}
