<?php

namespace Backend\Http\Controllers;

class NotificationController extends Controller
{
    public function index()
    {
        //$notifications = auth('backend')->user()->notifications->pluck('data');
        $notifications = auth('backend')->user()->notifications;
        //return view('backend.notifications.index')->with('notifications', json_decode($notifications));
        return view('backend.notifications.index', compact('notifications'));
    }

    public function show()
    {
        return auth('backend')->user()->unreadNotifications;
    }

    public function count()
    {
        return [
            'notificationsCount' => auth('backend')->user()
                ->unreadNotifications()
                ->count(),
        ];
    }

    public function destroy($notificationId)
    {
        auth('backend')->user()->notifications()->findOrFail($notificationId)->markAsRead();
    }
}
