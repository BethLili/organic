<?php

namespace Backend\Http\Controllers;

use App\Models\BackendUser;
use App\Models\PurchaseOrder;
use App\Notifications\PurchaseOrderReceived;
use Backend\Events\POReceived;

class ProductLedgerController extends Controller
{

//    protected static function boot()
//    {
//        parent::boot();
//
//        static::created(function ($product) {
//            $product->addToCosting();
//            $product->createSlug();
//        });
//    }

    public function store(PurchaseOrder $purchaseOrder)
    {
        $purchaseOrder->status_id = 2;
        $purchaseOrder->received_at = now();
        $purchaseOrder->save();

        $purchaseOrder->recordPayable($purchaseOrder->total);

        event(new POReceived(auth('backend')->user(), $purchaseOrder));

        $backendUsers = BackendUser::all();
        foreach ($backendUsers as $backendUser) {
            $backendUser->notify(new PurchaseOrderReceived($purchaseOrder));
        }

    }
}
