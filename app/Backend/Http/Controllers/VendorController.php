<?php

namespace Backend\Http\Controllers;

use App\Models\Vendor;
use Backend\Http\Requests\StoreVendorRequest;
use Backend\Http\Requests\UpdateVendorRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class VendorController extends Controller
{
    public function index()
    {
        $vendors = Vendor::all();

        return view('backend.vendors.index', compact('vendors'));
    }

    public function create()
    {
        return view('backend.vendors.create');
    }

    public function store(StoreVendorRequest $request)
    {
        $vendor = Vendor::query()->create($request->all());

        return redirect()->action([self::class, 'show'], $vendor)
            ->with('flash', 'Vendor has been saved.');
    }

    public function show(Vendor $vendor)
    {
        return view('backend.vendors.show', compact('vendor'));
    }

    public function edit(Vendor $vendor)
    {
        return view('backend.vendors.edit', compact('vendor'));
    }

    public function update(UpdateVendorRequest $request, Vendor $vendor)
    {
        $vendor->update($request->all());

        return redirect()->action([self::class, 'show'], $vendor)
            ->with('flash', 'Vendor has been updated.');
    }

    public function destroy(Vendor $vendor)
    {
        $vendor->delete();

//        Session::flash('success', $vendor->name . ' successfully deleted.');
//
//        return redirect()->action([self::class, 'index']);
    }
}
