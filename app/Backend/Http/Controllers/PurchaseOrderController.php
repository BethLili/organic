<?php

namespace Backend\Http\Controllers;

use App\Models\BackendUser;
use App\Models\Cash;
use App\Models\Product;
use App\Models\PurchaseOrder;
use App\Models\Vendor;
use App\Notifications\PurchaseOrderSent;
use App\Notifications\PurchaseOrderSentCompanyCopy;
use App\Notifications\PurchaseOrderPaymentMade;
use Backend\Events\POReceived;
use Backend\Events\PurchaseOrderPaid;
use Backend\Http\Requests\StorePurchaseOrderRequest;
use Backend\Http\Requests\UpdatePurchaseOrderRequest;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class PurchaseOrderController extends Controller
{

    public function index()
    {
        return view('backend.purchases.index');
    }

    public function store(StorePurchaseOrderRequest $request, PurchaseOrder $purchaseOrder)
    {
        $vendor = Vendor::find($request->vendor_id);

        $po = $purchaseOrder->create([
            'vendor_id' => $request->vendor_id,
            'total' => $request->total,
            'orders' => json_encode($request->orders),
            'due_at' => now()->addDays((int)$vendor->terms)->startOfDay(),
        ]);

     //   $vendor->notify(new PurchaseOrderSent($po));

        $backendUsers = BackendUser::all();
        foreach ($backendUsers as $backendUser) {
            $backendUser->notify(new PurchaseOrderSentCompanyCopy($po));
        }
    }

    public function show(PurchaseOrder $purchaseOrder)
    {
        $orders = json_decode($purchaseOrder->orders);
        $cash = Cash::all();
        $products = Product::all();
        return view('backend.purchases.show', compact('purchaseOrder', 'orders', 'cash', 'products'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\PurchaseOrder $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(PurchaseOrder $purchaseOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\PurchaseOrder $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePurchaseOrderRequest $request, PurchaseOrder $purchaseOrder)
    {
        event(new PurchaseOrderPaid(auth('backend')->user(), $purchaseOrder, $request));

        $backendUsers = BackendUser::all();
        foreach ($backendUsers as $backendUser) {
            $backendUser->notify(new PurchaseOrderPaymentMade($purchaseOrder));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\PurchaseOrder $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(PurchaseOrder $purchaseOrder)
    {
        $purchaseOrder->status_id = 3;
        $purchaseOrder->cancelled_at = Carbon::now();
        $purchaseOrder->save();
    }


    public function showVendor(PurchaseOrder $purchaseOrder)
    {
        $orders = json_decode($purchaseOrder->orders);
        $products = Product::all();
        return view('vendors.purchases.show', compact('purchaseOrder', 'orders', 'products'));
    }

    public function download(PurchaseOrder $purchaseOrder)
    {
        $orders = json_decode($purchaseOrder->orders);
        $products = Product::all();
        $pdf = PDF::loadView('vendors.purchases.pdf', compact('purchaseOrder', 'orders', 'products'));

        return $pdf->download('PO#' . $purchaseOrder->id);
    }

}
