<?php

namespace Backend\Http\Controllers;

use App\Models\Cash;
use App\Models\CashDeposit;
use App\Models\Equity;
use Backend\Events\CashDepositReceived;
use Backend\Http\Requests\StoreCashDepositRequest;
use Backend\Http\Requests\UpdateCashDepositRequest;
use Illuminate\Http\Request;

class CashDepositController extends Controller
{
    public function index()
    {
        $banks = Cash::all();
        $shareholders = Equity::all();
        $deposits = CashDeposit::all();
        return view('backend.cash-deposits.index', compact('banks', 'shareholders', 'deposits'));
    }

    public function create()
    {
        $banks = Cash::all();
        $shareholders = Equity::all();
        return view('backend.cash-deposits.create', compact('banks', 'shareholders'));
    }

    public function show(CashDeposit $cashDeposit)
    {
        return view('backend.cash-deposits.show', compact('cashDeposit'));
    }

    public function store(StoreCashDepositRequest $request)
    {
        $cashDeposit = CashDeposit::query()->create($request->all());

        event(new CashDepositReceived(auth('backend')->user(), $cashDeposit));

        return redirect()->action([self::class, 'index'], $cashDeposit)
            ->with('flash', 'Cash deposit transaction has been saved.');
    }

    public function edit()
    {

    }

    public function update(UpdateCashDepositRequest $request, CashDeposit $cashDeposit)
    {
//        $cashDeposit->update($request->all());
//
//        return redirect()->action([self::class, 'index'], $cashDeposit)
//            ->with('flash', 'Cash deposit transaction has been updated.');
    }

    public function destroy()
    {

    }
}
