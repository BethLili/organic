<?php

namespace Backend\Http\Controllers;

use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('backend.profiles.index', compact('users'));
    }

    public function show(User $user)
    {
        return view('backend.profiles.show', compact('user'));
    }
}
