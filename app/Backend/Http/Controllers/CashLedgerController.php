<?php

namespace Backend\Http\Controllers;

use App\Models\CashDeposit;
use App\Models\CashLedger;
use App\Models\CashPayment;
use App\Models\Product;
use App\Models\SalesOrder;
use Illuminate\Http\Request;

class CashLedgerController extends Controller
{
    public function show($id)
    {
        $cashLedger = CashLedger::find($id);

        if ($cashLedger->cashable_type === "App\Models\CashDeposit") {
            $cashDeposit = CashDeposit::query()->whereKey($cashLedger->cashable_id)->first();
            return view('backend.cash-deposits.show', compact('cashDeposit'));
        }

        if ($cashLedger->cashable_type === "App\Models\CashPayment") {
            $cashPayment = CashPayment::query()->whereKey($cashLedger->cashable_id)->first();
            return view('backend.cash-payments.show', compact('cashPayment'));
        }

        if ($cashLedger->cashable_type === "App\Models\SalesOrder") {
            $salesOrder = SalesOrder::query()->whereKey($cashLedger->cashable_id)->first();
            $orders = json_decode($salesOrder->orders);
            $products = Product::all();
            return view('backend.sales.show', compact('salesOrder', 'orders', 'products'));
        }
    }
}
