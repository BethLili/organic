<?php

namespace Backend\Events;

use App\Models\BackendUser;

class CashDepositReceived
{
    public $user;
    public $id;
    public $cashId;
    public $equityId;
    public $amount;

    public function __construct(BackendUser $user, $cashDeposit)
    {
        $this->user = $user;
        $this->id = $cashDeposit->id;
        $this->cashId = $cashDeposit->cash_id;
        $this->equityId = $cashDeposit->equity_id;
        $this->amount = $cashDeposit->amount;
    }
}
