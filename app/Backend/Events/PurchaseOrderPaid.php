<?php

namespace Backend\Events;

use App\Models\BackendUser;

class PurchaseOrderPaid
{
    public $user;
    public $id;
    public $cashId;
    public $poId;
    public $vendorId;
    public $amount;
    public $reference;

    public function __construct(BackendUser $user, $purchaseOrder, $request)
    {
        $this->user = $user;
        $this->id = $purchaseOrder->id;
        $this->cashId = $request->cash_id;
        $this->poId = $purchaseOrder->id;
        $this->vendorId = $purchaseOrder->vendor_id;
        $this->amount = $request->amount;
        $this->reference = $request->reference;
    }
}
