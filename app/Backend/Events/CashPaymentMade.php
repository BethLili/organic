<?php

namespace Backend\Events;

use App\Models\BackendUser;

class CashPaymentMade
{
    public $user;
    public $id;
    public $cashId;
    public $amount;
    public $reference;

    public function __construct(BackendUser $user, $payment)
    {
        $this->user = $user;
        $this->id = $payment->id;
        $this->cashId = $payment->cash_id;
        $this->amount = $payment->amount;
    }
}
