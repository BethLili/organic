<?php

namespace Backend\Events;

use App\Models\BackendUser;

class POReceived
{
    public $user;
    public $id;
    public $orders;
    public $balance;

    public function __construct(BackendUser $user, $purchaseOrder)
    {
        $this->user = $user;
        $this->id = $purchaseOrder->id;
        $this->orders = json_decode($purchaseOrder->orders);
        $this->balance = $purchaseOrder->balance();
    }
}
