<?php

namespace Backend\Listeners;

use App\Models\Cash;

class IncreaseCashBalance
{
    public function handle(object $event)
    {
        $cash = Cash::find($event->cashId);
        $cash->balance = $cash->balance + $event->amount;
        $cash->save();
    }
}
