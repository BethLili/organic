<?php

namespace Backend\Listeners;

use App\Models\Costing;
use App\Models\Product;
use App\Models\ProductLedger;
use App\Models\PurchaseOrder;

class IncrementProductsLedger
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(object $event)
    {
        foreach ($event->orders as $data) {
            [
                $data->item,
                $data->quantity,
                $data->price,
            ];

            $ledger = new ProductLedger();
            $ledger->ledgerable_type = PurchaseOrder::class;
            $ledger->ledgerable_id = $event->id;
            $ledger->product_id = (int)($data->item);
            $ledger->quantity = (int)$data->quantity;
            $ledger->save();

        }
    }
}
