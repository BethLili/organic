<?php

namespace Backend\Listeners;

use App\Models\Product;
use App\Models\Costing;

class MakeCosting
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(object $event)
    {
        foreach ($event->orders as $data) {
            [
                $data->item,
                $data->quantity,
                $data->price,
            ];

            $costing = Costing::where('product_id', '=', $data->item)->first();

            $costing->update([
                'product_id' => (int)($data->item),
                'quantity' => (int)($data->quantity + $costing->quantity),
                'cost' => $cost = (int)(($data->price * $data->quantity)
                        + ($costing->cost * $costing->quantity))
                        / ($data->quantity + $costing->quantity), //to get the average cost
                'price' => $cost * 1.20 //TODO Make margin dynamic
            ]);
        }
    }
}
