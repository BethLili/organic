<?php

namespace Backend\Listeners;

use App\Models\Cash;

class DecreaseCashBalance
{
    public function handle(object $event)
    {
        $cash = Cash::find($event->cashId);
        $cash->balance = $cash->balance - $event->amount;
        $cash->save();
    }
}
