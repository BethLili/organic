<?php

namespace Backend\Listeners;

use App\Models\Cash;
use App\Models\CashLedger;
use App\Models\CashPayment;
use Backend\Events\CashPaymentMade;
use Backend\Events\PurchaseOrderPaid;

class RecordCashPayment
{
    public function handle(object $event)
    {
        $payment = new CashPayment();
        $payment->cash_id = $event->cashId;
        $payment->po_id = $event->poId;
        $payment->vendor_id = $event->vendorId;
        $payment->amount = $event->amount * -1;
        $payment->reference = $event->reference;
        $payment->save();

        event(new CashPaymentMade(auth('backend')->user(), $payment));
    }
}
