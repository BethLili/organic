<?php

namespace Backend\Listeners;

use App\Models\Cash;
use App\Models\CashDeposit;
use App\Models\CashLedger;
use App\Models\PayableLedger;

class DecreasePayableBalance
{
    public function handle(object $event)
    {
        $payable = new PayableLedger();
        $payable->po_id = $event->poId;
        $payable->total = ($event->amount) * -1;
        $payable->save();
    }
}
