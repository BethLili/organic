<?php

namespace Backend\Listeners;

use App\Models\Cash;
use App\Models\CashLedger;
use App\Models\CashPayment;

class DecreaseCashLedgerBalance
{
    public function handle(object $event)
    {
        $ledger = new CashLedger();
        $ledger->cashable_type = CashPayment::class;
        $ledger->cashable_id = $event->id; //cashPayment id
        $ledger->cash_id = $event->cashId;
        $ledger->amount = $event->amount;
        $ledger->save();
    }
}
