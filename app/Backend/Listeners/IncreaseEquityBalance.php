<?php

namespace Backend\Listeners;

use App\Models\Equity;

class IncreaseEquityBalance
{
    public function handle(object $event)
    {
        $equity = Equity::find($event->equityId);
        $equity->balance = $equity->balance + $event->amount;
        $equity->save();
    }
}
