<?php

namespace Backend\Listeners;

use App\Models\EquityLedger;

class IncreaseEquityLedgerBalance
{
    public function handle(object $event)
    {
        $ledger = new EquityLedger();
        $ledger->equity_id = $event->equityId;
        $ledger->amount = $event->amount;
        $ledger->save();
    }
}
