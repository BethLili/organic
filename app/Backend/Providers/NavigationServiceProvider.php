<?php

namespace Backend\Providers;

use App\Models\BackendUser;
use App\Models\Cash;
use App\Models\CashDeposit;
use App\Models\CashPayment;
use App\Models\Category;
use App\Models\Equity;
use App\Models\Ledger;
use App\Models\PayableLedger;
use App\Models\Product;
use App\Models\PurchaseOrder;
use App\Models\Report;
use App\Models\SalesOrder;
use Backend\Http\Controllers\BackendUserController;
use App\Models\User;
use Backend\Http\Controllers\CashController;
use Backend\Http\Controllers\CashDepositController;
use Backend\Http\Controllers\CashPaymentController;
use Backend\Http\Controllers\CategoryController;
use Backend\Http\Controllers\EquityController;
use Backend\Http\Controllers\HomeController;
use Backend\Http\Controllers\LedgerController;
use Backend\Http\Controllers\PayableLedgerController;
use Backend\Http\Controllers\ProductController;
use Backend\Http\Controllers\PurchaseOrderController;
use Backend\Http\Controllers\ReportController;
use Backend\Http\Controllers\SalesOrderController;
use Backend\Http\Controllers\UserController;
use Backend\Http\Controllers\VendorController;
use Illuminate\Support\ServiceProvider;
use Spatie\Menu\Laravel\Menu;

class NavigationServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Menu::macro('backend', function () {
            return Menu::new()
                ->setActiveClassOnLink()
                ->setAttribute('class', 'nav-main')
                ->route('dashboard', '<i class="fas fa-users-cog"></i> Dashboard')
                ->html('Administration', ['class' => 'nav-main-heading'])
                ->route('utilities', '<i class="fas fa-users-cog"></i> Utilities')
                ->route('finance', '<i class="fas fa-dollar-sign"></i> Finance')
                ->action(
                    [VendorController::class, 'index'],
                    '<div class="dropright">
                      Ss
                      <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="sr-only">Toggle Dropright</span>
                      </button>
                      <div class="dropdown-menu">
                        <button class="dropdown-item" type="button">Action</button>
                        <button class="dropdown-item" type="button">Another action</button>
                        <button class="dropdown-item" type="button">Something else here</button>
                      </div>
                    </div>'
                )
                ->action(
                    [CategoryController::class, 'index'],
                    '<i class="fas fa-users-cog"></i> Categories'
                )
                ->actionIfCan(
                    ['viewAny', Cash::class],
//                ->action(
                    [CashController::class, 'index'],
                    '<i class="fa fa-dollar-sign"></i> Cash'
                )
                ->actionIfCan(
                    ['viewAny', User::class],
//                ->action(
                    [UserController::class, 'index'],
                    '<i class="fas fa-users-cog"></i> Users'
                )
//                ->actionIfCan(
//                    ['viewAny', Equity::class],
                ->action(
                    [EquityController::class, 'index'],
                    '<i class="fa fa-dollar-sign"></i> Equity'
                )
//                ->actionIfCan(
//                    ['viewAny', Product::class],
                ->action(
                    [ProductController::class, 'index'],
                    '<i class="fas fa-carrot"></i> Products'
                )
//                ->actionIfCan(
//                    ['viewAny', PurchaseOrder::class],
                ->action(
                    [PurchaseOrderController::class, 'index'],
                    '<i class="fas fa-cash-register"></i> Purchase Orders'
                )
//                ->actionIfCan(
//                    ['viewAny', SalesOrder::class],
                ->action(
                    [SalesOrderController::class, 'index'],
                    '<i class="fas fa-cash-register"></i> Sales Orders'
                )
//                ->actionIfCan(
//                    ['viewAny', CashDeposit::class],
                ->action(
                    [CashDepositController::class, 'index'],
                    '<i class="fas fa-cash-register"></i> Cash Deposits'
                )
//                ->actionIfCan(
//                    ['viewAny', CashPayment::class],
                ->action(
                    [CashPaymentController::class, 'index'],
                    '<i class="fas fa-cash-register"></i> Cash Payments'
                )
//                ->actionIfCan(
//                    ['viewAny', Ledger::class],
                ->action(
                    [LedgerController::class, 'index'],
                    '<i class="fas fa-chart-bar"></i> Ledgers'
                )
//                ->actionIfCan(
//                    ['viewAny', Report::class],
                ->action(
                    [ReportController::class, 'index'],
                    '<i class="fas fa-chart-line"></i> Reports'
                )
//                ->actionIfCan(
//                    ['viewAny', BackendUser::class],
                ->action(
                    [BackendUserController::class, 'index'],
                    '<i class="fas fa-users-cog"></i> Backend Users'
                )

//                ->htmlIf(
//                    request()->user()->can('view-any-access-log') ||
//                    request()->user()->can('view-any-email-log') ||
//                    request()->user()->can('view-any-application-log'),
//                    'Logs',
//                    ['class' => 'nav-main-heading']
//                )
//                ->actionIfCan(
//                    ['viewAny', AccessLog::class],
//                    [AccessLogController::class, 'index'],
//                    '<i class="far fa-clipboard-list"></i> Access Logs'
//                )
//                ->actionIfCan(
//                    ['viewAny', EmailLog::class],
//                    [EmailLogController::class, 'index'],
//                    '<i class="far fa-envelope-open-text"></i> Email Logs'
//                )
//                ->actionIfCan(
//                    'view-any-application-log',
//                    [LogViewerController::class, 'index'],
//                    '<i class="far fa-stream"></i> Application Logs</a>'
//                )
                ->setActive(request()->getUri());
        });
    }
}
