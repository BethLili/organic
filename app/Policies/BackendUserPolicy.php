<?php

namespace App\Policies;

use App\Models\BackendUser;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Foundation\Auth\User;

class BackendUserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the list of backend users.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the backend user.
     *
     * @param User $user
     * @param BackendUser $backendUser
     * @return mixed
     */
    public function view(User $user, BackendUser $backendUser)
    {
        return $user->can('view-backend-user');
    }

    /**
     * Determine whether the user can create backend users.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('create-backend-user');
    }

    /**
     * Determine whether the user can update the backend user.
     *
     * @param User $user
     * @param BackendUser $backendUser
     * @return mixed
     */
    public function update(User $user, BackendUser $backendUser)
    {
        return $user->can('update-backend-user');
    }

    /**
     * Determine whether the user can delete the backend user.
     *
     * @param User $user
     * @param BackendUser $backendUser
     * @return mixed
     */
    public function delete(User $user, BackendUser $backendUser)
    {
        return $user->can('delete-backend-user');
    }

    /**
     * Determine whether the user can restore the backend user.
     *
     * @param User $user
     * @param BackendUser $backendUser
     * @return mixed
     */
    public function restore(User $user, BackendUser $backendUser)
    {
        return $user->can('restore-backend-user');
    }

    /**
     * Determine whether the user can permanently delete the backend user.
     *
     * @param User $user
     * @param BackendUser $backendUser
     * @return mixed
     */
    public function forceDelete(User $user, BackendUser $backendUser)
    {
        return $user->can('force-delete-backend-user');
    }
}
