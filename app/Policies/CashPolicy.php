<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Foundation\Auth\User;

class CashPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return true;
    }
}
