<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Foundation\Auth\User as AuthUser;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the list of models.
     *
     * @param AuthUser $user
     * @return mixed
     */
    public function viewAny(AuthUser $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the user.
     *
     * @param AuthUser $user
     * @param User $model
     * @return mixed
     */
    public function view(AuthUser $user, User $model)
    {
        return $user->can('view-user');
    }

    /**
     * Determine whether the user can create users.
     *
     * @param AuthUser $user
     * @return mixed
     */
    public function create(AuthUser $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param AuthUser $user
     * @param User $model
     * @return mixed
     */
    public function update(AuthUser $user, User $model)
    {
        return $user->can('update-user');
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param AuthUser $user
     * @param User $model
     * @return mixed
     */
    public function delete(AuthUser $user, User $model)
    {
        return $user->can('delete-user');
    }

    /**
     * Determine whether the user can restore the user.
     *
     * @param AuthUser $user
     * @param User $model
     * @return mixed
     */
    public function restore(AuthUser $user, User $model)
    {
        return $user->can('restore-user');
    }

    /**
     * Determine whether the user can permanently delete the user.
     *
     * @param AuthUser $user
     * @param User $model
     * @return mixed
     */
    public function forceDelete(AuthUser $user, User $model)
    {
        return $user->can('force-delete-user');
    }
}
