<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Foundation\Auth\User;

class EquityPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return true;
    }
}
