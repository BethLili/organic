<?php

namespace Web\Events;

use App\Models\BackendUser;
use App\Models\User;

class SalesOrderPlaced
{
    public $user;
    public $id;
    public $orders;
    public $amount;
    public $cashId;

    public function __construct(User $user, $salesOrder)
    {
        $this->user = $user;
        $this->cashId = 1;
        $this->id = $salesOrder->id;
        $this->amount = $salesOrder->total;
        $this->orders = json_decode($salesOrder->orders);
    }
}
