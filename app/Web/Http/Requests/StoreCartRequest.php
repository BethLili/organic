<?php

namespace Web\Http\Requests;

use App\Rules\MustHaveEnoughQuantity;
use Illuminate\Foundation\Http\FormRequest;

class StoreCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity' => ['required', 'integer', new MustHaveEnoughQuantity($this->request)]
        ];
    }
}
