<?php

namespace Web\Http\Transformers;

use App\Models\Costing;
use League\Fractal\TransformerAbstract;

class CostingTransformer extends TransformerAbstract
{
    public function transform(Costing $costing)
    {
        return [
            'id' => $costing->id,
            'price' => $costing->price,
            'product' => $costing->product,
         //   'image' => $costing->product->getImage(),
            'balance' => $costing->product->balance(),
            'category' => $costing->product->category,
            'cart' => $costing->product->itemOnCart(),
            'wishlist' => $costing->product->itemIsOnWishlist(),
        ];
    }
}
