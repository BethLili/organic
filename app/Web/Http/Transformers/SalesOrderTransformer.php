<?php

namespace Web\Http\Transformers;

use App\Models\Product;
use App\Models\SalesOrder;
use League\Fractal\TransformerAbstract;

class SalesOrderTransformer extends TransformerAbstract
{
    public function transform(SalesOrder $salesOrder)
    {
        return [
            'id' => $salesOrder->id,
            'total' => $salesOrder->total,
            'orders' => json_decode($salesOrder->orders),
            'path' => $salesOrder->path(),
            'created_at' => $salesOrder->created_at,
        ];
    }
}
