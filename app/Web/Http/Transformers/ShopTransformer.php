<?php

namespace Web\Http\Transformers;

use App\Models\Costing;
use League\Fractal\TransformerAbstract;

class ShopTransformer extends TransformerAbstract
{
    public function transform(Costing $costing)
    {
        return [
            'id' => $costing->id,
            'price' => $costing->price,
            'product' => $costing->product,
            'image' => $costing->product->image,
            'balance' => $costing->product->balance(),
            'category' => $costing->product->category,
        ];
    }
}
