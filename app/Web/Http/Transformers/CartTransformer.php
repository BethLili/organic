<?php

namespace Web\Http\Transformers;

use App\Models\Cart;
use League\Fractal\TransformerAbstract;

class CartTransformer extends TransformerAbstract
{
    public function transform(Cart $cart)
    {
        return [
            'price' => $cart->price,
            'quantity' => $cart->quantity,
            'product' => $cart->product,
            'image' => $cart->product->image,
            'balance' => $cart->product->balance(),
        ];
    }
}
