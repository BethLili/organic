<?php

namespace Web\Http\Controllers;

use App\Http\Requests\Web\UserUpdateRequest;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Web\Http\Requests\UpdateProfileRequest;

class ProfileController extends Controller
{
    public function show()
    {
        $user = User::find(auth('web')->id());
        return view('web.profile.profile', compact('user'));
    }

    public function update(UpdateProfileRequest $request)
    {
        Profile::query()->updateOrCreate([
            'user_id' => auth('web')->id(),
        ],[
            'user_id' => auth('web')->id(),
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'birthday' => $request->birthday,
            'mobile' => $request->mobile,
            'province' => $request->province,
            'city' => $request->city,
            'personal_info' => $request->personal_info,
        ]);

        return redirect()->action([self::class, 'show'])
            ->with('flash', 'Your profile has been saved.');
    }
}
