<?php

namespace Web\Http\Controllers;

class NotificationController extends Controller
{
    public function show()
    {
        return auth('web')->user()->unreadNotifications;
    }

    public function count()
    {
        return [
            'notificationsCount' => auth('web')->user()
                ->unreadNotifications()
                ->count(),
        ];
    }

    public function destroy($notificationId)
    {
        auth('web')->user()->notifications()->findOrFail($notificationId)->markAsRead();
    }
}
