<?php

namespace Web\Http\Controllers;

use App\Models\Rating;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    public function store(Request $request)
    {
        Rating::updateOrCreate([
            'user_id' => auth('web')->id(),
            'product_id' => $request->product_id,
        ],[
            'user_id' => auth('web')->id(),
            'product_id' => $request->product_id,
            'rating' => $request->rating,
            'review' => $request->review,
        ]);
    }

    public function destroy(Rating $rating)
    {
        $rating->delete();
    }


}
