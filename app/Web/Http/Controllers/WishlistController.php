<?php

namespace Web\Http\Controllers;

use App\Models\Product;
use App\Models\Wishlist;
use App\Notifications\ProductAddedInWishlist;
use App\Notifications\ProductRemovedFromWishlist;
use Illuminate\Http\Request;

class WishlistController extends Controller
{
    public function index()
    {
        $wishlist = Wishlist::query()->where('user_id', '=', auth('web')->id())->get();

        return $wishlist;
    }

    public function store(Request $request)
    {
        Wishlist::query()->updateOrCreate([
            'user_id' => auth('web')->id(),
            'product_id' => $request->product_id
        ],[
            'user_id' => auth('web')->id(),
            'product_id' => $request->product_id
        ]);

        $product = Product::find($request->product_id);

        auth('web')->user()->notify(new ProductAddedInWishlist($product));
    }

    public function destroy($id)
    {
        Wishlist::query()
            ->where('user_id', auth('web')->id())
            ->where('product_id', $id)
            ->delete();

        $product = Product::find($id);

        auth('web')->user()->notify(new ProductRemovedFromWishlist($product));

        return redirect('/wishlist');
    }
}
