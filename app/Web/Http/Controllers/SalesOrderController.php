<?php

namespace Web\Http\Controllers;

use App\Models\Cart;
use App\Models\Costing;
use App\Models\Product;
use App\Models\ProductLedger;
use App\Models\PurchaseOrder;
use App\Models\SalesOrder;
use Illuminate\Http\Request;
use Web\Events\SalesOrderPlaced;
use Web\Http\Requests\StoreSalesOrderRequest;
use Web\Http\Transformers\SalesOrderTransformer;

class SalesOrderController extends Controller
{
    public function index()
    {
        $orders = SalesOrder::query()->where('user_id', '=', auth('web')->id())->get();

        return $this->response->withCollection($orders, new SalesOrderTransformer());
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'orders.*.quantity' => 'required',
        ]);

        $salesOrder = SalesOrder::create([
            'user_id' => auth('web')->id(),
            'total' => $request->get('total'),
            'orders' => json_encode($request->orders),
        ]);

        Cart::where('user_id', auth('web')->id())->delete();

        event(new SalesOrderPlaced(auth('web')->user(), $salesOrder, $request));
    }
}
