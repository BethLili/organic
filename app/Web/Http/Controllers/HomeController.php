<?php

namespace Web\Http\Controllers;

use App\Models\Category;
use App\Models\Costing;
use App\Models\Product;
use App\Models\Rating;
use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $freshDeals = Product::with('category')
            ->with('costing')
            ->with('media')
            ->paginate(8);

        $reviews = Rating::where('rating', '>', 3)->take(7)->get();

        return view('web.home', compact('freshDeals', 'reviews'));
    }
}
