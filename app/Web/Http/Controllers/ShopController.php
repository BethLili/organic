<?php

namespace Web\Http\Controllers;

use App\Models\Product;
use App\Models\SalesOrder;

class ShopController extends Controller
{
    public function show($slug)
    {
        $item = Product::with('category')
            ->with('costing')
            ->with('media')
            ->with('rating')
            ->where('slug', $slug)
            ->first();

        $relatedItems = Product::with('category')
            ->with('costing')
            ->with('media')
            ->where('category_id', $item->category_id)
            ->where('id', '!=', $item->id)
            ->paginate(6);

        $orders = SalesOrder::query()->where('user_id', auth('web')->id())->get();

        $items = collect($orders)->map(function ($order) use ($item) {
            foreach (json_decode($order->orders) as $d) {
                if ($d->item == $item->id) {
                    return true;
                }
            }
            return false;
        });

        $ordered = $items->contains(true) ? true : false;

        return view('web.item', compact('item', 'relatedItems', 'ordered'));
    }
}
