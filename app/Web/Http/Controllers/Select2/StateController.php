<?php


namespace Web\Http\Controllers\Select2;

use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class StateController extends BaseController
{
    public function states(Request $request)
    {
        $roles = State::query();

        if ($request->filled('q')) {
            $roles->where('name', 'like', "%{$request->get('q')}%");
        }

        $role = $roles->paginate();

        return [
            'data' => collect($role->items())->map(function ($value) {
                return [
                    'id' => $value->id,
                    'text' => $value->name,
                ];
            }),
            'pagination' => [
                'more' => $role->hasMorePages(),
            ],
        ];
    }
}
