<?php


namespace Web\Http\Controllers\Select2;

use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class CityController extends BaseController
{
    public function cities(Request $request, $stateId)
    {
        $cities = City::query()->where('state_id', '=' ,$stateId);

        if ($request->filled('q')) {
            $cities->where('name', 'like', "%{$request->get('q')}%");
        }

        $city = $cities->paginate();

        return [
            'data' => collect($city->items())->map(function ($value) {
                return [
                    'id' => $value->id,
                    'text' => $value->name,
                ];
            }),
            'pagination' => [
                'more' => $city->hasMorePages(),
            ],
        ];
    }
}
