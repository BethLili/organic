<?php

namespace Web\Http\Controllers;

use App\Models\Cart;
use Symfony\Component\HttpFoundation\Request;
use Web\Http\Requests\StoreCartRequest;
use Web\Http\Transformers\CartTransformer;

class CartController extends Controller
{
    public function index()
    {
        $carts = Cart::query()->where('user_id', '=', auth('web')->id())->get();

        return $this->response->withCollection($carts, new CartTransformer());
    }

    public function addToCart(Request $request)
    {
        $cart = Cart::where('product_id', $request->item)->where('user_id', auth('web')->user()->id)->first();
        if ($cart) {
            if ($cart->costing->quantity > $cart->quantity) {
                $cart->increment('quantity', 1);
            }
        }
        else {
            $cart = new Cart();
            $cart->user_id = auth('web')->id();
            $cart->product_id = $request->item;
            $cart->price = $request->price;
            $cart->quantity = 1;
            $cart->save();
        }
    }

    public function incrementCart(StoreCartRequest $request)
    {
        $cart = Cart::where('product_id', '=', $request->item)->first();
        if ($cart) {
            if ($request->quantity == 0) {
                $cart->delete();
            }
            $cart->update([
                'quantity' => $request->quantity,
            ]);
        } else {
            $cart = new Cart();
            $cart->user_id = auth('web')->id();
            $cart->product_id = $request->item;
            $cart->price = $request->price;
            $cart->quantity = $request->quantity;
            $cart->save();
        }
    }

    public function destroy(Cart $cart)
    {
        $cart->delete();
    }

    public function count()
    {
        return [
            'cartCount' => Cart::query()->where('user_id', auth('web')->id())->sum('quantity')
        ];
    }
}
