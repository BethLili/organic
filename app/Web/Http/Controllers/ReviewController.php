<?php

namespace Web\Http\Controllers;

use App\Models\Rating;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function store(Request $request)
    {
        Rating::updateOrCreate([
            'user_id' => auth('web')->id(),
            'product_id' => $request->product_id,
        ],[
            'user_id' => auth('web')->id(),
            'product_id' => $request->product_id,
            'review' => $request->review,
        ]);
    }


}
