<?php

namespace Web\Http\Controllers\Api;

use App\Models\SalesOrder;
use Web\Http\Transformers\SalesOrderTransformer;

class OrderController extends Controller
{
    public function index()
    {
        $orders = SalesOrder::query()->get();

        return $this->response->withCollection($orders, new SalesOrderTransformer());
    }
}
