<?php

namespace Web\Http\Controllers\Api;

use App\Models\Costing;
use Web\Http\Transformers\CostingTransformer;

class CostingController extends Controller
{
//    moved to Web
//    public function index()
//    {
//        $shops = Costing::all();
//
//        return $this->response->withCollection($shops, new CostingTransformer());
//    }

    public function show(Costing $costing)
    {
        $costing = Costing::query()->where('id', '=', $costing->id)->get();
        return $this->response->withCollection($costing, new CostingTransformer());
    }
}
