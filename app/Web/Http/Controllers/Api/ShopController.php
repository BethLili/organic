<?php

namespace Web\Http\Controllers\Api;

use App\Models\Costing;
use Web\Http\Transformers\ShopTransformer;

class ShopController extends Controller
{
    public function index()
    {
        //FIFO
        $shops = Costing::query()->orderBy('created_at', 'DESC')->get()->keyBy('product_id');

        return $this->response->withCollection($shops, new ShopTransformer());
    }

    public function show(Costing $costing)
    {
        $costing = Costing::query()->where('id', '=', $costing->id)->get();
        return $this->response->withCollection($costing, new ShopTransformer());
    }
}
