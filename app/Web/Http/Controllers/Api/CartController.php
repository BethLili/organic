<?php

namespace Web\Http\Controllers\Api;

use App\Models\Cart;
use Web\Http\Transformers\CartTransformer;

class CartController extends Controller
{
    public function index()
    {
        $carts = Cart::query()->where('user_id', '=', auth('web')->id())->get();

        return $this->response->withCollection($carts, new CartTransformer());
    }
}
