<?php

namespace Web\Http\Controllers;

use EllipseSynergie\ApiResponse\Laravel\Response;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use League\Fractal\Manager;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $response;

    public function __construct()
    {
        $fractal = new Manager();
        if (request()->query->has('include')) {
            $fractal->parseIncludes(request()->query('include'));
        }
        $this->response = new Response($fractal);
    }
}
