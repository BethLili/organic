<?php

namespace Web\Listeners;

use App\Models\Cart;
use App\Models\Costing;
use App\Models\Inventory;
use App\Models\Product;
use App\Models\ProductLedger;
use App\Models\PurchaseOrder;
use App\Models\SalesOrder;

class DecrementProductsLedger
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(object $event)
    {
        foreach ($event->orders as $data) {
            [
                $data->item,
                $data->quantity,
            ];

            $item = Product::find($data->item);

            $ledger = new ProductLedger();
            $ledger->ledgerable_type = SalesOrder::class;
            $ledger->ledgerable_id = $event->id;
            $ledger->product_id = (int)($data->item);
            $ledger->quantity = (int)$data->quantity * -1;
            $ledger->save();

            //TODO Refactor
            //technically, data->item is wrong, it should be product_id, but since data->item and costing->id will always be the same (will they always be the same?)
            $costing = Costing::find($data->item);
            $costing->quantity = (int)($item->balance()) - (int)($data->quantity);
            $costing->save();

            //Delete from cart all items that are out of stock

            $item = Costing::where('product_id', $data->item)->first();

            if ($item->quantity < 1) {
                Cart::where('product_id', $data->item)->delete();
            }
        }
    }
}
