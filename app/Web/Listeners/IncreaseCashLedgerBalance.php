<?php

namespace Web\Listeners;

use App\Models\Cash;
use App\Models\CashDeposit;
use App\Models\CashLedger;
use App\Models\SalesOrder;

class IncreaseCashLedgerBalance
{
    public function handle(object $event)
    {
        Cash::find($event->cashId);
        $ledger = new CashLedger();
        $ledger->cashable_type = SalesOrder::class;
        $ledger->cashable_id = $event->id;
        $ledger->cash_id = $event->cashId;
        $ledger->amount = $event->amount;
        $ledger->save();
    }
}
