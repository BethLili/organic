<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Organic

## Requirements

## Installation

Install the composer dependencies:

```bash
composer install
```

Make a copy of the `.env.example` file into a `.env` file

```bash
cp .env.example .env
```

Once you have a `.env` file, configure all the necessary contents. A copy of development credentials can be obtained through our system administrator.

Once populated, run the following commands:

```bash
php artisan key:generate

php artisan storage:link
```

Set up database connection

```bash
php artisan migrate:fresh --seed
```

You'll also need to compile the Javascript and CSS assets. Yarn is the preferred method here.

```bash
yarn

yarn run dev
```

While developing, you can automatically compile the Javascript and CSS without having to run the `yarn run dev` command. Use the following to watch for changes in your JS and CSS files and automatically compile them:

```bash
yarn run watch
```

To create route
```bash
valet link
```
