<?php

use Backend\Http\Controllers\Api\CashController;
use Backend\Http\Controllers\Api\CashLedgerController;
use Backend\Http\Controllers\Api\CashPaymentController;
use Backend\Http\Controllers\Api\CategoryController;
use Backend\Http\Controllers\Api\EquityLedgerController;
use Backend\Http\Controllers\Api\NotificationController;
use Backend\Http\Controllers\Api\PayablesController;
use Backend\Http\Controllers\Api\ProductController;
use Backend\Http\Controllers\Api\ProductLedgerController;
use Backend\Http\Controllers\Api\PurchaseOrderController;
use Backend\Http\Controllers\Api\SalesOrderController;
use Backend\Http\Controllers\Api\ShopController;
use Backend\Http\Controllers\Api\StatusController;
use Backend\Http\Controllers\Api\VendorController;
use Illuminate\Support\Facades\Route;
use Web\Http\Controllers\Api\CartController;
use Web\Http\Controllers\Api\CostingController;
use Web\Http\Controllers\Api\OrderController;
use Web\Http\Controllers\Api\PaymentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

    //use in AddPurchase, products dropdown
    Route::get('products', [ProductController::class, 'index']);
    Route::get('products/{productId}', [ProductController::class, 'show']);
    Route::get('payables', [PayablesController::class, 'index']);
    Route::get('payables/{vendor}', [PayablesController::class, 'show']);

    Route::get('products-ledger', [ProductLedgerController::class, 'index']);
    Route::get('product-ledger/{productId}', [ProductLedgerController::class, 'show']);
    Route::get('product-ledger/{productId}/purchases', [ProductLedgerController::class, 'showPurchases']);
    Route::get('product-ledger/{productId}/sales', [ProductLedgerController::class, 'showSales']);


    Route::get('vendors', [VendorController::class, 'index']);
    Route::get('vendor-purchases/{vendorId}', [PurchaseOrderController::class, 'show']);
    Route::get('vendor-payments/{vendorId}', [CashPaymentController::class, 'show']);

    Route::get('categories', [CategoryController::class, 'index']);
    Route::get('purchases-statuses', [StatusController::class, 'indexPO']);
    Route::get('sales-statuses', [StatusController::class, 'indexSO']);
    Route::get('purchase-orders', [PurchaseOrderController::class, 'index']);
    Route::get('cash-ledger', [CashLedgerController::class, 'index']);
    Route::get('cash-ledger/{cash}', [CashLedgerController::class, 'show']);
    Route::get('cash-ledger/{cash}/deposits', [CashLedgerController::class, 'showDeposits']);
    Route::get('cash-ledger/{cash}/payments', [CashLedgerController::class, 'showPayments']);
    Route::get('cash', [CashController::class, 'index']);
    Route::put('cash/{cash}', [CashController::class, 'update']);

    Route::get('equity-ledger/{equity}', [EquityLedgerController::class, 'show']);
    //Route::get('shops/{product}', [ShopController::class, 'index']);

    //ledgers
    Route::get('sales-orders', [SalesOrderController::class, 'index']);
    Route::get('sales-orders/{user}', [SalesOrderController::class, 'show']);

    //WEB
    //Route::get('costing', [CostingController::class, 'index']); moved to web.php
    Route::get('/costing/{costing}', [CostingController::class, 'show']);
//    Route::get('/carts', [CartController::class, 'index']);
   Route::get('/orders', [OrderController::class, 'index']);
