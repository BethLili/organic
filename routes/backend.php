<?php

/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
|
| Here is where you can register backend routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Backend\Http\Controllers\Auth\LoginController;
use Backend\Http\Controllers\BackendUserController;
use Backend\Http\Controllers\CashController;
use Backend\Http\Controllers\CashDepositController;
use Backend\Http\Controllers\CashLedgerController;
use Backend\Http\Controllers\CashPaymentController;
use Backend\Http\Controllers\CategoryController;
use Backend\Http\Controllers\EquityController;
use Backend\Http\Controllers\HomeController;
use Backend\Http\Controllers\LedgerController;
use Backend\Http\Controllers\NotificationController;
use Backend\Http\Controllers\PayableLedgerController;
use Backend\Http\Controllers\ProductLedgerController;
use Backend\Http\Controllers\ProductController;
use Backend\Http\Controllers\PurchaseOrderController;
use Backend\Http\Controllers\ReportController;
use Backend\Http\Controllers\SalesOrderController;
use Backend\Http\Controllers\CostingController;
use Backend\Http\Controllers\UserController;
use Backend\Http\Controllers\VendorController;
use Illuminate\Support\Facades\Route;

Route::namespace('Backend\Http\Controllers\Auth')->group(function () {
    Route::get('backend/login', [LoginController::class, 'showLoginForm'])->name('backend.login');
    Route::post('backend/login', [LoginController::class, 'login']);
    Route::post('backend/logout', [LoginController::class, 'logout'])->name('backend.logout');
});

    Route::get('dashboard', [HomeController::class, 'index'])->name('dashboard');
    Route::get('utilities', [HomeController::class, 'utilities'])->name('utilities');
    Route::get('finance', [HomeController::class, 'finance'])->name('finance');
    Route::get('backend-users', [BackendUserController::class, 'index']);

//    Route::get('vendors', [VendorController::class, 'index']);
//    Route::get('vendors/create', [VendorController::class, 'create']);
//    Route::post('vendors', [VendorController::class, 'store']);
//    Route::get('vendors/{vendor}', [VendorController::class, 'show']);
//    Route::get('vendors/{vendor}/edit', [VendorController::class, 'edit']);
//    Route::put('vendors/{vendor}', [VendorController::class, 'update']);
//    Route::delete('vendors/{vendor}', [VendorController::class, 'destroy']);

Route::middleware('auth:backend')->name('backend.')->group(function () {
    Route::resource('users', UserController::class);
    Route::resource('cash', CashController::class);
    Route::resource('equity', EquityController::class);
    Route::resource('vendors', VendorController::class);
    Route::resource('categories', CategoryController::class);
    Route::resource('products', ProductController::class);
    Route::get('products/{product}/costing', [CostingController::class, 'show']);
    Route::resource('purchase-orders', PurchaseOrderController::class);
    Route::resource('sales-orders', SalesOrderController::class);
//    Route::resource('shops-ledger', ProductLedgerController::class);
    Route::post('/products-ledger/{purchaseOrder}', [ProductLedgerController::class, 'store']);
    //Route::resource('payables-ledger', PayableLedgerController::class);
    Route::resource('cash-journals', CashLedgerController::class);
    Route::resource('cash-deposits', CashDepositController::class);
    Route::resource('cash-payments', CashPaymentController::class);
    Route::put('shops/{shop}', [CostingController::class, 'update']);
    Route::get('notifications', [NotificationController::class, 'index']);
    Route::get('unread-notifications', [NotificationController::class, 'show']);
    Route::get('notifications-count', [NotificationController::class, 'count']);
    Route::delete('notifications/{notificationId}', [NotificationController::class, 'destroy']);

    //ledgers
    Route::get('ledgers/sales', [LedgerController::class, 'sales']);
    Route::get('sales/datatables', [LedgerController::class, 'salesDatatables']);
    Route::get('ledgers/purchases', [LedgerController::class, 'purchases']);
    Route::get('purchases/datatables', [LedgerController::class, 'purchasesDatatables']);
    Route::get('ledgers/payables', [LedgerController::class, 'payables']);
    Route::get('payables/datatables', [LedgerController::class, 'payablesDatatables']);
    Route::get('ledgers/cash', [LedgerController::class, 'cash']);
    Route::get('bank/datatables', [LedgerController::class, 'bankDatatables']);

    //reports
    Route::get('reports/profit-and-loss', [ReportController::class, 'profitAndLoss']);
    Route::post('reports/generate/profit-and-loss', [ReportController::class, 'generateProfitAndLoss']);
    Route::get('reports/balance-sheet', [ReportController::class, 'balanceSheet']);
    Route::post('reports/generate/balance-sheet', [ReportController::class, 'generateBalanceSheet']);

    //vendors-view
    Route::get('vendors/purchase-orders/{purchaseOrder}', [PurchaseOrderController::class, 'showVendor']);
    Route::get('/purchase-orders/{purchaseOrder}/download',[PurchaseOrderController::class, 'download']);
});


