<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Web\Http\Controllers\CartController;
use Web\Http\Controllers\CostingController;
use Web\Http\Controllers\HomeController;
use Web\Http\Controllers\NotificationController;
use Web\Http\Controllers\PaymentController;
use Web\Http\Controllers\ProfileController;
use Web\Http\Controllers\RatingController;
use Web\Http\Controllers\ReviewController;
use Web\Http\Controllers\SalesOrderController;
use Web\Http\Controllers\Select2\CityController;
use Web\Http\Controllers\Select2\StateController;
use Web\Http\Controllers\ShopController;
use Web\Http\Controllers\WishlistController;

Route::namespace('\Web\Http\Controllers')->group(function () {
    Auth::routes([
        'verify' => true,
    ]);
});

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::view('/shops', 'web.shops');
Route::get('/item/{slug}', [ShopController::class, 'show']);

Route::middleware('auth:web')->group(function () {
    Route::view('/carts', 'web.carts');
    Route::view('/orders', 'web.orders');
//Route::resource('/carts', CartController::class);
    Route::put('/carts', [CartController::class, 'addToCart']);
    Route::put('/wishlist', [WishlistController::class, 'store']);
    Route::delete('/wishlist/{id}', [WishlistController::class, 'destroy']);
    Route::put('/shop', [CartController::class, 'incrementCart']);
    Route::delete('/carts/{cart}', [CartController::class, 'destroy']);
    Route::resource('/web/sales-orders', SalesOrderController::class);

//PROFILE
    Route::get('/profile', [ProfileController::class, 'show']);
    Route::put('/profile', [ProfileController::class, 'update']);
    Route::get('states', [StateController::class, 'states'])->name('states');
    Route::get('cities/{stateId}', [CityController::class, 'cities'])->name('cities');
    Route::view('/wishlist', 'web.profile.wishlist');
    Route::view('/get-notifications', 'web.profile.notifications');
    Route::view('/reviews', 'web.profile.reviews');

    Route::post('/rating', [RatingController::class, 'store']);
    Route::post('/review', [ReviewController::class, 'store']);
    Route::delete('/rating/{rating}', [RatingController::class, 'destroy']);

    Route::get('cart-count', [CartController::class, 'count']);

    Route::get('notifications', [NotificationController::class, 'index']);
    Route::get('user-unread-notifications', [NotificationController::class, 'show']);
    Route::get('user-notifications-count', [NotificationController::class, 'count']);
    Route::delete('user-notifications/{notificationId}', [NotificationController::class, 'destroy']);

});

//apis
Route::get('/api/carts', [CartController::class, 'index']);
Route::get('/api/costing', [CostingController::class, 'index']);
Route::get('/api/wishlist', [WishlistController::class, 'index']);
Route::get('/api/orders', [SalesOrderController::class, 'index']);
