@extends('web.layouts.app')

@section('content')
    <div class="container-fluid custom-login-container" style="min-height: 80vh;">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-lg-4 ml-auto">
                <div class="custom-login-form-container p-4">
                    <div class="h1 font-accent my-3 text-center">{{ __('Reset Password') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email"
                                       class="col-12 col-form-label">{{ __('E-Mail Address') }}</label>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <input id="email" type="email"
                                           class="form-control bg-transparent custom-form-control @error('email') is-invalid @enderror" name="email"
                                           value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-12">
                                    <button type="submit" class="btn-block custom-button">
                                        {{ __('Send Password Reset Link') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
