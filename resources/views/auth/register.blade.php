@extends('web.layouts.app')

@section('content')
<div class="container-fluid custom-login-container pb-4">
    <div class="row justify-content-center">
        <div class="col-12 col-md-6 col-lg-4 ml-auto">
            <div class="custom-register-form-container p-4">
                <div class="h1 font-accent my-3 text-center">{{ __('Sign up now to start shopping') }}</div>

                <div class="">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-12">
                                <label for="name" class="col-12 col-form-label">{{ __('Username') }}</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <input id="name" type="text" class="form-control bg-transparent custom-form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">
                                <label for="email" class="col-12 col-form-label">{{ __('E-Mail Address') }}</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <input id="email" type="email" class="form-control bg-transparent custom-form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">
                                <label for="password" class="col-12 col-form-label">{{ __('Password') }}</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <input id="password" type="password" class="form-control bg-transparent custom-form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-12 col-form-label">{{ __('Confirm Password') }}</label>

                            <div class="col-12">
                                <input id="password-confirm" type="password" class="form-control bg-transparent custom-form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-12">
                                <button type="submit" class="btn-block custom-button">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-12">
                                Already got an account?
                                <a class="btn btn-link" href="{{ route('login') }}">
                                    {{ __('Sign in') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
