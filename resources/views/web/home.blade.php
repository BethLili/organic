@extends('web.layouts.app')

@section('content')
    <div class="container-fluid d-none d-md-block">

        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="{{ asset('assets/web/images/home/slider1.jpg') }}" class="w-100" alt="">
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('assets/web/images/home/slider2.jpg') }}" class="w-100" alt="">
                </div>

            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    <h1 class="text-center text-gray-dark font-accent text-color-accent mt-4">Fresh Deals</h1>
    <div class="products container">
        <div class="row justify-content-center">
            @foreach($freshDeals as $key => $freshDeal)
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 px-4 px-sm-3 mb-4 {{ $key > 3 ? "d-none d-md-block" : "" }}">
                    <div class="card prize-card h-100 border-0">
                        <div class="text-center">
                            <a href="/item/{{ $freshDeal->slug }}" class="nav-link">
                                <img src="{{ $freshDeal->image }}" alt="{{ $freshDeal->name }}"
                                     class="w-75 my-2">
                            </a>
                        </div>
                        <div class="card-body text-center">
                            <h4 class="card-text font-weight-bolder font-accent">{{ $freshDeal->name }}</h4>
                            <p class="mb-1 text-color-gray-dark"><small>{{ $freshDeal->description }}</small></p>

                        </div>
                        <div class="units-counter">
                            <p class="text-white">{{ $freshDeal->balance }} {{ $freshDeal->balance < 1 ? 'unit' :
                                'units' }}
                                <br>
                                left</p>
                        </div>

                        <a href="/item/{{ $freshDeal->slug }}"
                           class="nav-link font-weight-bolder text-center border-0 text-white p-0 bbr-3 p-2 d-flex align-items-center justify-content-center custom-button">
                            View Details
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="row m-1">
        <div class="col-12 col-md-8 p-4 mx-auto">
            <div class="widget p-4">
                <h1 class="text-center text-gray-dark font-accent text-color-accent my-4">About Us</h1>
              <div class="row justify-content-center">
                  <div class="col-12 col-md-6">
                      <p class="text-center">
                          <img class="w-75" src="https://www.niche.com/blog/wp-content/uploads/2019/01/importance-of-community-service-1200-1200x794.jpg" alt="">
                      </p>
                  </div>
                  <div class="col-12 col-md-6 font-regular">
                      <p>Organic is an online grocery dedicated to providing fresh produce to our long time customers. We are built on 1988 and has been providing world class quality service since then.</p>
                      <p>Our partners work non-stop to find the best ways and to always stay on top of our game.</p>
                      <p>Aside from our dedication to provide the best service, we are also committed to the community. We have scholarship projects and community services. We also have a program that "for every kilo you buy, we give another kilo to a family in need."</p>
                  </div>
              </div>

            </div>
        </div>
    </div>

    @if ($reviews->isNotEmpty())
        <h1 class="text-center text-gray-dark font-accent text-color-accent my-4">Here's what our customers has to say</h1>
        <div class="products  pb-4">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    @foreach($reviews as $key => $review)
                        <li data-target="#carouselExampleIndicators" data-slide-to="{{ $key }}"
                            class="{{ $key === 0 ? "active" : "" }} bg-color-accent"></li>
                    @endforeach
                </ol>
                <div class="carousel-inner">
                    @foreach($reviews as $key => $review)
                        <div class="carousel-item {{ $key === 0 ? "active" : "" }}">
                            <div class="row mx-4">
                                <div class="col-12 col-md-6 my-3 mx-auto">
                                    <div class="widget p-4 text-center">
                                           <vue-star-rating
                                               :rating="{{ $review->rating }}"
                                               :show-rating="false"
                                               :star-size="15"
                                               :read-only="true"
                                               :increment=".5"
                                           class="mx-auto col-6 col-md-4"></vue-star-rating>

                                        <p class="text-center"><i class="fas fa-quote-left text-secondary"></i>
                                            <span class="font-regular"> {{ $review->review }}</span>
                                            <i class="fas fa-quote-right text-secondary">
                                            </i>
                                        </p>

                                        <p class="notification-body mb-1">
                                            <small><i
                                                    class="fas fa-user-edit text-secondary mr-2"></i>{{ $review->user->name }}
                                                , {{ $review->created_at->diffForHumans() }}</small>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    @endif
@endsection
