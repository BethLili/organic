@extends('web.layouts.app')

@section('content')
    <div class="mt-4 container p-sm-3 px-0" style="min-height: 90vh;">
       <shops :auth="{{ auth('web')->check() ? "true" : "false" }}"></shops>
    </div>
@endsection
