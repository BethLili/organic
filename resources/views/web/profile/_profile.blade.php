<h1 class="text-center font-accent text-color-accent mt-4">My Profile</h1>
<form action="/profile" method="POST">
    @csrf @method('PUT')

    <div class="bg-white">
        <div class="row mt-4">
            <div class="col-12 add_top_30">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Name</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text"
                                   class="form-control"
                                   placeholder="Your name"
                                   value="{{ auth('web')->user()->profile ? auth('web')->user()->profile->first_name : old('first_name') }}"
                                   name="first_name">
                            @if( $errors->has('first_name'))
                                <div
                                    class="text-danger">{{ $errors->first('first_name') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Last name</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text"
                                   class="form-control"
                                   placeholder="Your last name"
                                   value="{{ auth('web')->user()->profile ? auth('web')->user()->profile->last_name : old('last_name') }}"
                                   name="last_name">
                            @if( $errors->has('last_name'))
                                <div class="text-danger">{{ $errors->first('last_name') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- /row-->
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Mobile</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text"
                                   class="form-control"
                                   placeholder="09xx xxx xxxx"
                                   value="{{ auth('web')->user()->profile ? auth('web')->user()->profile->mobile : old('mobile') }}"
                                   name="mobile">
                            @if( $errors->has('mobile'))
                                <div class="text-danger">{{ $errors->first('mobile') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Birthday</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="date"
                                   class="form-control"
                                   name="birthday"
                                   value="{{ auth('web')->user()->profile ? auth('web')->user()->profile->birthday : old('birthday') }}"
                                   placeholder="Your birthday">
                            @if( $errors->has('birthday'))
                                <div class="text-danger">{{ $errors->first('birthday') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Province</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <select class="js-select2-states form-control"
                                    id="province-select2-multiple" name="province"
                                    style="width: 100%;" data-placeholder="Choose province.."
                                    onchange="setState(this)">
                                @if(!empty(auth('web')->user()->profile->state))
                                    <option selected="selected">
                                        {{ auth('web')->user()->profile->state->name }}
                                    </option>
                                @endif
                            </select>
                            @if( $errors->has('province'))
                                <div class="text-danger">{{ $errors->first('province') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>City</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <select class="js-select2-cities form-control"
                                    id="city-select2-multiple"
                                    name="city"
                                    style="width: 100%;" data-placeholder="Choose city..">
                                @if(!empty(auth('web')->user()->profile->place))
                                    <option selected="selected">
                                        {{ auth('web')->user()->profile->place->name}}
                                    </option>
                                @endif
                            </select>
                            @if( $errors->has('city'))
                                <div class="text-danger">{{ $errors->first('city') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- /row-->
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Personal info</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                                            <textarea style="height:100px;"
                                                      class="form-control"
                                                      placeholder="Personal info"
                                                      name="personal_info">
                                                {{ auth('web')->user()->profile ? auth('web')->user()->profile->personal_info : old('personal_info') }}
                                            </textarea>
                            @if( $errors->has('personal_info'))
                                <div
                                    class="text-danger">{{ $errors->first('personal_info') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- /row-->
                <div class="form-group">
                    <button class="custom-button">Update Profile</button>
                </div>

            </div>
        </div>
    </div>
    <!-- /box_general-->
</form>
