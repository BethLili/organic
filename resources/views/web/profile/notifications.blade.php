@extends('web.layouts.app')

@section('content')
    <div class="container" style="min-height: 90vh;">

        <div class="row mt-4">
            <div class="col-12 col-md-3 mr-auto custom-margin-top">
                @include('web.profile.profile_sidebar')
            </div>

            <div class="col-md-9 mx-auto">
                <div class="col-12 tab-content" id="pills-profileContent">

                    <div class="tab-pane fade show active mb-3" id="pills-profile" role="tabpanel"
                         aria-labelledby="pills-profile-tab">
                        <notification-list :notifications="{{ auth('web')->user()->notifications }}"></notification-list>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
