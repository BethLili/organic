@extends('web.layouts.app')

@section('css_before')
    <link rel="stylesheet" href="{{ asset('assets/web/plugins/select2/css/select2.css') }}">
@endsection

@section('content')
    <div class="container" style="min-height: 90vh;">

        <div class="row mt-4">
            <div class="col-12 col-md-3 mr-auto custom-margin-top">
                <ul class="nav flex-column nav-pills w-100 p-0" id="profileTab"
                    role="tablist">

                    <li class="nav-item px-0">
                        <a class="nav-link active" id="pills-profile-tab" data-toggle="pill"
                           href="#pills-profile"
                           role="tab" aria-controls="pills-profile" aria-selected="true">
                            Profile
                        </a>
                    </li>

                    <li class="nav-item px-0">
                        <a class="nav-link" id="pills-notifications-tab" data-toggle="pill"
                           href="#pills-notifications"
                           role="tab" aria-controls="pills-notifications" aria-selected="true">
                            Notifications
                        </a>
                    </li>

                    <li class="nav-item px-0">
                        <a class="nav-link" id="pills-reviews-tab" data-toggle="pill" href="#pills-reviews"
                           role="tab"
                           aria-controls="pills-reviews" aria-selected="false">
                            Reviews
                        </a>
                    </li>

                    <li class="nav-item px-0">
                        <a class="nav-link" id="pills-wishlist-tab" data-toggle="pill"
                           href="#pills-wishlist"
                           role="tab"
                           aria-controls="pills-wishlist" aria-selected="false">
                            Wishlist
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-md-9 mx-auto">
                <div class="col-12 tab-content" id="pills-profileContent">

                    <div class="tab-pane fade show active mb-3" id="pills-profile" role="tabpanel"
                         aria-labelledby="pills-profile-tab">
                        @include('web.profile._profile')
                    </div>

                    <div class="tab-pane fade mb-3" id="pills-notifications" role="tabpanel"
                         aria-labelledby="pills-notifications-tab">
                        <notification-list :notifications="{{ $notifications }}"></notification-list>
                    </div>

                    <div class="tab-pane fade p-1" id="pills-reviews" role="tabpanel"
                         aria-labelledby="pills-reviews-tab">
                        <reviews-list :reviews="{{ $ratings }}"></reviews-list>
                    </div>

                    <div class="tab-pane fade p-4" id="pills-wishlist" role="tabpanel"
                         aria-labelledby="pills-wishlist-tab">
                        @include('web.profile._wishlist')
                    </div>

                </div>

            </div>
        </div>
    </div>

@endsection

@section('js_after')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{asset('assets/web/plugins/select2/js/select2.full.min.js')}}" defer></script>
    <script>

        jQuery(function () {
            $('.js-select2-states').select2({
                ajax: {
                    url: '{{ action([\Web\Http\Controllers\Select2\StateController::class , 'states']) }}',
                    dataType: 'json',
                    processResults: function (data) {
                        let a = $('#province-select2-multiple').val()
                        return {
                            results: data.data,
                            pagination: {
                                more: data.pagination.more
                            }
                        }
                    }
                }
            })
        })

        function setState(e) {
            let stateId = e.value
            $('.js-select2-cities').select2({
                ajax: {
                    url: '/cities',
                    data: stateId,
                    dataType: 'json',
                    processResults: function (data) {
                        return {
                            results: data.data,
                            pagination: {
                                more: data.pagination.more
                            }
                        }
                    }
                }
            })
        }

    </script>
    <script>

        function setState(e) {
            let stateId = e.value
            $('.js-select2-cities').select2({
                ajax: {
                    url: `/cities/${stateId}`,
                    dataType: 'json',
                    processResults: function (data) {
                        return {
                            results: data.data,
                            pagination: {
                                more: data.pagination.more
                            }
                        }
                    }
                }
            })
        }

    </script>

    <script>
        jQuery(function () {
            let hash = window.location.hash
            if (hash != '') {
                let href = ''

                $('#profileTab li a').each(function () {
                    href = $(this).attr('href')
                    if (href == hash) {
                        $(this).tab('show')
                    }
                })
            }
        })
    </script>
@endsection
