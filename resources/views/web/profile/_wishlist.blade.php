<h1 class="text-center font-accent text-color-accent my-4">My Wishlist</h1>
<div class="products">
    <div class="row justify-content-center">
        @forelse(auth('web')->user()->wishlist()->get() as $item)
            <div class="col-6 col-md-3 px-1 px-sm-1 mb-4">
                <div class="card prize-card h-100 border-0">
                    <div class="text-center">
                        <a href="item/{{$item->product->slug }}" class="nav-link">
                            <img src="{{ $item->product->image }}" alt="{{ $item->product->name }}" class="w-100">
                        </a>
                    </div>
                    <div class="card-body text-center">
                        <small
                            class="card-text font-regular font-weight-bold text-color-gray-dark">{{ $item->product->name }}</small>
                    </div>
                    <form action="/wishlist/{{$item->product->id}}" method="post">
                        @csrf @method('delete')
                        <button type="submit" class="btn-block font-weight-bolder text-center border-0 text-white p-0 custom-button bbr-3
                    p-2 d-flex align-items-center justify-content-center text-uppercase">
                            Remove
                        </button>
                    </form>
                </div>
            </div>
        @empty
            <div class="text-center my-4 col-12">
                <div class="item-card my-2 p-4">
                    <p>You don't any items in your wishlist yet.</p>
                </div>
            </div>
        @endforelse
    </div>
</div>


