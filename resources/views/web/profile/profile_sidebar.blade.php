<ul class="nav flex-column nav-pills w-100 p-0" id="profileTab"
    role="tablist">

    <li class="nav-item px-0">
        <a class="nav-link {{ request()->is('profile') ? "active" : "" }}" id="pills-profile-tab"
           href="/profile">
            Profile

        </a>
    </li>

    <li class="nav-item px-0">
        <a class="nav-link {{ request()->is('get-notifications') ? "active" : "" }}"
           href="/get-notifications">
            Notifications
        </a>
    </li>

    <li class="nav-item px-0">
        <a class="nav-link {{ request()->is('reviews') ? "active" : "" }}" id="pills-reviews-tab" href="/reviews">
            Reviews
        </a>
    </li>

    <li class="nav-item px-0">
        <a class="nav-link {{ request()->is('wishlist') ? "active" : "" }}" id="pills-wishlist-tab"
           href="/wishlist">
            Wishlist
        </a>
    </li>
</ul>
