@extends('web.layouts.app')

@section('content')
    <div class="my-4 container p-sm-3 px-0">
        <a href="{{ url()->previous() }}" class="nav-link">
            <i class="fas fa-arrow-left"></i>
            Back
        </a>

        @auth('web')
        <item :item="{{ $item }}"
              :score="{{ $item->rating->isNotEmpty() ? $item->rating->average('rating') : 0 }}"
              :review-count="{{ $item->rating->isNotEmpty() ? $item->rating->count() : 0 }}"
              :added-to-wishlist="{{ auth('web')->user()->itemOnWishlist($item->id) ? 'true' : 'false' }}"
              :added-to-cart="{{ auth('web')->user()->itemOnCart($item->id) ? 'true' : 'false' }}"
              :auth="{{ auth('web')->check() ? "true" : "false" }}"
        ></item>
        @else
            <item :item="{{ $item }}"
                  :score="{{ $item->rating->isNotEmpty() ? $item->rating->average('rating') : 0 }}"
                  :review-count="{{ $item->rating->isNotEmpty() ? $item->rating->count() : 0 }}"
                  :added-to-wishlist="{{'false'}}"
                  :added-to-cart="{{'false'}}"
                  :auth="{{ auth('web')->check() ? "true" : "false" }}"
            ></item>
        @endauth

        <div class="m-4">
            <h4 class="card-text font-weight-bolder font-regular">Related Products</h4>
            <div class="row justify-content-center">
                @forelse($relatedItems as $product)
                    <related-item
                        :related-item="{{ $product }}"
                    ></related-item>
                @empty
                    <p>No related items.</p>
                @endforelse
            </div>
        </div>

        <div class="m-4" id="reviews">
            <h4 class="card-text font-weight-bolder font-regular">Product Ratings & Reviews</h4>
            @auth('web')
                @if (auth('web')->user()->rating->where('product_id', $item->id)->isEmpty() && $ordered)
                    <rating :item="{{ $item }}"></rating>
                @endif
            @endauth

            <div class="row justify-content-center">
                @forelse($item->rating as $rate)

                    <div id="rate-{{ $rate->id }}"></div>

                       @if ($rate->user_id == auth('web')->id())
                           <edit-rating :rate="{{ $rate }}"
                                        :user="'{{$rate->user->name }}'"
                                        :item="{{ $item }}"></edit-rating>
                       @else
                           <div class="col-12">
                               <div class="col-lg-6 d-flex justify-content-between mr-auto align-items-sm-start">
                                   <div>
                                       <vue-star-rating
                                           :rating="{{ $rate->rating }}"
                                           :show-rating="false"
                                           :star-size="15"
                                           :read-only="true"
                                           :increment=".5"
                                       ></vue-star-rating>
                                       @if ($rate->review)
                                       <p><small><i class="fas fa-quote-left text-secondary"></i> {{ $rate->review }} <i class="fas fa-quote-right text-secondary"></i></small></p>
                                       @else
                                           <p></p>
                                       @endif
                                       <small><i class="fas fa-user-edit text-secondary mr-2"></i>{{ $rate->user->name }}, {{ $rate->created_at->diffForHumans() }}</small>
                                   </div>
                               </div>
                               <hr class="w-50">
                           </div>
                       @endif
                @empty
                    <p>No ratings yet.</p>
                @endforelse
            </div>
        </div>

    </div>
@endsection
