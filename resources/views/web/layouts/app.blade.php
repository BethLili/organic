<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Organic') }}</title>

    <!--Font Awesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('css_before')
<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kaushan+Script&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=ABeeZee&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

<!-- CSS -->
    <link rel="stylesheet" href="{{ mix('assets/web/css/app.css') }}">
</head>
<body>
<div id="app">

     @include('web.layouts.nav')

    <main class="">
        @yield('content')
    </main>

    @include('web.layouts.footer')

    <flash message="{{ session('flash') }}"></flash>
</div>

@yield('js_after')

<script src="{{ mix('assets/web/js/app.js') }}"></script>

</body>
</html>
