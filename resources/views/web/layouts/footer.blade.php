<!-- Footer -->
{{--<footer class="footer font-small blue pt-4 bg-color-primary-dark" style="position:fixed; left: 0; bottom: 0; width: 100%">--}}
    <footer class="footer font-small blue pt-4 bg-color-primary-dark">

    <div class="container-fluid text-center text-md-left text-white">
        <div class="row">
            <div class="col-12 mt-md-0 mt-3 mx-auto">
                <p class="ml-4 text-center">
                    <small><a class="text-white" href="{{ url('/') }}">{{ config('app.name', 'Organic') }}</a></small> |
                    <small>93 P Santos, Quezon City, Metro Manila</small> |
                    <small><a href="mailto:info@organic.com" class="nav-item text-white">info@organic.com</a></small> |
                    <small>+63 981 111 1122</small>
                </p>
            </div>
            <div class="col-12 mt-md-0 mt-3 mx-auto">
                <p class="ml-4 text-center">
                    <small><a class="text-white" href="/">Home</a></small> |
                    <small><a class="text-white" href="/shops">Shop</a></small> |
                    <small><a class="text-white" href="/cart">Cart</a></small> |
                    <small><a class="text-white" href="/orders">My Orders</a></small>
                </p>
            </div>
        </div>
    </div>

</footer>
<!-- Footer -->
