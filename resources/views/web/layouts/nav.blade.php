<nav class="navbar navbar-expand-lg navbar-light bg-color-primary-dark sticky-top">
    <a class="organic-title font-accent nav-link text-white" href="{{ url('/') }}">{{ config('app.name', 'Organic') }}</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a href="/shops" class="nav-link text-white">
                    Shop
                </a>
            </li>

        @auth('web')
                <li class="nav-item">
                    <a href="/carts" class="nav-link text-white">
                        Cart
                        <user-cart-count></user-cart-count>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/orders" class="nav-link text-white">
                        My Orders
                    </a>
                </li>

                <notifications></notifications>

                <li class="nav-item dropdown text-white">
                    <a id="navbarDropdown" class="nav-link text-white dropdown-toggle" href="#" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }}
                        <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right custom-dropdown" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="/profile">
                            My Profile
                        </a>
                        <a class="dropdown-item" href="/reviews">
                            My Reviews
                        </a>
                        <a class="dropdown-item" href="/wishlist">
                            My Wishlist
                        </a>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link text-white" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @endauth
        </ul>
    </div>
</nav>
