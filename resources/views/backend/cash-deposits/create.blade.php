@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center my-4">
            <div class="col-md-10 my-4">
                <form action="{{ action([\Backend\Http\Controllers\CashDepositController::class, 'store']) }}"
                      method="post">
                    @csrf
                    <div class="widget rounded">
                        <div class="card-header">
                            <div class="d-flex justify-content-between">
                                <h5>Add new deposit</h5>
                                <div class="form-group row">
                                    <div class="col-md-9">
                                        <button type="submit" class="btn btn-outline-warning">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body p-4">
                            <div class="row">
                                <div class="mx-4">
                                    <h5>From</h5>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <select name="equity_id" class="form-control" required>
                                                <option value="">Please select shareholders account</option>
                                                @foreach($shareholders as $equity)
                                                    <option value="{{  old('cash_id') && old('cash_id') == $equity->id ? old('cash_id') : $equity->id }}" {{ old('equity_id') && old('equity_id') == $equity->id ? "selected" : "" }}>{{ ucwords(strtolower($equity->name)) }}</option>
                                                    @if( $errors->has('equity_id'))
                                                        <div
                                                            class="text-danger">{{ $errors->first('equity_id') }}</div>
                                                    @endif
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row p-4">
                            <div class="col-md-4">
                                <select name="cash_id" class="form-control" required>
                                    <option value="">Please select account</option>
                                    @foreach($banks as $cash)
                                        <option value="{{ old('cash_id') && old('cash_id') == $cash->id ? old('cash_id') : $cash->id }}" {{ old('cash_id') && old('cash_id') == $cash->id ? "selected" : "" }}>{{ ucwords(strtolower($cash->name)) }}</option>
                                        @if( $errors->has('cash_id'))
                                            <div
                                                class="text-danger">{{ $errors->first('cash_id') }}</div>
                                        @endif
                                    @endforeach
                                </select>
                                <label for="cash_id">Deposit to</label>
                            </div>
                            <div class="col-md-4">
                                <div
                                    class="form-material {{ $errors->has('amount') ? 'form-material-danger' : ''}}">
                                    <input required
                                           type="text"
                                           class="form-control"
                                           id="amount"
                                           name="amount"
                                           value="{{ old('amount') }}"
                                    placeholder="Amount">
                                    <label for="">Amount</label>
                                </div>
                                @if( $errors->has('amount'))
                                    <div class="text-danger">{{ $errors->first('amount') }}</div>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <div
                                    class="form-material {{ $errors->has('description') ? 'form-material-danger' : ''}}">
                                    <input required
                                           type="text"
                                           class="form-control"
                                           id="description"
                                           name="description"
                                           value="{{ old('description') }}"
                                           placeholder="Purpose of deposit">
                                    <label for="description">Purpose of deposit</label>
                                </div>
                                @if( $errors->has('description'))
                                    <div class="text-danger">{{ $errors->first('description') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
