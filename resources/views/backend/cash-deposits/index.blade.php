@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center my-4">
            <div class="col-md-10 my-4">
                <div class="block">
                    <div class="block-header block-header-default d-flex justify-content-between">
                        <h3 class="block-title font-weight-bolder"> </h3>
                        <div class="btn-group text-center">
                            <a href="{{ action([\Backend\Http\Controllers\CashDepositController::class, 'create']) }}"
                               class="btn btn-sm btn-info">
                                Add Deposit
                            </a>
                        </div>
                    </div>

                    <div class="block-content">
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="details" role="tabpanel"
                                 aria-labelledby="details-tab">
                                <div class="widget my-4">
                                    <div class="card-header font-weight-bolder d-flex justify-content-between">
                                        Deposit Transactions
                                    </div>

                                    <div class="card-body">
                                        <table class="table table-vcenter text-center">
                                            <thead>
                                            <tr>
                                                <th class="d-none d-md-table-cell">Date</th>
                                                <th class="d-none d-md-table-cell">Bank</th>
                                                <th class="">Shareholder</th>
                                                <th class="d-none d-md-table-cell">Purpose</th>
                                                <th>Amount</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @forelse ($deposits as $deposit)
                                                <tr>
                                                    <td class="d-none d-md-table-cell">{{ $deposit->created_at->diffForHumans() }}</td>
                                                    <td class="d-none d-md-table-cell"><a href="{{ $deposit->cash->path() }}">{{ ucwords(strtolower($deposit->cash->bank)) }}</a></td>
                                                    <td>
                                                        <a href="{{ $deposit->equity->path() }}">{{ ucwords(strtolower($deposit->equity->name)) }}</a>
                                                        <br>
                                                        <small class="d-md-none"><a href="{{ $deposit->cash->path() }}">{{ ucwords(strtolower($deposit->cash->bank)) }}</a></small>
                                                    </td>
                                                    <td class="d-none d-md-table-cell text-wrap">{{ $deposit->description }}</td>
                                                    <td>{{ number_format($deposit->amount) }}<br>
                                                        <small class="d-md-none">{{ $deposit->created_at->diffForHumans() }}</small>
                                                    </td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="5" class="text-center"><p>No data.</p></td>
                                                </tr>
                                            @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
