@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center my-4">
            <div class="col-md-12 my-4">

                <div class="widget p-4 bg-white rounded">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div class="row">
                                <div class="mx-4">
                                    <h5>From</h5>
                                    <a href="{{ $cashDeposit->equity->path()  }}">{{ ucwords(strtolower($cashDeposit->equity->name)) }}</a>
{{--                                    <p><small>{{ $cashDeposit->vendor->address  }} <br>--}}
{{--                                            {{ $cashDeposit->vendor->email  }} <br>--}}
{{--                                            {{ $cashDeposit->vendor->contact  }}</small></p>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <table class="table table-vcenter text-center">
                            <thead>
                            <tr>
                                <th>Date Deposited</th>
                                <th class="d-none d-md-table-cell">Deposited to</th>
                                <th class="">Description</th>
                                <th class="">Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $cashDeposit->created_at->format('d M Y')  }}</td>
                                <td class="d-none d-md-table-cell">{{ ucwords(strtolower($cashDeposit->cash->name))  }}</td>
                                <td>{{ $cashDeposit->description  }}</td>
                                <td><strong>{{ number_format($cashDeposit->amount, 2) }}</strong></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
