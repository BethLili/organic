@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center my-4">
            <div class="col-md-10 my-4">

                <div class="block">
                    <div class="block-header block-header-default">
                        <h4 class="block-title font-weight-bold">Edit Vendor Information</h4>
                    </div>
                    <div class="block-content">
                        <form action="{{ action([\Backend\Http\Controllers\VendorController::class, 'update'], $vendor) }}" method="post">
                            @csrf @method('PUT')
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="form-material {{ $errors->has('name') ? 'form-material-danger' : ''}}">
                                        <label for="name">Name</label>
                                        <input required
                                               type="text"
                                               class="form-control"
                                               id="name"
                                               name="name"
                                               value="{{ old('name') ? old('name') : $vendor->name}}">
                                    </div>
                                    @if( $errors->has('name'))
                                        <div class="text-danger">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="form-material {{ $errors->has('terms') ? 'form-material-danger' : ''}}">
                                        <label for="terms">Terms</label>
                                        <input required
                                               type="text"
                                               class="form-control"
                                               id="terms"
                                               name="terms"
                                               value="{{ old('terms') ? old('terms') : $vendor->terms}}">
                                    </div>
                                    @if( $errors->has('terms'))
                                        <div class="text-danger">{{ $errors->first('terms') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-6">
                                    <div class="form-material {{ $errors->has('email') ? 'form-material-danger' : ''}}">
                                        <label for="email">Email</label>
                                        <input required
                                               type="email"
                                               class="form-control"
                                               id="email"
                                               name="email"
                                               value="{{ old('email') ? old('email') : $vendor->email }}">
                                    </div>
                                    @if( $errors->has('email'))
                                        <div class="text-danger">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>
                                <div class="col-6">
                                    <div class="form-material">
                                        <label for="contact">Contact Number</label>
                                        <input required
                                               type="text"
                                               class="form-control"
                                               id="contact"
                                               name="contact"
                                               value="{{ old('contact') ? old('contact') : $vendor->contact }}">
                                    </div>
                                    @if( $errors->has('contact'))
                                        <div class="text-danger">{{ $errors->first('contact') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-material form-material-danger">
                                        <label for="address">Address</label>
                                        <input required
                                               type="text"
                                               class="form-control"
                                               id="address"
                                               name="address"
                                               value="{{ old('address') ? old('address') : $vendor->address }}">
                                    </div>
                                    @if( $errors->has('address'))
                                        <div class="text-danger">{{ $errors->first('address') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-material">
                                        <label for="description">Description</label>
                                        <textarea required
                                                  class="form-control"
                                                  id="description"
                                                  name="description"
                                                  rows="3"
                                        >{{ old('description') ? old('description') : $vendor->description }}
                                        </textarea>
                                        <small class="form-text text-muted text-right">Please provide as much vendor details as possible.</small>
                                    </div>
                                    @if( $errors->has('description'))
                                        <div class="text-danger">{{ $errors->first('description') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-9">
                                    <button type="submit" class="btn btn-outline-warning">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
