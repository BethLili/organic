@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center my-4">
            <div class="col-md-10 my-4">
                <div class="block">
                    <div class="block-header block-header-default d-flex justify-content-between flex-wrap">
                        <nav>
                            <div class="nav nav-tabs custom-nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active"
                                   id="details-tab"
                                   data-toggle="tab" role="tab"
                                   href="#details" aria-controls="details" aria-selected="true">
                                    <strong class="block-title font-weight-bolder"> {{ ucfirst($vendor->name) }}</strong>
                                </a>
                                <a class="nav-item nav-link"
                                   id="po-tab"
                                   data-toggle="tab" role="tab"
                                   href="#po"
                                   aria-controls="po" aria-selected="true">
                                    Purchases
                                </a>
                                <a class="nav-item nav-link"
                                   id="payments-tab"
                                   data-toggle="tab" role="tab"
                                   href="#payments"
                                   aria-controls="payments" aria-selected="true">
                                    Payments
                                </a>
                            </div>
                        </nav>

                        <div class="col-12 col-md-2 mt-4 mt-md-0" role="group">
                            <p class="text-right">
                                <a href="{{ $vendor->path() }}/edit" title="edit"
                                   class="btn btn-sm btn-info px-4">Edit</a>
                            @if($vendor->purchaseOrders->isEmpty())
                                <delete-button :id="{{ $vendor->id }}"
                                               :name="'{{ $vendor->name }}'"
                                               :path="'{{ $vendor->path() }}'"
                                               :redirect-url="'/vendors'"
                                ></delete-button>
                            @endif
                            </p>
                        </div>

                    </div>

                    <div class="block-content my-4">
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="details" role="tabpanel"
                                 aria-labelledby="details-tab">
                                @include('backend.vendors._details')
                            </div>
                            <div class="tab-pane fade" id="po" role="tabpanel" aria-labelledby="po-tab">
                                <vendor-purchases :id="{{ $vendor->id }}" :vendor="'{{ $vendor->name }}'"></vendor-purchases>
                            </div>
                            <div class="tab-pane fade" id="payments" role="tabpanel" aria-labelledby="payments-tab">
                                <vendor-payments :id="{{ $vendor->id }}" :vendor="'{{ $vendor->name }}'"></vendor-payments>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
