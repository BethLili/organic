@extends('backend.layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center my-4">
        <div class="col-md-10 my-4">
            <div class="card my-4">
                <div class="card-header font-weight-bolder d-flex justify-content-between">
                    Vendors
                    <div class="" role="group">
                        <a href="{{ action([\Backend\Http\Controllers\VendorController::class, 'create']) }}"
                           class="btn btn-sm btn-info">Add new</a>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-vcenter">
                        <thead>
                        <tr>
                            <th class="d-none d-md-table-cell" style="width: 30%;">Name</th>
                            <th class="d-none d-md-table-cell" style="width: 30%;">Description</th>
                            <th class="d-none d-md-table-cell" style="width: 30%;">Email</th>
                            <th class="d-none d-md-table-cell" style="width: 30%;">Contact</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($vendors as $vendor)
                            <tr>
                                <td>
                                    <a href="{{ $vendor->path() }}"
                                        class="nav-link">
                                        {{ ucwords(strtolower($vendor->name)) }}
                                    </a>
                                </td>
                                <td class="d-none d-md-table-cell">{{ $vendor->description }}</td>
                                <td class="d-none d-md-table-cell">{{ $vendor->email }}</td>
                                <td class="d-none d-md-table-cell">{{ $vendor->contact }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5" class="text-center"><p>No data.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
