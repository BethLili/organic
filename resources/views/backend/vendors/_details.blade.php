<div class="block-content">
    <div class="form-group row">
        <div class="col-md-3">
            <label>Description</label>
        </div>
        <div class="col-md-9">
            <p>{{ $vendor->description }}</p>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-3">
            <label>Email</label>
        </div>
        <div class="col-md-9">
            <p>{{ $vendor->email }}</p>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-3">
            <label>Contact Number</label>
        </div>
        <div class="col-md-9">
            <p>{{ $vendor->contact }}</p>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-3">
            <label>Address</label>
        </div>
        <div class="col-md-9">
            <p>{{ ucwords(strtolower($vendor->address)) }}</p>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-3">
            <label>Terms</label>
        </div>
        <div class="col-md-9">
            <p>{{ $vendor->terms }} days</p>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-3">
            <label>Total Purchases</label>
        </div>
        <div class="col-md-9">
            <p>{{ number_format($vendor->purchaseOrders->sum('total'), 2) }}</p>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-3">
            <label>Total Payments</label>
        </div>
        <div class="col-md-9">
            <p>{{ number_format($vendor->cashPayments->sum('amount'), 2) }}</p>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-3">
            <label>Balance</label>
        </div>
        <div class="col-md-9">
            <p>{{ number_format(($vendor->purchaseOrders->sum('total') + $vendor->cashPayments->sum('amount')), 2) }}</p>
        </div>
    </div>
</div>

