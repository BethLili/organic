<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>{{ config('app.name', 'Organic') }}</title>

    <meta name="description" content="A Backend Management System">
    <meta name="author" content="Beth Fabregas">
    <meta name="robots" content="noindex, nofollow">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--Font Awesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">

    <!-- Fonts and Styles -->
    @yield('css_before')

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
    <link rel="stylesheet" id="css-main" href="{{ mix('assets/backend/css/app.css') }}">

    @yield('css_after')

</head>
<body>

<div id="app">
    <div class="container" style="margin-top: 100px;">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-lg-4 mx-auto">
                <div class="card">
                    <div class="card-header">{{ __('Login') }}</div>

                    <div class="card-body">
                        <form method="POST">
                            @csrf

                            <div class="form-group row">
                                <div class="col-12">
                                    <label for="email" class="col-12 col-form-label">{{ __('E-Mail Address') }}</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <input id="email" type="email" class="form-control bg-transparent custom-form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-12">
                                    <label for="password" class="col-12 col-form-label">{{ __('Password') }}</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <input id="password" type="password" class="form-control bg-transparent custom-form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ mix('assets/backend/js/app.js') }}"></script>

</body>
</html>

