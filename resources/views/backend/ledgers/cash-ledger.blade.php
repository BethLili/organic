@extends('backend.layouts.app')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>

    <script>
        $('.js-dataTable-full').DataTable({
            pageLength: 50,
            lengthMenu: [[50, 100, 200, 500], [50, 100, 200, 500]],
            serverSide: true,
            ajax: "{{ action([\Backend\Http\Controllers\LedgerController::class, 'bankDatatables']) }}",
            columns: [
                {
                    name: 'id',
                    className: 'd-none d-md-table-cell',
                },
                {
                    name: 'actions',
                    searchable: false,
                    orderable: false,
                    className: 'd-none d-md-table-cell',
                },
                {
                    name: 'cash.bank',
                    className: 'd-none d-md-table-cell',
                },
                {
                    name: 'cashable_type'
                },
                {
                    name: 'cashable_id',
                    className: 'd-none d-md-table-cell',
                },
                {
                    name: 'amount'
                },
                {
                    name: 'created_at',
                    orderable: true,
                    searchable: true,
                    className: 'd-none d-md-table-cell',
                },
            ],
        });
    </script>
@endsection

@section('content')
    <div class="container">

        <div class="block-content block-content-full widget p-4">
            <table class="table table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th>ID</th>
                    <th></th>
                    <th>Bank</th>
                    <th>Transation Type</th>
                    <th>Transation ID</th>
                    <th>Total</th>
                    <th>Created At</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection
