@extends('backend.layouts.app')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>

    <script>
        $('.js-dataTable-full').DataTable({
            pageLength: 50,
            lengthMenu: [[50, 100, 200, 500], [50, 100, 200, 500]],
            serverSide: true,
            ajax: "{{ action([\Backend\Http\Controllers\LedgerController::class, 'salesDatatables']) }}",
            columns: [
                {
                    name: 'id',
                    className: 'd-none d-md-table-cell',
                },
                {
                    name: 'actions',
                    searchable: false,
                    orderable: false,
                    className: 'd-none d-md-table-cell',
                },
                {
                    name: 'customer.name',
                    orderable: false,
                    searchable: true
                },
                {
                    name: 'total'
                },
                {
                    name: 'created_at',
                    orderable: true,
                    searchable: true,
                    className: 'd-none d-md-table-cell',},
            ],
        });
    </script>
@endsection

@section('content')
    <div class="container">

        <div class="block-content block-content-full mt-4 widget p-4">
            <table class="table table-striped table-vcenter js-dataTable-full" style="width: unset;">
                <thead>
                <tr>
                    <th>Sales Order #</th>
                    <th>Actions</th>
                    <th>Customer</th>
                    <th>Total Sales</th>
                    <th>Created At</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection
