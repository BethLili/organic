@extends('backend.layouts.app')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>

    <script>
        $('.js-dataTable-full').DataTable({
            pageLength: 50,
            lengthMenu: [[50, 100, 200, 500], [50, 100, 200, 500]],
            serverSide: true,
            ajax: "{{ action([\Backend\Http\Controllers\LedgerController::class, 'payablesDatatables']) }}",
            columns: [
                {
                    name: 'id',
                    className: 'd-none d-md-table-cell',
                },
                {
                    name: 'actions',
                    searchable: false,
                    orderable: false,
                    className: 'd-none d-md-table-cell',
                },
                { name: 'po_id' },
                { name: 'total'},
                {
                    name: 'created_at',
                    orderable: true,
                    searchable: true,
                    className: 'd-none d-md-table-cell',
                },
            ],
        });
    </script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 my-4">
                <div class="widget">
                    <div class="card-body">
                        <h5 class="card-title">Bills need to pay</h5>
                        @if ($totalPayables === 0 )
                            <p>No bills to pay yet</p>
                        @else
                            <div class="col-10">
                                <div class="d-flex justify-content-between">
                                    <p>Total Bills To Pay, 100%</p>
                                    <p>{{ number_format($totalPayables, 2) }}</p>
                                </div>
                            </div>
                            <div class="col-10">
                                <div class="d-flex justify-content-between">
                                    <p class="text-danger">
                                        Overdue, {{ number_format(($totalDue / $totalPayables) * 100, 2) }}%</p>
                                    <p class="text-danger">{{ number_format($totalDue, 2) }} </p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="block-content block-content-full mt-4 widget p-4">
            <table class="table table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th>Payment ID</th>
                    <th>Actions</th>
                    <th>Purchase ID</th>
                    <th>Total</th>
                    <th>Created At</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection
