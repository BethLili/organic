@auth('backend')
    <nav class="sidebar">
        <div class="p-4">
            <ul class="list-unstyled components mb-5">
                <li>
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li class="active">
                    <a href="#admin" data-toggle="collapse" aria-expanded="false"
                       class="dropdown-toggle">Admin</a>
                    <ul class="collapse list-unstyled" id="admin">
                            <li>
                                <a href="{{ url('/categories') }}">Categories</a>
                            </li>
                            <li>
                                <a href="{{ url('/products') }}">Products</a>
                            </li>
                            <li>
                                <a href="{{ url('/vendors') }}">Vendors</a>
                            </li>
                            <li>
                                <a href="{{ url('/users') }}">Customers</a>
                            </li>
{{--                            <li>--}}
{{--                                <a href="{{ url('/backend-users') }}">Admin</a>--}}
{{--                            </li>--}}
                    </ul>
                </li>
                <li>
                    <a href="#finance" data-toggle="collapse" aria-expanded="false"
                       class="dropdown-toggle">Finance</a>
                    <ul class="collapse list-unstyled" id="finance">
                            <li>
                                <a href="{{ url('/cash') }}">Cash</a>
                            </li>
                            <li>
                                <a href="{{ url('/cash-deposits') }}">Cash Deposit</a>
                            </li>
                            <li>
                                <a href="{{ url('/cash-payments') }}">Cash Payments</a>
                            </li>
                            <li>
                                <a href="{{ url('/equity') }}">Equity</a>
                            </li>
                    </ul>
                </li>
                    <li>
                        <a href="{{ url('/purchase-orders') }}">Purchase Orders</a>
                    </li>
                    <li>
                        <a href="{{ url('/sales-orders') }}">Sales Orders</a>
                    </li>
                <li>
                    <a href="#ledgers" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Ledgers</a>
                    <ul class="collapse list-unstyled" id="ledgers">
                            <li>
                                <a href="/ledgers/sales">Sales Ledger</a>
                            </li>
                            <li>
                                <a href="/ledgers/purchases">Purchases Ledger</a>
                            </li>
                            <li>
                                <a href="/ledgers/cash">Cash Ledger</a>
                            </li>
                            <li>
                                <a href="/ledgers/payables">Payable Ledger</a>
                            </li>
                    </ul>
                </li>
                    <li>
                        <a href="#reports" data-toggle="collapse" aria-expanded="false"
                           class="dropdown-toggle">Financial Reports</a>
                        <ul class="collapse list-unstyled" id="reports">

                            <li>
                                <a href="/reports/profit-and-loss">Profit and Loss</a>
                            </li>
                            <li>
                                <a href="/reports/balance-sheet">Balance Sheet</a>
                            </li>
{{--                            <li>--}}
{{--                                <a href="#">Cash Flow</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="#">Equity</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="#">Admin reports</a>--}}
{{--                            </li>--}}
                        </ul>
                    </li>
            </ul>
        </div>
    </nav>


@endauth
