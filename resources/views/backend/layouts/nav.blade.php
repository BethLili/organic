<nav class="navbar navbar-expand-lg navbar-dark m-0">
    <div class="navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto text-right">
            @auth('backend')
                <notifications></notifications>
                <li class="nav-item">
                    <a class="nav-link text-dark" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('backend.logout') }}" method="POST"
                          style="display: none;">
                        @csrf
                    </form>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link text-dark" href="{{ route('backend.login') }}">{{ __('Login') }}</a>
                </li>
            @endauth
        </ul>
    </div>
</nav>


@section('js_after')
    <script>
        $( "#sidebarToggler" ).click(function() {
            if ($('#sidebar').css('display') == 'block') {
                $('#sidebar').css('display', 'none')
            } else {
                $('#sidebar').css('display', 'block')
            }
        });

        $( "#closeSidebar" ).click(function() {
            $('#sidebar').css('display', 'none')
        });
    </script>
@endsection
