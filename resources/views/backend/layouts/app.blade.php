<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>{{ config('app.name', 'Organic') }}</title>

    <meta name="description" content="A Backend Management System">
    <meta name="author" content="Beth Fabregas">
    <meta name="robots" content="noindex, nofollow">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--Font Awesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">

    <!-- Fonts and Styles -->
    @yield('css_before')

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
    <link href="https://fonts.googleapis.com/css2?family=ABeeZee&display=swap" rel="stylesheet">
    <link rel="stylesheet" id="css-main" href="{{ mix('assets/backend/css/app.css') }}">

    @yield('css_after')

</head>
<body>

<div id="app">
    <div class="container-fluid" style="padding: 0">
        <div class="wrapper">
            <input type="checkbox" id="menuToggler" class="input-toggler"/>
            <label for="menuToggler" class="menu-toggler">
                <span class="menu-toggler__line"></span>
                <span class="menu-toggler__line"></span>
                <span class="menu-toggler__line"></span>
            </label>

            @include('backend.layouts.sidebar')

            <main class="content col-12">
                @include('backend.layouts.nav')

                @yield('content')

                <flash message="{{ session('flash') }}"></flash>
            </main>
        </div>
    </div>
</div>

<script src="{{ mix('assets/backend/js/app.js') }}"></script>
<script>
    // Hide the contents until the burger menu finishes sliding in from the left
    var content = document.getElementsByTagName("main")[0];
    content.style.visibility = "hidden";

    var lowerLayerBurger = document.getElementsByClassName("menu-toggler__line")[2];
    lowerLayerBurger.addEventListener("animationend", function (evt) {
        content.style.visibility = "visible";
    });
</script>

@yield('js_after')

</body>
</html>
