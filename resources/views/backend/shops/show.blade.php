@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center my-4">
            <div class="col-md-10 my-4">
                <div class="widget my-4">
                    <div class="card-header font-weight-bolder d-flex justify-content-between">
                        <div>
                            <h5>Costing for {{ $product->name }}</h5>
                            <p> Current inventory: {{ $product->balance }} kgs</p>
                            <small>Default profit margin is 20% of cost.</small>
                        </div>
                        <div class="btn-group text-center">
                            @if($item->quantity > 0)
                                <costing :item="{{ $item }}"
                                         :product="{{ $product }}"></costing>
                            @endif
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-vcenter text-center">
                            <thead>
                            @if($item->quantity > 0)
                                <tr>
                                    <th class="d-none d-md-table-cell">Quantity</th>
                                    <th class="d-none d-md-table-cell">Cost</th>
                                    <th class="d-none d-md-table-cell">Price</th>
                                    <th colspan="3" class="text-center">Margin</th>
                                </tr>
                                <tr>
                                    <th class="d-none d-md-table-cell"></th>
                                    <th class="d-none d-md-table-cell"></th>
                                    <th class="d-none d-md-table-cell"></th>
                                    <th>per unit, in PESO</th>
                                    <th>total, in PESO</th>
                                    <th>total, in %</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>

                                <td class="d-none d-md-table-cell">{{ $item->quantity }}</td>
                                <td class="d-none d-md-table-cell"> {{ number_format($item->cost, 2) }}</td>
                                <td class="d-none d-md-table-cell"> {{ number_format($item->price, 2) }}</td>
                                <td>{{ number_format(($item->price - $item->cost), 2) }}
                                    <br>
                                <small class="d-md-none">Cost:  {{ number_format( $item->cost, 2)  }}</small></td>
                                <td>{{ number_format((($item->price - $item->cost) * $item->quantity), 2) }}</td>
                                <td>{{ (($item->price - $item->cost) / $item->cost) * 100 }}%</td>

                            </tr>
                            @else
                                <tr>
                                    <td colspan="5">No transactions yet, costing cannot be determined.</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
