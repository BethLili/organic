@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <notification-list :notifications="{{ $notifications }}"></notification-list>
    </div>
@endsection
