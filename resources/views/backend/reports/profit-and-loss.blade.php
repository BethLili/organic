@extends('backend.layouts.app')

@section('content')
    <div class="container">

        <div class="block-content block-content-full">
            <h5>Organic</h5>
            <p>Profit and Loss</p>
            <p>For {{ now()->startOfMonth()->format('M d, Y') }} to {{ now()->endOfMonth()->format('M d, Y') }}</p>

            <form action="/reports/generate/profit-and-loss" method="POST">
                @csrf @method('POST')
                <div class="form-group row">
                    <label for="from" class="col-3">From</label>
                    <input type="date" class="col-9 col-sm-3" name="from" id="from" value="{{ now()->startOfMonth()->format('Y-m-d') }}">
                    @if( $errors->has('from'))
                        <div
                            class="text-danger">{{ $errors->first('from') }}</div>
                    @endif
                </div>
                <div class="form-group row">
                    <label for="to" class="col-3">To</label>
                    <input type="date" class="col-9 col-sm-3" name="to" id="to" value="{{ now()->endOfMonth()->format('Y-m-d') }}">
                    @if( $errors->has('to'))
                        <div
                            class="text-danger">{{ $errors->first('to') }}</div>
                    @endif
                </div>
                <button type="submit" class="btn btn-outline-warning">
                    GENERATE
                </button>
            </form>

            <br>
            <div class="col-12 col-lg-6">
                <div class="d-flex justify-content-between">
                    <p>Sales</p>
                    <p>{{ number_format($sales, 2) }}</p>
                </div>
                <div class="d-flex justify-content-between">
                    <p>Cost of goods sold</p>
                    <p>({{ number_format($totalBeginning + $totalPurchases - $totalEnding, 2) }})</p>
                </div>

                <div class="col-10">
                    <div class="d-flex justify-content-between">
                        <p>Beginning Inventory</p>
                        <p>{{ number_format($totalBeginning, 2) }}</p>
                    </div>
                </div>
                <div class="col-10">
                    <div class="d-flex justify-content-between">
                        <p>Purchases</p>
                        <p>{{ number_format($totalPurchases, 2) }}</p>
                    </div>
                </div>
                <div class="col-10">
                    <div class="d-flex justify-content-between font-weight-bold">
                        <p>Total Goods Available For Sale</p>
                        <p>{{ number_format($totalBeginning + $totalPurchases, 2) }}</p>
                    </div>
                </div>
                <div class="col-10">
                    <div class="d-flex justify-content-between">
                        <p>Less: Ending inventory</p>
                        <p>{{ number_format($totalEnding, 2) }}</p>
                    </div>
                </div>

                <div class="d-flex justify-content-between">
                    <p>Net Income</p>
                    <p>{{ number_format($sales - ($totalBeginning + $totalPurchases - $totalEnding), 2) }}</p>
                </div>
            </div>

        </div>
    </div>
@endsection
