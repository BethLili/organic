@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a href="#" class="nav-link active">ALL</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Analysis</a>
                <ul>
                    <li>best sellers</li>
                    <li>delivery lead time</li>
                    <li>top customers</li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Financial</a>
                <ul>
                    <li>balance sheet</li>
                    <li>income statement</li>
                    <li>equity</li>
                    <li>cash flow</li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Admin</a>
                order level qty
            </li>
        </ul>


    </div>
@endsection
