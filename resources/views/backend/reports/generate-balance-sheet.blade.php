@extends('backend.layouts.app')

@section('content')
    <div class="container">

        <div class="block-content block-content-full">


            <form action="/reports/generate/balance-sheet" method="POST" class="my-4">
                @csrf @method('POST')
                <div class="form-group row">
                    <label for="from" class="col-3">From</label>
                    <input type="date" class="col-9 col-sm-3" name="from" id="from"
                           value="{{ $from->format('Y-m-d') }}">
                    @if( $errors->has('from'))
                        <div
                            class="text-danger">{{ $errors->first('from') }}</div>
                    @endif
                </div>
                <div class="form-group row">
                    <label for="to" class="col-3">To</label>
                    <input type="date" class="col-9 col-sm-3" name="to" id="to"
                           value="{{ $to->format('Y-m-d') }}">
                    @if( $errors->has('to'))
                        <div
                            class="text-danger">{{ $errors->first('to') }}</div>
                    @endif
                </div>
                <button type="submit" class="btn btn-outline-warning">
                    GENERATE
                </button>
            </form>


            <h5>Organic</h5>
            <p>Balance Sheet</p>
            <p>Period covered {{ $from->format('M d, Y') }}
                to {{ $to->format('M d, Y') }}</p>


            <br>
            <div class="col-12 col-lg-6">
                <div class="d-flex justify-content-between">
                    <h5>Assets</h5>
                </div>

                <div class="col-10">
                    <div class="d-flex justify-content-between">
                        <p>Cash and cash equivalents</p>
                        <p>{{ number_format($cash, 2) }}</p>
                    </div>
                </div>

                <div class="col-10">
                    <div class="d-flex justify-content-between">
                        <p>Inventory</p>
                        <p>{{ number_format($inventory, 2) }}</p>
                    </div>
                </div>
                <div class="d-flex justify-content-between">
                    <h5 class="font-weight-bold">Total Assets</h5>
                    <p class="font-weight-bold border-bottom">{{ number_format($inventory + $cash, 2) }}</p>
                </div>
                <br>
                <div class="d-flex justify-content-between">
                    <h5>Liabilities</h5>
                </div>

                <div class="col-10">
                    <div class="d-flex justify-content-between">
                        <p>Accounts payable</p>
                        <p>{{ number_format($payable, 2) }}</p>
                    </div>
                </div>

                <div class="d-flex justify-content-between">
                    <h5>Shareholders' Equity</h5>
                </div>
                <div class="col-10">
                    <div class="d-flex justify-content-between">
                        <p>Shareholders' Equity</p>
                        <p>{{ number_format($equity, 2) }}</p>
                    </div>
                </div>

                <div class="col-10">
                    <div class="d-flex justify-content-between">
                        <p>Total Income for the period</p>
                        <p>{{ number_format($netIncome, 2) }}</p>
                    </div>
                </div>

                <div class="d-flex justify-content-between">
                    <h5 class="font-weight-bold">Total Liabilities & Shareholders' Equity</h5>
                    <p class="font-weight-bold border-bottom">{{ number_format($payable + $equity + $netIncome, 2) }}</p>
                </div>


            </div>

        </div>
    </div>
@endsection
