<div class="card-body col-12 col-md-7 my-4 mx-auto">
    <div class="block-content">
        <table class="table table-vcenter">
            <thead>
            <tr>
                <th class="w-75">Shareholder's name</th>
            </tr>
            </thead>
            <tbody>
            @forelse($equities as $equity)
                <tr>
                    <td class="w-75">
                        <a href="{{ $equity->path() }}"
                           class="nav-link">
                            {{ ucwords(strtolower($equity->name)) }}
                        </a>
                        </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center"><p>No data.</p></td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>
