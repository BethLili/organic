<div class="block-content">
    <div class="form-group row">
        <div class="col-md-3">
            <label>Account Name</label>
        </div>
        <div class="col-md-9">
            <p>{{ ucwords(strtolower($equity->name) )}}</p>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-3">
            <label>Balance</label>
        </div>
        <div class="col-md-9">
            <p>{{ number_format($equity->balance, 2) }}</p>
        </div>
    </div>
</div>

