@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <div class="card my-4">
            <div class="card-header font-weight-bolder d-flex justify-content-between">
                Cash Accounts
            </div>

            <div class="row">
                <div class="card-body col-12 col-md-4 my-4 mx-auto">
                    <div class="block-content">
                        <form action="{{ action([\Backend\Http\Controllers\CashController::class, 'update'], $cash) }}" method="post">
                            @csrf @method('PUT')
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="form-material {{ $errors->has('bank') ? 'form-material-danger' : ''}}">
                                        <input required
                                               type="text"
                                               class="form-control"
                                               id="bank"
                                               name="bank"
                                               value="{{ old('bank') ? old('bank') : $cash->bank}}"
                                               placeholder="Bank and branch">
                                        <label for="bank">Bank and branch</label>
                                    </div>
                                    @if( $errors->has('bank'))
                                        <div class="text-danger">{{ $errors->first('bank') }}</div>
                                    @endif
                                </div>
                                <div class="col-md-12">
                                    <div class="form-material {{ $errors->has('name') ? 'form-material-danger' : ''}}">
                                        <input required
                                               type="text"
                                               class="form-control"
                                               id="name"
                                               name="name"
                                               value="{{ old('name') ? old('name') : $cash->name }}"
                                               placeholder="Account name" >
                                        <label for="bank">Account name</label>
                                    </div>
                                    @if( $errors->has('name'))
                                        <div class="text-danger">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>
                                <div class="col-md-12">
                                    <div class="form-material {{ $errors->has('account') ? 'form-material-danger' : ''}}">
                                        <input required
                                               type="text"
                                               class="form-control"
                                               id="account"
                                               name="account"
                                               value="{{ old('account') ? old('account') : $cash->account }}"
                                               placeholder="Account number" >
                                        <label for="bank">Account number</label>
                                    </div>
                                    @if( $errors->has('account'))
                                        <div class="text-danger">{{ $errors->first('account') }}</div>
                                    @endif
                                </div>

                            </div>
                            <div class="form-group row">
                                <div class="col-md-9">
                                    <button type="submit" class="btn btn-outline-warning">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                @include('backend.cash._list')
            </div>
        </div>
    </div>
@endsection
