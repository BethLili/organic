<div class="block-content">
    <div class="form-group row">
        <div class="col-md-3">
            <label>Account Name</label>
        </div>
        <div class="col-md-9">
            <p>{{ ucwords(strtolower($cash->name) )}}</p>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-3">
            <label>Account Number</label>
        </div>
        <div class="col-md-9">
            <p>{{ $cash->account }}</p>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-3">
            <label>Balance</label>
        </div>
        <div class="col-md-9">
            <p>{{ $cash->balance }}</p>
        </div>
    </div>
</div>

