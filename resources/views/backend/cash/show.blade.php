@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center my-4">
            <div class="col-md-10 my-4">
                <div class="block">
                    <div class="block-header block-header-default d-flex justify-content-between flex-wrap">
                        <nav>
                            <div class="nav nav-tabs custom-nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active"
                                   id="details-tab"
                                   data-toggle="tab" role="tab"
                                   href="#details"
                                   aria-controls="details" aria-selected="true">
                                    <strong class="block-title font-weight-bolder">{{ ucwords(strtolower($cash->bank)) }}</strong>
                                </a>
                                <a class="nav-item nav-link"
                                   id="ledger-tab"
                                   data-toggle="tab" role="tab"
                                   href="#ledger"
                                   aria-controls="ledger" aria-selected="true">
                                    Ledger
                                </a>
                                <a class="nav-item nav-link"
                                   id="receipts-tab"
                                   data-toggle="tab" role="tab"
                                   href="#receipts"
                                   aria-controls="receipts" aria-selected="true">
                                    Deposits
                                </a>
                                <a class="nav-item nav-link"
                                   id="payments-tab"
                                   data-toggle="tab" role="tab"
                                   href="#payments"
                                   aria-controls="payments" aria-selected="true">
                                    Payments
                                </a>
                            </div>
                        </nav>

                        <div class="col-12 col-md-2 my-4 my-md-0">
                            <p class="ml-auto text-right">
                                <a href="{{ $cash->path() }}/edit"
                                   class="btn btn-sm btn-info px-4">
                                    Edit
                                </a>
                            </p>
                        </div>

                    </div>

                    <div class="block-content my-4">
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details-tab">
                                @include('backend.cash._details')
                            </div>
                            <div class="tab-pane fade" id="ledger" role="tabpanel" aria-labelledby="ledger-tab">
                                {{--purchases and payments--}}
                                <cash-ledger :id="{{ $cash->id }}" :bank="'{{ ucwords(strtolower($cash->bank)) }}'"></cash-ledger>
                            </div>
                            <div class="tab-pane fade" id="receipts" role="tabpanel" aria-labelledby="receipts-tab">
                                <cash-deposit :id="{{ $cash->id }}" :bank="'{{ ucwords(strtolower($cash->bank)) }}'"></cash-deposit>
                            </div>
                            <div class="tab-pane fade" id="payments" role="tabpanel" aria-labelledby="payments-tab">
                                <cash-payment :id="{{ $cash->id }}" :bank="'{{ ucwords(strtolower($cash->bank)) }}'"></cash-payment>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
