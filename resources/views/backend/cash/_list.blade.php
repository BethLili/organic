<div class="card-body col-12 col-lg-7 my-4 mx-auto">
    <div class="block-content">
        <table class="table table-vcenter">
            <thead>
            <tr>
                <th class="d-none d-md-table-cell">Bank</th>
                <th>Name</th>
                <th>Number</th>
            </tr>
            </thead>
            <tbody>
            @forelse($cashList as $cash)
                <tr>
                    <td class="d-none d-md-table-cell">
                        <a href="{{ $cash->path() }}"
                           class="nav-link">
                            {{ ucwords(strtolower($cash->bank)) }}
                        </a>
                    </td>
                    <td>{{ ucwords(strtolower($cash->name)) }}
                        <br>
                    <small class="d-md-none">{{ ucwords(strtolower($cash->bank)) }}</small></td>
                    <td>{{ $cash->account }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center"><p>No data.</p></td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>
