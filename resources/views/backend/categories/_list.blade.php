<div class="card-body col-12 col-md-5 my-4 mx-auto">
    <div class="block-content">
        <table class="table table-vcenter text-center">
            <thead>
            <tr>
                <th class="w-75">Category</th>
            </tr>
            </thead>
            <tbody>
            @forelse($categories as $category)
                <tr>
                    <td class="w-75">
                        <a href="{{ $category->path() }}" title="edit"
                           class="nav-link">
                            {{ ucfirst(strtolower($category->name)) }}
                        </a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center"><p>No data.</p></td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>
