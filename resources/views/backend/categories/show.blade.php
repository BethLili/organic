@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center my-4">
            <div class="col-md-10 my-4">
                <div class="block">
                    <div class="block-header block-header-default d-flex justify-content-between">
                        <h3 class="block-title font-weight-bolder text-uppercase">{{  $category->name }}</h3>
                        <div class="" role="group">
                            <a href="{{ action([\Backend\Http\Controllers\CategoryController::class, 'edit'], $category) }}"
                               class="btn btn-sm btn-info">
                                Edit
                            </a>
                            @if($category->products->isEmpty())
                            <delete-button :id="{{ $category->id }}"
                                           :name="'{{ $category->name }}'"
                                           :path="'{{ $category->path() }}'"
                                           :redirect-url="'{{ action([\Backend\Http\Controllers\CategoryController::class, 'index']) }}'"
                            ></delete-button>
                            @endif
                        </div>
                    </div>

                    <div class="block-content">
                        <h5 class="block-title font-weight-bolder text-gray-dark">List of products under {{ strtolower($category->name) }}</h5>

                        <div class="card">
                            <div class="card-body">
                                <table class="table table-vcenter text-center">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th class="d-none d-md-table-cell" style="width: 30%;">Description</th>
                                        <th>Inventory (kgs)</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($category->products as $product)
                                        <tr>
                                            <td><a href="{{ $product->path() }}">{{ $product->name }}</a></td>
                                            <td class="d-none d-md-table-cell">{{ $product->description }}</td>
                                            <td>{{ $product->balance() }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="5" class="text-center"> <p>No data.</p></td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
