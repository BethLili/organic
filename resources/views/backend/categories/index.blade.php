@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <div class="widget my-4">
            <div class="card-header font-weight-bolder d-flex justify-content-between">
                Categories
            </div>

            <div class="row">
                <div class="card-body col-12 col-md-4 my-4 mx-auto">
                    <div class="block-content">
                        <form action="{{ action([\Backend\Http\Controllers\CategoryController::class, 'store']) }}" method="post">
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="form-material {{ $errors->has('name') ? 'form-material-danger' : ''}}">
                                        <input required
                                               type="text"
                                               class="form-control"
                                               id="name"
                                               name="name"
                                               value="{{ old('name') }}">
                                        <label for="name">Add new category</label>
                                    </div>
                                    @if( $errors->has('name'))
                                        <div class="text-danger">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-9">
                                    <button type="submit" class="btn btn-outline-warning">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
               @include('backend.categories._list')
            </div>
        </div>
    </div>
@endsection
