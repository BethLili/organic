@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center my-4">
            <div class="col-md-10 my-4">
                <div class="widget my-4">
                    <div class="card-header font-weight-bolder d-flex justify-content-between">
                        Users
                    </div>

                    <div class="card-body">
                        <table class="table table-vcenter">
                            <thead>
                            <tr>
                                <th class="">Name</th>
                                <th class="d-none d-md-table-cell">Email</th>
                                <th class="d-none d-md-table-cell">Address</th>
                                <th>#Orders</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($users as $user)
                                <tr>
                                    <td><a href="{{ $user->path() }}">{{ ucfirst($user->name)  }}</a></td>
                                    <td class="d-none d-sm-table-cell">{{ $user->email  }}</td>
                                    <td class="d-none d-sm-table-cell">{{ $user->profile ? $user->profile->address : 'Address not provided' }}</td>
                                    <td>{{ $user->orders->count() }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td>No users yet.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
