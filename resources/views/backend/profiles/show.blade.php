@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center my-4">
            <div class="col-12 my-4 widget p-4">
                <div class="block">
                    <div class="block-header block-header-default d-flex justify-content-between">
                        <nav>
                            <div class="nav nav-tabs custom-4-nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active"
                                   id="details-tab"
                                   data-toggle="tab" role="tab"
                                   href="#details"
                                   aria-controls="details" aria-selected="true">
                                    <strong class="block-title font-weight-bolder">{{ $user->name }}'s Profile</strong>
                                </a>
                                <a class="nav-item nav-link"
                                   id="orders-tab"
                                   data-toggle="tab" role="tab"
                                   href="#orders"
                                   aria-controls="orders" aria-selected="true">
                                    Orders
                                </a>
                            </div>
                        </nav>
                    </div>

                    <div class="block-content my-4">
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="details" role="tabpanel"
                                 aria-labelledby="details-tab">
                                <div class="block-content">
                                    <div class="form-group row">
                                        <div class="col-3">
                                            <label>Username</label>
                                        </div>
                                        <div class="col-9">
                                            <p>{{ ucfirst($user->name) }}</p>
                                        </div>
                                        <div class="col-3">
                                            <label>Email</label>
                                        </div>
                                        <div class="col-9">
                                            <p>{{ $user->email }}</p>
                                        </div>
                                    </div>
                                    @if($user->profile)
                                        <div class="form-group row">
                                            <div class="col-3">
                                                <label>First Name</label>
                                            </div>
                                            <div class="col-9">
                                                <p>{{ ucfirst(strtolower($user->profile->first_name)) }}</p>
                                            </div>
                                            <div class="col-3">
                                                <label>Last Name</label>
                                            </div>
                                            <div class="col-9">
                                                <p>{{ ucfirst(strtolower($user->profile->last_name)) }}</p>
                                            </div>
                                            <div class="col-3">
                                                <label>Birthday</label>
                                            </div>
                                            <div class="col-9">
                                                <p>{{ $user->profile->birthday }}</p>
                                            </div>
                                            <div class="col-3">
                                                <label>Mobile</label>
                                            </div>
                                            <div class="col-9">
                                                <p>{{ $user->profile->mobile }}</p>
                                            </div>
                                            <div class="col-3">
                                                <label>Address</label>
                                            </div>
                                            <div class="col-9">
                                                <p>{{ $user->profile->place->name }}, {{ $user->profile->state->name }}</p>
                                            </div>
                                            <div class="col-3">
                                                <label>Personal Info</label>
                                            </div>
                                            <div class="col-9">
                                                <p>{{ $user->profile->personal_info }}</p>
                                            </div>
                                        </div>
                                    @else
                                        <p>User has not provided personal details yet.</p>
                                    @endif
                                </div>

                            </div>
                            <div class="tab-pane fade" id="orders" role="tabpanel" aria-labelledby="orders-tab">
                                <users-orders :user="{{ $user->id }}"></users-orders>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
