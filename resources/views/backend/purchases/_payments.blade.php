@foreach($purchaseOrder->paymentTransactions as $payment)
    <tr>
        <td class="d-none d-md-table-cell"></td>
        <td class="d-none d-md-table-cell"></td>
        <td><strong class="text-secondary">
                Less:
                <a href="{{ action([\Backend\Http\Controllers\CashPaymentController::class, 'show'], $payment->id) }}">
                    Payment
                </a>
            </strong>
            <br>
            <small>{{ $payment->created_at->diffForHumans() }}</small>
        </td>
        <td class="text-right">
            <a href="">{{ number_format($payment->amount, 2) }}</a>
        </td>
    </tr>
@endforeach
