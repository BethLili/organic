@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center my-4">
            <div class="col-12 col-md-10 my-4">

                <div class="widget p-4 bg-white rounded">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <p>PO# {{ $purchaseOrder->id  }} : {{ ucfirst($purchaseOrder->status->name) }}</p>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <div class="d-flex justify-content-between flex-wrap">
                                <div class="mx-4">
                                    <small>From</small>
                                    <br>
                                    <strong><a href="{{ $purchaseOrder->vendor->path()  }}">{{ $purchaseOrder->vendor->name }}</a></strong>
                                    <p><small>{{ $purchaseOrder->vendor->address  }} <br>
                                            {{ $purchaseOrder->vendor->email  }} <br>
                                            {{ $purchaseOrder->vendor->contact  }}</small></p>
                                </div>
                                <div class="mx-4">
                                    <small>Date</small>
                                    <p>{{ $purchaseOrder->created_at->format('d M Y')  }}</p>
                                </div>
                                <div class="mx-4">
                                    <small>Due Date</small>
                                    <p>{{ \Carbon\Carbon::createFromDate($purchaseOrder->due_at)->format('d M Y')  }}</p>
                                </div>
                            <div class="mx-4">
                                <small>Total</small>
                                <p>{{ number_format($purchaseOrder->total, 2) }}</p>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <table class="table table-vcenter text-left table-responsive-sm">
                            <thead>
                            <tr>
                                <th>Item</th>
                                <th class="d-none d-md-table-cell">Quantity</th>
                                <th class="d-none d-md-table-cell">Unit Price</th>
                                <th class="text-right">Amount</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($orders as $order)
                                <tr>
                                    <td>
                                    @foreach($products as $product)
                                        <span class="{{ $product->id === $order->item ? 'd-inline' : 'd-none' }}">
                                            {{ $product->id === $order->item ? $product->name : '' }}
                                        </span>
                                    @endforeach
                                        <br>
                                        <small class="d-block d-md-none">{{ $order->quantity }}kgs @ {{ number_format($order->price, 2) }}</small>
                                    </td>
                                    <td class="d-none d-md-table-cell">{{ $order->quantity }}</td>
                                    <td class="d-none d-md-table-cell">{{ number_format($order->price, 2) }}</td>
                                    <td class="text-right">{{ number_format($order->quantity * $order->price, 2) }}</td>
                                </tr>
                            @endforeach

                            <tr>
                                <td class="d-none d-md-table-cell"></td>
                                <td class="d-none d-md-table-cell"></td>
                                <td><strong>Total:</strong></td>
                                <td class="text-right"><strong>{{ number_format($purchaseOrder->total, 2) }}</strong></td>
                            </tr>

                            @include('backend.purchases._payments')

                            <tr>
                                <td class="d-none d-md-table-cell"></td>
                                <td class="d-none d-md-table-cell"></td>
                                <td><strong>Amount due:</strong></td>
                                <td class="text-right"><strong>{{ number_format($purchaseOrder->balance(), 2) }}</strong></td>
                            </tr>

                            </tbody>
                        </table>

                        @if($purchaseOrder->status->id === 1)

                            <receive-inventory :po="{{$purchaseOrder}}"></receive-inventory>

                        @elseif($purchaseOrder->status->id === 4 || $purchaseOrder->status->id === 2 || $purchaseOrder->status->id === 3)

                            <add-payment :po="{{$purchaseOrder}}"></add-payment>

                        @else
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
