@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center my-4">
            <div class="col-md-10 my-4">
                <div class="block">
                    <div class="block-content">
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="details" role="tabpanel"
                                 aria-labelledby="details-tab">
                                <div class="widget my-4">
                                    <div class="card-header font-weight-bolder d-flex justify-content-between">
                                        Payment Transactions
                                    </div>

                                    <div class="card-body">
                                        <table class="table table-vcenter text-center">
                                            <thead>
                                            <tr>
                                                <th class="d-none d-md-table-cell">Date</th>
                                                <th class="">Payee</th>
                                                <th class="d-none d-md-table-cell">Purchase Order #</th>
                                                <th class="d-none d-md-table-cell">Payment Reference</th>
                                                <th>Amount</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @forelse ($payments as $payment)
                                                <tr>
                                                    <td class="d-none d-md-table-cell">{{$payment->created_at->format('M d, Y')}}</td>
                                                    <td>
                                                        <a href="{{$payment->vendor->path()}}">{{$payment->vendor->name}}</a><br>
                                                    <small>{{$payment->created_at->format('M d, Y')}}</small></td>
                                                    <td class="d-none d-md-table-cell"><a href="{{ $payment->purchaseOrder->path() }}">{{$payment->po_id}}</a></td>
                                                    <td class="d-none d-md-table-cell"><a href="{{ $payment->path() }}">{{$payment->reference}}</a></td>
                                                    <td>{{ number_format($payment->amount, 2) }}</td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="5" class="text-center"><p>No data.</p></td>
                                                </tr>
                                            @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
