@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center my-4">
            <div class="col-md-12 my-4">

                <div class="widget p-4 bg-white rounded">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <p>{{ ucfirst($cashPayment->purchaseOrder->status->name) }}</p>
{{--                            <div>--}}
{{--                                <div>--}}
{{--                                    <form action="{{ route('backend.cash-payments.destroy', $cashPayment) }}" method="POST">--}}
{{--                                        @csrf @method('DELETE')--}}
{{--                                        <button class="btn btn-info" type="submit">DELETE</button>--}}
{{--                                    </form>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div class="row">
                                <div class="mx-4">
                                    <h5>From</h5>
                                    <a href="{{ $cashPayment->purchaseOrder->vendor->path()  }}">{{ ucwords(strtolower($cashPayment->purchaseOrder->vendor->name)) }}</a>
                                    <p><small>{{ ucwords(strtolower($cashPayment->purchaseOrder->vendor->address))  }} <br>
                                            {{ $cashPayment->purchaseOrder->vendor->email  }} <br>
                                            {{ $cashPayment->purchaseOrder->vendor->contact  }}</small></p>
                                </div>
                                <div class="mx-4">
                                    <h5>Payment Date</h5>
                                    <p>{{ $cashPayment->created_at->format('d M Y')  }}</p>
                                </div>
                                <div class="mx-4">
                                    <h5>Reference</h5>
                                    <p>{{ $cashPayment->reference  }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <table class="table table-vcenter text-center">
                            <thead>
                            <tr>
                                <th class="d-none d-md-table-cell">PO Date</th>
                                <th class="d-none d-md-table-cell">Due Date</th>
                                <th class="">Total Due</th>
                                <th class="">Payment Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="d-none d-md-table-cell">{{ $cashPayment->purchaseOrder->created_at->format('d M Y')  }}</td>
                                <td class="d-none d-md-table-cell">{{ $cashPayment->purchaseOrder->created_at->addDays(30)->format('d M Y')  }}</td>
                                <td><strong>{{ number_format($cashPayment->purchaseOrder->total, 2)  }}</strong>
                                    <br>
                                    <small>PO Date: {{ $cashPayment->purchaseOrder->created_at->format('d M Y')  }}</small></td>
                                <td><strong>{{ number_format($cashPayment->amount, 2) }}</strong></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
