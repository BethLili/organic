@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center my-4">
            <div class="col-md-10 my-4">
                <sales-orders></sales-orders>
            </div>
        </div>
    </div>
@endsection
