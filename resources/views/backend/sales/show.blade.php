@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center my-4">
            <div class="col-md-12 my-4">

                <div class="card p-4 bg-white rounded">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <p>{{ ucfirst($salesOrder->status->name) }}</p>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div class="row">
                                <div class="mx-4">
                                    <h5>From</h5>
                                    <a href="{{ $salesOrder->customer->path()  }}">{{ ucwords(strtolower($salesOrder->customer->name)) }}</a>
                                    <br>
                                    <small> {{ $salesOrder->customer->email  }}</small> <br>
                                    @if ($salesOrder->customer->profile)
                                    <p>
                                        <small> {{ $salesOrder->customer->profile->mobile  }}</small> <br>
                                        <small>{{ $salesOrder->customer->profile->place->name }}, {{ $salesOrder->customer->profile->state->name }}</small><br>
                                    </p>
                                    @endif
                                </div>
                                <div class="mx-4">
                                    <h5>Date</h5>
                                    <p>{{ $salesOrder->created_at->format('d M Y')  }}</p>
                                </div>
                            </div>
                            <div class="mx-4">
                                <h5>Total</h5>
                                <p>{{ number_format($salesOrder->total, 2) }}</p>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <table class="table table-vcenter text-center">
                            <thead>
                            <tr>
                                <th>Item</th>
                                <th class="d-none d-md-table-cell">Quantity</th>
                                <th class="d-none d-md-table-cell">Unit Price</th>
                                <th class="">Amount</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($orders as $order)
                                <tr>
                                    <td>
                                    @foreach($products as $product)
                                        <span class="{{ $product->id === $order->item ? 'd-inline' : 'd-none' }}">
                                            {{ $product->id === $order->item ? $product->name : '' }}
                                        </span>
                                    @endforeach
                                        <br>
                                        <small class="d-md-none">{{ $order->quantity }}kg/s @ {{ $order->price }}</small>
                                    </td>
                                    <td class="d-none d-md-table-cell">{{ $order->quantity }}</td>
                                    <td  class="d-none d-md-table-cell">{{ $order->price }}</td>
                                    <td>{{ number_format($order->quantity * $order->price, 2) }}</td>
                                </tr>
                            @endforeach

                            <tr>
                                <td class="d-none d-md-table-cell"></td>
                                <td class="d-none d-md-table-cell"></td>
                                <td><strong>Total:</strong></td>
                                <td><strong>{{ number_format($salesOrder->total, 2) }}</strong></td>
                            </tr>

{{--                            @include('backend.purchases._payments')--}}

{{--                            <tr>--}}
{{--                                <td></td>--}}
{{--                                <td></td>--}}
{{--                                <td><strong>Amount due:</strong></td>--}}
{{--                                <td><strong>{{ number_format($salesOrder->balance(), 2) }}</strong></td>--}}
{{--                            </tr>--}}

                            </tbody>
                        </table>

{{--                        @if($salesOrder->status->id === 1)--}}

{{--                            <receive-inventory :po="{{$salesOrder}}"></receive-inventory>--}}

{{--                        @elseif($salesOrder->status->id === 4 || $salesOrder->status->id === 2 || $salesOrder->status->id === 3)--}}

{{--                            <add-payment :po="{{$salesOrder}}"></add-payment>--}}

{{--                        @else--}}
{{--                        @endif--}}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
