@extends('backend.layouts.app')

@section('content')
    @include('backend.products._create')
    <div class="container">
        @if($errors->any())
            <div class="text-danger"><i class="fas fa-exclamation-triangle text-danger mr-4"></i>Adding new product failed.</div>
            {!! implode('', $errors->all('<div><small class="text-danger">:message</small></div>')) !!}
        @endif
        <div class="row justify-content-center my-4">
            <div class="col-md-10 my-4">
                <div>
                    <div class="widget my-4">
                        <div class="card-header font-weight-bolder d-flex justify-content-between">
                            Products
                            <div class="btn-group text-center">
                                <button class="btn btn-sm btn-info" type="button" data-toggle="modal" data-target="#create-product-modal">Add New
                                </button>
                            </div>
                        </div>

                        <div class="card-body">
                            <table class="table table-vcenter">
                                <thead>
                                <tr>
                                    <th class="">Name</th>
                                    <th  class="d-none d-md-table-cell"></th>
                                    <th class="d-none d-md-table-cell">Category</th>
                                    <th>Quantity</th>
                                </tr>
                                </thead>
                                <tbody>

                                @forelse ($products as $product)
                                    <tr>
                                        <td>
                                            <a title="view"
                                               class="nav-link"
                                               href="{{ $product->path() }}">
                                                {{ ucfirst(strtolower($product->name)) }}
                                            </a>
                                        </td>
                                        <td class="d-none d-md-table-cell">
                                            <img src="{{ $product->image }}" class="w-25" alt="">
                                        </td>
                                        <td class="d-none d-md-table-cell">
                                            <a href="{{ $product->category->path() }}">{{ ucfirst(strtolower($product->category->name)) }}</a>
                                        </td>
                                        <td>{{ number_format($product->balance, 0) }}</td>
                                    </tr>
                                @empty
                                    <p>no data</p>
                                @endforelse

                                </tbody>
                                <tbody v-else>
                                <tr>
                                    <td colspan="5" class="text-center"><p>No data.</p></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
