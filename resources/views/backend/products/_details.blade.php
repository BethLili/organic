<div class="block-content">
    <div class="form-group row">
        <div class="col-md-3">
            <label>Filed under</label>
        </div>
        <div class="col-md-9">
            <p>{{ ucfirst(strtolower($product->category->name)) }}</p>
        </div>
        <div class="col-md-3">
            <label>Description</label>
        </div>
        <div class="col-md-9">
            <p>{{ ucfirst(strtolower($product->description))}}</p>
        </div>
        <div class="col-md-3">
            <label>Balance</label>
        </div>
        <div class="col-md-9">
            <p>{{ $product->balance}} kgs</p>
        </div>
    </div>
</div>
