<div class="modal fade" id="create-product-modal" tabindex="-1" role="dialog" aria-labelledby="product-modalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-popout" role="document">
        <div class="modal-content p-4">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="form-material pt-0 font-weight-bolder">
                                Create New Product
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <form action="{{ action([\Backend\Http\Controllers\ProductController::class, 'store']) }}" method="POST"
                  enctype="multipart/form-data">
                @csrf @method('POST')

                <div class="block-content">
                    <div class="form-group row {{ $errors->has('name') ? 'form-material-danger' : ''}}">
                        <label for="name" class="col-12 col-md-3">Name</label>
                        <input type="text" class="col-12 col-md-9 form-control" name="name" id="name" value="{{ old('name') ? old('name') : "" }}">
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-12 col-md-3">Image</label>
                        <input class="form-control col-12 col-md-9 my-2" type="file" name="image"
                               id="product_image" value="{{ old('image') ? old('image') : "" }}">
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-12 col-md-3">Description</label>
                        <textarea required
                                  class="form-control col-12 col-md-9"
                                  id="description"
                                  rows="2"
                                  name="description"
                        >{{  old('description') ? old('description') : "" }}</textarea>
                    </div>
                    <div class="form-group row">
                        <label for="product_image" class="col-12 col-md-3">Category</label>
                        <select class="form-control py-0 col-12 col-md-9" name="category_id">
                            <option>Please select</option>
                            @foreach($categories as $category)
                                <option value="{{  old('category_id') &&  old('category_id') == $category->id ? old('category_id') : $category->id }}"
                                    {{  old('category_id') &&  old('category_id') == $category->id ? "selected" : "" }}>
                                    {{ ucwords(strtolower($category->name)) }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <div>
                        <button type="submit" class="btn btn-outline-warning">
                            SAVE
                        </button>
                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
