@extends('backend.layouts.app')

@section('content')
    @include('backend.products._edit')
    <div class="container">
        @if($errors->any())
            <div class="text-danger"><i class="fas fa-exclamation-triangle text-danger mr-4"></i>Editing product failed.</div>
            {!! implode('', $errors->all('<div><small class="text-danger">:message</small></div>')) !!}
        @endif
        <div class="row justify-content-center my-4">
            <div class="widget col-md-10 my-4 p-4">
                <div class="block">
                    <div class="block-header block-header-default d-flex justify-content-between flex-wrap">
                        <nav>
                            <div class="nav nav-tabs custom-nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active"
                                   id="details-tab"
                                   data-toggle="tab" role="tab"
                                   href="#details"
                                   aria-controls="details" aria-selected="true">
                                    <strong
                                        class="block-title font-weight-bolder">{{ ucfirst(strtolower($product->name)) }}</strong>
                                </a>
                                <a class="nav-item nav-link"
                                   id="ledger-tab"
                                   data-toggle="tab" role="tab"
                                   href="#ledger"
                                   aria-controls="ledger" aria-selected="true">
                                    Ledger
                                </a>
                                <a class="nav-item nav-link"
                                   id="po-tab"
                                   data-toggle="tab" role="tab"
                                   href="#po"
                                   aria-controls="po" aria-selected="true">
                                    Purchases
                                </a>
                                <a class="nav-item nav-link"
                                   id="sales-tab"
                                   data-toggle="tab" role="tab"
                                   href="#sales"
                                   aria-controls="sales" aria-selected="true">
                                    Sales
                                </a>
                            </div>
                        </nav>

                        <div class="btn-group text-center mt-4 mt-md-0 col-12 col-md-2 ">
                            <p class="ml-auto text-right">
                                <a class="btn btn-sm btn-info text-white" type="button" data-toggle="modal"
                                   data-target="#edit-product-modal">Edit
                                </a>
                                <a class="btn btn-sm btn-info"
                                   href="/products/{{ $product->slug }}/costing">
                                    Costing
                                </a>
                            </p>
                        </div>
                    </div>

                    <div class="block-content my-4">
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="details" role="tabpanel"
                                 aria-labelledby="details-tab">
                                @include('backend.products._details')
                            </div>
                            <div class="tab-pane fade" id="ledger" role="tabpanel" aria-labelledby="ledger-tab">
                                <product-ledger :id="{{ $product->id }}"></product-ledger>
                            </div>
                            <div class="tab-pane fade" id="po" role="tabpanel" aria-labelledby="po-tab">
                                <product-purchases :id="{{ $product->id }}"></product-purchases>
                            </div>
                            <div class="tab-pane fade" id="sales" role="tabpanel" aria-labelledby="sales-tab">
                                <product-sales :id="{{ $product->id }}"></product-sales>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
