<div class="modal fade" id="edit-product-modal" tabindex="-1" role="dialog" aria-labelledby="product-modalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-popout" role="document">
        <div class="modal-content p-4">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="form-material pt-0">
                               <strong> Edit {{ $product->name }}</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <form action="{{ action([\Backend\Http\Controllers\ProductController::class, 'update'], $product) }}" method="POST" enctype="multipart/form-data">
                @csrf @method('PUT')

                <div class="block-content">
                    <div class="form-group row">
                        <label for="name" class="col-12 col-md-3">Name</label>
                        <input type="text" class="col-12 col-md-9 form-control" name="name" id="name" value="{{ $product->name }}">
                    </div>
                    <div class="form-group row">
                        <label for="product_image" class="col-12 col-md-3">Image</label>
                        <img class="w-25 img-fluid col-12 col-md-9" src="{{ $product->image }}" alt="">
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-12 col-md-3">Upload a new one</label>
                        <input class="form-control col-12 col-md-9 my-2" type="file" name="image"
                               id="product_image">
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-12 col-md-3">Description</label>
                        <textarea required
                                  class="form-control col-12 col-md-9"
                                  id="description"
                                  rows="2"
                                  name="description"
                        >{{ $product->description }}</textarea>
                    </div>
                    <div class="form-group row">
                        <label for="product_image" class="col-12 col-md-3">Category</label>
                        <select class="form-control py-0 col-12 col-md-9" name="category_id">
                            <option>Please select</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}"
                                        selected="{{$product->category->id == $category->id}}"
                                >
                                    {{ ucfirst($category->name) }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <div>
                        <button type="submit" class="btn btn-outline-warning">
                           SAVE
                        </button>
                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
