@extends('backend.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 my-4">
                <div class="widget">
                    <div class="card-body d-flex justify-content-between">
                        <h5 class="card-title">{{ $salesOrders }} new {{ Str::plural('order', $salesOrders) }}</h5>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 my-4">
                <sales-orders></sales-orders>
            </div>
        </div>
    </div>
@endsection
