<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>{{ config('app.name', 'Organic') }}</title>

    <meta name="description" content="A Backend Management System">
    <meta name="author" content="Beth Fabregas">
    <meta name="robots" content="noindex, nofollow">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--Font Awesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">

    <!-- Fonts and Styles -->
    @yield('css_before')

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
    <link rel="stylesheet" id="css-main" href="{{ mix('assets/backend/css/app.css') }}">

    @yield('css_after')

</head>
<body>
<div class="wrapper d-flex align-items-stretch">

<!-- Page Content  -->
    <div id="app" class="p-2">

        <div class="position-relative">
            @yield('content')

            <flash message="{{ session('flash') }}" ></flash>
        </div>

    </div>
</div>

<script src="{{ mix('assets/backend/js/app.js') }}"></script>

@yield('js_after')

</body>
</html>
