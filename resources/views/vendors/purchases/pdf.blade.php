<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <style>
        .d-inline {
            display: inline;
        }

        .d-none {
            display: none;
        }
    </style>
</head>
<body>

<table>
    <tr>
        <td>PO#:</td>
        <td colspan="3">{{ $purchaseOrder->id }}</td>
    </tr>
    <tr>
        <td>To:</td>
        <td colspan="3">{{ $purchaseOrder->vendor->name }}</td>
    </tr>
    <tr>
        <td>Date</td>
        <td colspan="3">{{ $purchaseOrder->created_at->format('d M Y') }}</td>
    </tr>
    <tr>
        <th>ITEM</th>
        <th>QUANTITY</th>
        <th>UNIT PRICE</th>
        <th>AMOUNT</th>
    </tr>

    @foreach($orders as $order)
        <tr>
            @foreach($products as $product)
                <td class="{{ $product->id === $order->item ? 'd-inline' : 'd-none' }}">
                    {{ $product->id === $order->item ? $product->name : '' }}
                </td>
            @endforeach
            <td>{{ $order->quantity }}</td>
            <td>{{ $order->price }}</td>
            <td>{{ number_format($order->quantity * $order->price, 2) }}</td>
        </tr>
    @endforeach

    <tr>
        <td></td>
        <td></td>
        <td><strong>Total:</strong></td>
        <td><strong>{{ number_format($purchaseOrder->total, 2) }}</strong></td>
    </tr>

</table>
</body>
</html>
