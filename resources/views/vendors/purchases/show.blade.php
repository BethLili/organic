@extends('vendors.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center my-4">
            <div class="col-md-12 my-4">

                <div class="card p-4 bg-white rounded">
                    <div class="card-header">
                        <div class="d-flex justify-content-end">
                            <div>
                                <a class="btn btn-sm btn-info"
                                   href="{{action([\Backend\Http\Controllers\PurchaseOrderController::class, 'download'], $purchaseOrder)}}">Download
                                    PDF</a>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div class="row">
                                <div class="mx-4">
                                    <h5>To</h5>
                                    <p>{{ $purchaseOrder->vendor->name }}</p>
                                    <p><small>{{ $purchaseOrder->vendor->address  }} <br>
                                            {{ $purchaseOrder->vendor->email  }} <br>
                                            {{ $purchaseOrder->vendor->contact  }}</small></p>
                                </div>
                                <div class="mx-4">
                                    <h5>Date</h5>
                                    <p>{{ $purchaseOrder->created_at->format('d M Y')  }}</p>
                                </div>
                            </div>
                            <div class="mx-4">
                                <h5>Total</h5>
                                <p>{{ number_format($purchaseOrder->total, 2) }}</p>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <table class="table table-vcenter text-center">
                            <thead>
                            <tr>
                                <th class="d-none d-sm-table-cell">Item</th>
                                <th class="d-none d-sm-table-cell">Quantity</th>
                                <th class="d-none d-sm-table-cell">Unit Price</th>
                                <th class="">Amount</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($orders as $order)
                                <tr>
                                    @foreach($products as $product)
                                        <td class="{{ $product->id === $order->item ? 'd-inline' : 'd-none' }}">
                                            {{ $product->id === $order->item ? $product->name : '' }}
                                        </td>
                                    @endforeach
                                    <td>{{ $order->quantity }}</td>
                                    <td>{{ $order->price }}</td>
                                    <td>{{ number_format($order->quantity * $order->price, 2) }}</td>
                                </tr>
                            @endforeach

                            <tr>
                                <td></td>
                                <td></td>
                                <td><strong>Total:</strong></td>
                                <td><strong>{{ number_format($purchaseOrder->total, 2) }}</strong></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
