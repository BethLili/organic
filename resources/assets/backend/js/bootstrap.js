window._ = require('lodash')

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default
    window.$ = window.jQuery = require('jquery')

    require('bootstrap')
} catch (e) {}

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });

// import jQuery from 'jquery';
// import SimpleBar from 'simplebar';
// // import Cookies from 'js-cookie';
// import 'bootstrap';
// import 'popper.js';
// // import 'jquery.appear';
// import 'jquery-scroll-lock';
// // import 'jquery-countto';
//
// // ..and assign to window the ones that need it
// window.$ = window.jQuery    = jQuery;
// window.SimpleBar            = SimpleBar;
// // window.Cookies              = Cookies;
