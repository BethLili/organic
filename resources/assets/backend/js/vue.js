import Vue from 'vue';
import store from './store'

window.axios = require('axios')

require('./bootstrap')

//import $ from 'jquery';
window.$ = window.jQuery = $;
window.Vue = Vue;
window.events = new Vue();
window.flash = function (message, level = 'success') {
    window.events.$emit('flash', {message, level});
    store.dispatch('getAdminNotifications')
}

// Vue.use(VueRouter)

require('./bootstrap')

//reusable components
Vue.component('carousel', require('./components/Carousel').default)
Vue.component('paginator', require('./components/Paginator').default)
Vue.component('delete-button', require('./components/DeleteButton').default)
Vue.component('flash', require('./components/Flash').default)
Vue.component('modal', require('./components/Modal').default)
Vue.component('notifications', require('./components/Notifications').default)
Vue.component('notification-list', require('./components/NotificationList').default)


//pages
//Vue.component('products', require('./pages/Products').default);
//Vue.component('add-product', require('./pages/AddProduct').default);
//Vue.component('edit-product', require('./pages/EditProduct').default);
Vue.component('product-ledger', require('./pages/ProductLedger').default);
Vue.component('product-purchases', require('./pages/ProductPurchases').default);
Vue.component('product-sales', require('./pages/ProductSales').default);

Vue.component('purchase-orders', require('./pages/PurchaseOrders').default);
Vue.component('add-purchase-order', require('./pages/AddPurchaseOrder').default);

Vue.component('inventory', require('./pages/Inventory').default);
Vue.component('receive-inventory', require('./pages/ReceiveInventory').default);
Vue.component('payables', require('./pages/Payables').default);

Vue.component('vendor-purchases', require('./pages/VendorPurchases').default);
Vue.component('vendor-payments', require('./pages/VendorPayments').default);


Vue.component('cash', require('./pages/Cash').default);
Vue.component('cash-ledger', require('./pages/CashLedger').default);
Vue.component('cash-payment', require('./pages/CashPayment').default);
Vue.component('cash-deposit', require('./pages/CashDeposit').default);

Vue.component('equity-ledger', require('./pages/EquityLedger').default);

Vue.component('add-payment', require('./pages/AddPayment').default);

Vue.component('costing', require('./pages/Costing').default);

Vue.component('sales-orders', require('./pages/SalesOrders').default)
Vue.component('users-orders', require('./pages/UserOrders').default)

//ledgers
Vue.component('sales', require('./pages/Sales').default);

//reports
Vue.component('cash-ledgers', require('./pages/CashLedgers').default);

const app = new Vue({
    el: '#app',
    store
    // router
});


