import moment from 'moment'

export default {
    filters: {
        capitalize(value) {
            if (!value) return ''
            value = value.toString()
            return value.charAt(0).toUpperCase() + value.slice(1)
        },
        currency(value) {
            let formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'PHP',
            });

            return formatter.format(value);
        },
        number(value) {
            return value.toFixed(2);
        },
        time: function (value) {
            return moment(value).format('MMM DD, YYYY')
        },
    },
}
