import * as api from '../api'

export default {
  getAdminNotifications ({commit}) {
    api.getAdminNotifications(count => {
      commit('setNotifications', count)
    })
  },
}
