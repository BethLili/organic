$(document).ready(function () {
  $('body').tooltip({selector: '[data-toggle="tooltip"]', trigger: 'hover'})

  $('.moment-date').html(function () {
    if ($(this).data('format') === undefined) {
      return momentDate($(this).data('date'))
    }

    return momentDate($(this).data('date'), $(this).data('format'))
  })

})
