export function getAdminNotifications (callback) {
  window.axios.get('/notifications-count').then(({data}) => {
    callback(data.notificationsCount)
  }).catch(e => {
    console.log(e)
  })
}
