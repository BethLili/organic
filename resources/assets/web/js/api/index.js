export function getNotifications (callback) {
  window.axios.get('/user-notifications-count').then(({data}) => {
    callback(data.notificationsCount)
      console.log(data.notificationsCount)
  }).catch(e => {
    console.log(e)
  })
}

export function getCartCount (callback) {
  window.axios.get('/cart-count').then(({data}) => {
    callback(data.cartCount)
  }).catch(e => {
    console.log(e)
  })
}
