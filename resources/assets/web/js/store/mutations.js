export default {
  setNotifications (state, count) {
    state.userNotificationsCount = count
  },
  setCartCount (state, count) {
    state.cartCount = count
  },
}
