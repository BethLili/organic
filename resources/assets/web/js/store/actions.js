import * as api from '../api'

export default {
  getNotifications ({commit}) {
    api.getNotifications(count => {
      commit('setNotifications', count)
    })
  },
  getCartCount ({commit}) {
    api.getCartCount(count => {
      commit('setCartCount', count)
    })
  },
}
