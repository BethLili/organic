import Vue from 'vue';
import store from './store'
import VueStarRating from 'vue-star-rating'


window.Vue = Vue;

window.axios = require('axios')

require('./bootstrap')

window.events = new Vue();

window.flash = function (message, level = 'success') {
    window.events.$emit('flash', {message, level});
}

import $ from 'jquery';
window.$ = window.jQuery = $;

//reusable components
Vue.component('flash', require('./components/Flash').default)
Vue.component('carousel', require('./components/Carousel').default)
Vue.component('paginator', require('./components/Paginator').default)
Vue.component('add-to-cart', require('./components/AddToCart').default)
Vue.component('quantity', require('./components/Quantity').default)
Vue.component('delete-btn', require('./components/DeleteBtn').default)
Vue.component('delete-button', require('./components/DeleteButton').default)
Vue.component('user-cart-count', require('./components/UserCartCount').default)
Vue.component('vue-star-rating', VueStarRating);
Vue.component('notifications', require('./components/Notifications').default)
Vue.component('notification-list', require('./components/NotificationList').default)
Vue.component('reviews-list', require('./components/ReviewsList').default)
//pages
Vue.component('shops', require('./pages/Shops').default)

Vue.component('cart', require('./pages/Cart').default)
Vue.component('orders', require('./pages/Orders').default)
Vue.component('item', require('./pages/Item').default)
Vue.component('related-item', require('./pages/RelatedItem').default)
Vue.component('edit-rating', require('./pages/EditRating').default)
Vue.component('rating', require('./pages/Rating').default)

const app = new Vue({
    el: '#app',
    store
});

store.dispatch('getCartCount')
store.dispatch('getNotifications')
