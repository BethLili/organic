import VueRouter from 'vue-router';

let routes = [
    {
        path: '/shops',
        name: 'Shops',
        component: require('./pages/Shops').default
    },
    {
        path: '/carts',
        name: 'Cart',
        component: require('./pages/Cart').default
    },
    {
        path: '/item/:id',
        name: "ShopDetails",
        component: require('./pages/Item').default
    },
    {
        path: '/orders',
        name: "Orders",
        component: require('./pages/Orders').default
    },
];

export default new VueRouter({
    routes,
    mode: "history",
    linkActiveClass: 'active',
    // or linkExactActiveClass
})
