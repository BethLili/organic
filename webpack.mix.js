const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//backend
mix
    .js('resources/assets/backend/js/app.js', 'public/assets/backend/js')
    .sass('resources/assets/backend/sass/app.scss', 'public/assets/backend/css')


    //web
    .js('resources/assets/web/js/app.js', 'public/assets/web/js')
    .sass('resources/assets/web/sass/app.scss', 'public/assets/web/css')
    .copyDirectory('resources/assets/web/images', 'public/assets/web/images')
    .copyDirectory('resources/assets/web/fonts', 'public/assets/web/fonts')

