purchase order
one row per qty or one per PO?
subtotal and total should not be saved

once po is saved, 
notify vendor, and notify admin

once items are delivered, 
new event -> update items ledger
update payables/cash
remove inventories table 

deleting product will also delete inventory, hence will cause an error of 
"trying to get balance of non-object. product has soft delete, while inventory does not. 
i was able to do this error because, 
i deleted the product, so deleted_at generated a date, then I undo the delete by deleting the date in the deleted_at column, 
so now when the product call for $product->balance, it cant call the balance because it is based on inventory, which was permanently deleted.


BUGS TO FIX
on adding Purchase Order
    empty input still saves the row, with no product_id resulting in undefined $product->balance error
    
    login redirects to home, home no longer exists
    



BUGS TO FIX:

    Shops.vue - when I click add to cart three times, 3 kgs should be on the cart, but it only has one
        -solution might be to create another controller for shops.vue, right now, 
        i only disable the add to cart button if it is already added
        
        ==enabled
        ==incremented
        
        limit up to item.balance
        
    Shop.vue - items are added to cart even if insuffients stocks
                when i add to cart for the first time, for ex: 2 kgs, it is added to cart
                when i add to cart for the 2nd time, for ex: 3 kgs, it is added to cart. cart should have 5 kgs of this item,
                    but it only has 3 kgs, I update the quantity on click, should be increment
                     
        ==no add to cart and input quantity on this page
                            
    Cart.vue - when item has only 10kgs. I add 2 items on the cart
                then i update the quantity to 12kgs, i checkout 12 kgs, 
                it will store the previous value, which is 2kgs
                
                on checkout, cart items should be deleted
    
                when I add the item on Jan 5, 4 kgs, i didnt checkout, i come back on Jan 6, 
                the item became out of stock, item should be deleted on all users cart
                
                JC added to cart cauliflower 2 kg 
                
        ==disabled the input button
        ==on
